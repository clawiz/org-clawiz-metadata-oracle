/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.oracle.loader.table;

import org.clawiz.core.common.system.database.Statement;
import org.clawiz.metadata.oracle.loader.AbstractOracleMetadataLoader;

import java.util.ArrayList;
import java.util.HashMap;

public class TableListOracleMetadataLoader extends AbstractOracleMetadataLoader {

    @Override
    public void process() {
        super.process();

        logDebug("Loading tables definitions ...");

        HashMap<String, TableOracleMetadataLoaderContext> tableContextsMap = new HashMap<>();
        ArrayList<TableOracleMetadataLoaderContext>       tableContexts    = new ArrayList<>();

        Statement statement = executeSourceQuery("select table_name from user_tables order by 1");
        while ( statement.next() ) {

            TableOracleMetadataLoaderContext tableContext = new TableOracleMetadataLoaderContext();
            tableContext.setName(statement.getString(1));

            tableContextsMap.put(tableContext.getName(), tableContext);
            tableContexts.add(tableContext);

        }
        statement.close();

        statement = executeSourceQuery("select table_name, column_id, column_name, data_type, data_length, data_precision, data_scale\n" +
                " from user_tab_cols\n" +
                " order by 1, 2");
        while ( statement.next() ) {

            String tableName = statement.getString(1);
            TableOracleMetadataLoaderContext tableContext = tableContextsMap.get(tableName);

            TableOracleMetadataLoaderContext.Column column = tableContext.addColumn();
            column.name      = statement.getString(3);
            column.type      = statement.getString(4);
            column.length    = statement.getBigDecimal(5);
            column.precision = statement.getBigDecimal(6);
            column.scale     = statement.getBigDecimal(7);


        }
        statement.close();

        for ( TableOracleMetadataLoaderContext tableContext : tableContexts ) {
            addLoader(TableOracleMetadataLoader.class).setTableContext(tableContext);
        }


        logDebug("   tables definitions loaded");
    }
}
