/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.oracle.loader.source;

import org.clawiz.core.common.system.database.Statement;
import org.clawiz.metadata.oracle.loader.AbstractOracleMetadataLoader;

import java.util.HashMap;

public class AllSourcesOracleMetadataLoader extends AbstractOracleMetadataLoader {

    String        currentType;
    String        currentName;
    StringBuilder source;

    HashMap<String, HashMap<String, String>> sources;

    private void fixSource() {

        if ( currentType == null || currentName == null ) {
            return;
        }

        HashMap<String, String> namesCache = sources.get(currentType);
        if ( namesCache == null ) {
            namesCache = new HashMap<>();
            sources.put(currentType, namesCache);
        }

        namesCache.put(currentName, source.toString());

    }

    @Override
    public void callChildsProcess() {
        super.callChildsProcess();

        logDebug("Loading sources ...");

        sources = getContext().getSources();
        sources.clear();

        String        prefix = "";

        Statement statement = executeSourceQuery("select type, name, text from user_source order by name, type, line");
        while ( statement.next() ) {

            String type = statement.getString(1);
            String name = statement.getString(2);
            String text = statement.getString(3);

            if ( currentType == null || ! currentType.equals(type) || ! currentName.equals(name)) {
                fixSource();
                currentType = type;
                currentName = name;
                source      = new StringBuilder();
                prefix      = "";

            }

            if ( ! currentName.equals(name)) {
                fixSource();
            }

            source.append(prefix).append(text);
            prefix = "\n";
        }

        statement.close();
        fixSource();

        logDebug("    sources loaded");

    }
}
