/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.oracle.loader;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.metadata.data.service.ServiceNode;
import org.clawiz.core.common.metadata.data.service.method.ServiceMethod;
import org.clawiz.core.common.metadata.data.service.method.parameter.ServiceMethodParameter;
import org.clawiz.core.common.metadata.data.service.method.valuetype.AbstractServiceMethodValueType;
import org.clawiz.core.common.metadata.data.service.method.valuetype.ServiceMethodValueTypeString;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.metadata.oracle.loader.antlr.PlSqlParserBaseListener;
import org.clawiz.metadata.oracle.loader.antlr.PlSqlParser;

import java.util.ArrayList;

public class AbstractOracleMetadataParserListener extends PlSqlParserBaseListener {


    AbstractOracleMetadataLoader loader;

    ArrayList<MetadataNode>      nodesStack = new ArrayList<>();

    public AbstractOracleMetadataLoader getLoader() {
        return loader;
    }

    public void setLoader(AbstractOracleMetadataLoader loader) {
        this.loader = loader;
    }

    public void throwException(String code, Object... parameters) {
        loader.throwException(code, parameters);
    }
    
    protected void throwNotImplemented() {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        throw new CoreException("Parser listener method for '?' not implemented ('?')", stackTrace[2].getMethodName().substring(5).toUpperCase(), stackTrace[2].getMethodName());
    }

    public void logDebug(String message) {
        loader.logDebug(message);
    }

    public MetadataNode getNode() {
        return nodesStack.size() > 0 ? nodesStack.get(nodesStack.size()-1) : null;
    }

    public <T extends MetadataNode> T push(Class<T> nodeClass, String parentNodeFieldName) {
        T child;
        if ( getNode() == null ) {
            try {
                child = nodeClass.newInstance();
                child.setPackageName(getLoader().getContext().getPackageName());
                loader.getContext().getNodes().add(child);
            } catch (Exception e) {
                throwException("Exception on create node class ? for field '?' : ?", nodeClass, parentNodeFieldName, e.getMessage(), e);
                return null;
            }

            loader.getContext().getPackageName();
        } else {
            child = getNode().createChildNode(nodeClass, parentNodeFieldName);
        }
        nodesStack.add(child);
        return child;
    }

    public void pop() {
        if ( nodesStack.size() == 0 ) {
            throwException("Cannot pop from empty nodes stack");
        }
        nodesStack.remove(nodesStack.size()-1);
    }

    public <T extends AbstractServiceMethodValueType> Class<T> nativeDataTypeNameToServiceMethodValueTypeClass(String name) {

        if ( name == null ) {
            throwException("Cannot define service method value type for NULL native data type");
        }
        switch (name.toUpperCase()) {
            case "VARCHAR":
            case "VARCHAR2": return (Class<T>) ServiceMethodValueTypeString.class;

            default: throwException("Cannot define service method value type for '' native data type", name);
        }

        return null;

    }

    @Override
    public void enterCreate_package(PlSqlParser.Create_packageContext ctx) {
        push(ServiceNode.class, null);
    }


    public AbstractOracleMetadataParserListener() {
        super();
    }

    @Override
    public void enterSql_script(PlSqlParser.Sql_scriptContext ctx) {
        throwNotImplemented(); // super.enterSql_script(ctx);
    }

    @Override
    public void exitSql_script(PlSqlParser.Sql_scriptContext ctx) {
        super.exitSql_script(ctx);
    }

    @Override
    public void enterUnit_statement(PlSqlParser.Unit_statementContext ctx) {
        throwNotImplemented(); // super.enterUnit_statement(ctx);
    }

    @Override
    public void exitUnit_statement(PlSqlParser.Unit_statementContext ctx) {
        super.exitUnit_statement(ctx);
    }

    @Override
    public void enterDrop_function(PlSqlParser.Drop_functionContext ctx) {
        throwNotImplemented(); // super.enterDrop_function(ctx);
    }

    @Override
    public void exitDrop_function(PlSqlParser.Drop_functionContext ctx) {
        super.exitDrop_function(ctx);
    }

    @Override
    public void enterAlter_function(PlSqlParser.Alter_functionContext ctx) {
        throwNotImplemented(); // super.enterAlter_function(ctx);
    }

    @Override
    public void exitAlter_function(PlSqlParser.Alter_functionContext ctx) {
        super.exitAlter_function(ctx);
    }

    @Override
    public void enterCreate_function_body(PlSqlParser.Create_function_bodyContext ctx) {
        throwNotImplemented(); // super.enterCreate_function_body(ctx);
    }

    @Override
    public void exitCreate_function_body(PlSqlParser.Create_function_bodyContext ctx) {
        super.exitCreate_function_body(ctx);
    }

    @Override
    public void enterParallel_enable_clause(PlSqlParser.Parallel_enable_clauseContext ctx) {
        throwNotImplemented(); // super.enterParallel_enable_clause(ctx);
    }

    @Override
    public void exitParallel_enable_clause(PlSqlParser.Parallel_enable_clauseContext ctx) {
        super.exitParallel_enable_clause(ctx);
    }

    @Override
    public void enterPartition_by_clause(PlSqlParser.Partition_by_clauseContext ctx) {
        throwNotImplemented(); // super.enterPartition_by_clause(ctx);
    }

    @Override
    public void exitPartition_by_clause(PlSqlParser.Partition_by_clauseContext ctx) {
        super.exitPartition_by_clause(ctx);
    }

    @Override
    public void enterResult_cache_clause(PlSqlParser.Result_cache_clauseContext ctx) {
        throwNotImplemented();
        throwNotImplemented(); // super.enterResult_cache_clause(ctx);
    }

    @Override
    public void exitResult_cache_clause(PlSqlParser.Result_cache_clauseContext ctx) {
        super.exitResult_cache_clause(ctx);
    }

    @Override
    public void enterRelies_on_part(PlSqlParser.Relies_on_partContext ctx) {
        throwNotImplemented(); // super.enterRelies_on_part(ctx);
    }

    @Override
    public void exitRelies_on_part(PlSqlParser.Relies_on_partContext ctx) {
        super.exitRelies_on_part(ctx);
    }

    @Override
    public void enterStreaming_clause(PlSqlParser.Streaming_clauseContext ctx) {
        throwNotImplemented(); // super.enterStreaming_clause(ctx);
    }

    @Override
    public void exitStreaming_clause(PlSqlParser.Streaming_clauseContext ctx) {
        super.exitStreaming_clause(ctx);
    }

    @Override
    public void enterDrop_package(PlSqlParser.Drop_packageContext ctx) {
        throwNotImplemented(); // super.enterDrop_package(ctx);
    }

    @Override
    public void exitDrop_package(PlSqlParser.Drop_packageContext ctx) {
        super.exitDrop_package(ctx);
    }

    @Override
    public void enterAlter_package(PlSqlParser.Alter_packageContext ctx) {
        throwNotImplemented(); // super.enterAlter_package(ctx);
    }

    @Override
    public void exitAlter_package(PlSqlParser.Alter_packageContext ctx) {
        super.exitAlter_package(ctx);
    }

    @Override
    public void exitCreate_package(PlSqlParser.Create_packageContext ctx) {
        super.exitCreate_package(ctx);
        pop();
    }

    @Override
    public void enterCreate_package_body(PlSqlParser.Create_package_bodyContext ctx) {
        throwNotImplemented(); // super.enterCreate_package_body(ctx);
    }

    @Override
    public void exitCreate_package_body(PlSqlParser.Create_package_bodyContext ctx) {
        super.exitCreate_package_body(ctx);
    }

    @Override
    public void enterPackage_obj_spec(PlSqlParser.Package_obj_specContext ctx) {
        super.enterPackage_obj_spec(ctx);
    }

    @Override
    public void exitPackage_obj_spec(PlSqlParser.Package_obj_specContext ctx) {
        super.exitPackage_obj_spec(ctx);
    }

    @Override
    public void enterProcedure_spec(PlSqlParser.Procedure_specContext ctx) {

        ((ServiceNode) getNode()).getMethods().add(
                push(ServiceMethod.class, "methods")
        );

    }

    @Override
    public void exitProcedure_spec(PlSqlParser.Procedure_specContext ctx) {
        super.exitProcedure_spec(ctx);
        pop();
    }

    @Override
    public void enterFunction_spec(PlSqlParser.Function_specContext ctx) {
        throwNotImplemented(); // super.enterFunction_spec(ctx);
    }

    @Override
    public void exitFunction_spec(PlSqlParser.Function_specContext ctx) {
        super.exitFunction_spec(ctx);
    }

    @Override
    public void enterPackage_obj_body(PlSqlParser.Package_obj_bodyContext ctx) {
        throwNotImplemented(); // super.enterPackage_obj_body(ctx);
    }

    @Override
    public void exitPackage_obj_body(PlSqlParser.Package_obj_bodyContext ctx) {
        super.exitPackage_obj_body(ctx);
    }

    @Override
    public void enterDrop_procedure(PlSqlParser.Drop_procedureContext ctx) {
        throwNotImplemented(); // super.enterDrop_procedure(ctx);
    }

    @Override
    public void exitDrop_procedure(PlSqlParser.Drop_procedureContext ctx) {
        super.exitDrop_procedure(ctx);
    }

    @Override
    public void enterAlter_procedure(PlSqlParser.Alter_procedureContext ctx) {
        throwNotImplemented(); // super.enterAlter_procedure(ctx);
    }

    @Override
    public void exitAlter_procedure(PlSqlParser.Alter_procedureContext ctx) {
        super.exitAlter_procedure(ctx);
    }

    @Override
    public void enterFunction_body(PlSqlParser.Function_bodyContext ctx) {
        throwNotImplemented(); // super.enterFunction_body(ctx);
    }

    @Override
    public void exitFunction_body(PlSqlParser.Function_bodyContext ctx) {
        super.exitFunction_body(ctx);
    }

    @Override
    public void enterProcedure_body(PlSqlParser.Procedure_bodyContext ctx) {
        throwNotImplemented(); // super.enterProcedure_body(ctx);
    }

    @Override
    public void exitProcedure_body(PlSqlParser.Procedure_bodyContext ctx) {
        super.exitProcedure_body(ctx);
    }

    @Override
    public void enterCreate_procedure_body(PlSqlParser.Create_procedure_bodyContext ctx) {
        throwNotImplemented(); // super.enterCreate_procedure_body(ctx);
    }

    @Override
    public void exitCreate_procedure_body(PlSqlParser.Create_procedure_bodyContext ctx) {
        super.exitCreate_procedure_body(ctx);
    }

    @Override
    public void enterDrop_trigger(PlSqlParser.Drop_triggerContext ctx) {
        throwNotImplemented(); // super.enterDrop_trigger(ctx);
    }

    @Override
    public void exitDrop_trigger(PlSqlParser.Drop_triggerContext ctx) {
        super.exitDrop_trigger(ctx);
    }

    @Override
    public void enterAlter_trigger(PlSqlParser.Alter_triggerContext ctx) {
        throwNotImplemented(); // super.enterAlter_trigger(ctx);
    }

    @Override
    public void exitAlter_trigger(PlSqlParser.Alter_triggerContext ctx) {
        super.exitAlter_trigger(ctx);
    }

    @Override
    public void enterCreate_trigger(PlSqlParser.Create_triggerContext ctx) {
        throwNotImplemented(); // super.enterCreate_trigger(ctx);
    }

    @Override
    public void exitCreate_trigger(PlSqlParser.Create_triggerContext ctx) {
        super.exitCreate_trigger(ctx);
    }

    @Override
    public void enterTrigger_follows_clause(PlSqlParser.Trigger_follows_clauseContext ctx) {
        throwNotImplemented(); // super.enterTrigger_follows_clause(ctx);
    }

    @Override
    public void exitTrigger_follows_clause(PlSqlParser.Trigger_follows_clauseContext ctx) {
        super.exitTrigger_follows_clause(ctx);
    }

    @Override
    public void enterTrigger_when_clause(PlSqlParser.Trigger_when_clauseContext ctx) {
        throwNotImplemented(); // super.enterTrigger_when_clause(ctx);
    }

    @Override
    public void exitTrigger_when_clause(PlSqlParser.Trigger_when_clauseContext ctx) {
        super.exitTrigger_when_clause(ctx);
    }

    @Override
    public void enterSimple_dml_trigger(PlSqlParser.Simple_dml_triggerContext ctx) {
        throwNotImplemented(); // super.enterSimple_dml_trigger(ctx);
    }

    @Override
    public void exitSimple_dml_trigger(PlSqlParser.Simple_dml_triggerContext ctx) {
        super.exitSimple_dml_trigger(ctx);
    }

    @Override
    public void enterFor_each_row(PlSqlParser.For_each_rowContext ctx) {
        throwNotImplemented(); // super.enterFor_each_row(ctx);
    }

    @Override
    public void exitFor_each_row(PlSqlParser.For_each_rowContext ctx) {
        super.exitFor_each_row(ctx);
    }

    @Override
    public void enterCompound_dml_trigger(PlSqlParser.Compound_dml_triggerContext ctx) {
        throwNotImplemented(); // super.enterCompound_dml_trigger(ctx);
    }

    @Override
    public void exitCompound_dml_trigger(PlSqlParser.Compound_dml_triggerContext ctx) {
        super.exitCompound_dml_trigger(ctx);
    }

    @Override
    public void enterNon_dml_trigger(PlSqlParser.Non_dml_triggerContext ctx) {
        throwNotImplemented(); // super.enterNon_dml_trigger(ctx);
    }

    @Override
    public void exitNon_dml_trigger(PlSqlParser.Non_dml_triggerContext ctx) {
        super.exitNon_dml_trigger(ctx);
    }

    @Override
    public void enterTrigger_body(PlSqlParser.Trigger_bodyContext ctx) {
        throwNotImplemented(); // super.enterTrigger_body(ctx);
    }

    @Override
    public void exitTrigger_body(PlSqlParser.Trigger_bodyContext ctx) {
        super.exitTrigger_body(ctx);
    }

    @Override
    public void enterRoutine_clause(PlSqlParser.Routine_clauseContext ctx) {
        throwNotImplemented(); // super.enterRoutine_clause(ctx);
    }

    @Override
    public void exitRoutine_clause(PlSqlParser.Routine_clauseContext ctx) {
        super.exitRoutine_clause(ctx);
    }

    @Override
    public void enterCompound_trigger_block(PlSqlParser.Compound_trigger_blockContext ctx) {
        throwNotImplemented(); // super.enterCompound_trigger_block(ctx);
    }

    @Override
    public void exitCompound_trigger_block(PlSqlParser.Compound_trigger_blockContext ctx) {
        super.exitCompound_trigger_block(ctx);
    }

    @Override
    public void enterTiming_point_section(PlSqlParser.Timing_point_sectionContext ctx) {
        throwNotImplemented(); // super.enterTiming_point_section(ctx);
    }

    @Override
    public void exitTiming_point_section(PlSqlParser.Timing_point_sectionContext ctx) {
        super.exitTiming_point_section(ctx);
    }

    @Override
    public void enterNon_dml_event(PlSqlParser.Non_dml_eventContext ctx) {
        throwNotImplemented(); // super.enterNon_dml_event(ctx);
    }

    @Override
    public void exitNon_dml_event(PlSqlParser.Non_dml_eventContext ctx) {
        super.exitNon_dml_event(ctx);
    }

    @Override
    public void enterDml_event_clause(PlSqlParser.Dml_event_clauseContext ctx) {
        throwNotImplemented(); // super.enterDml_event_clause(ctx);
    }

    @Override
    public void exitDml_event_clause(PlSqlParser.Dml_event_clauseContext ctx) {
        super.exitDml_event_clause(ctx);
    }

    @Override
    public void enterDml_event_element(PlSqlParser.Dml_event_elementContext ctx) {
        throwNotImplemented(); // super.enterDml_event_element(ctx);
    }

    @Override
    public void exitDml_event_element(PlSqlParser.Dml_event_elementContext ctx) {
        super.exitDml_event_element(ctx);
    }

    @Override
    public void enterDml_event_nested_clause(PlSqlParser.Dml_event_nested_clauseContext ctx) {
        throwNotImplemented(); // super.enterDml_event_nested_clause(ctx);
    }

    @Override
    public void exitDml_event_nested_clause(PlSqlParser.Dml_event_nested_clauseContext ctx) {
        super.exitDml_event_nested_clause(ctx);
    }

    @Override
    public void enterReferencing_clause(PlSqlParser.Referencing_clauseContext ctx) {
        throwNotImplemented(); // super.enterReferencing_clause(ctx);
    }

    @Override
    public void exitReferencing_clause(PlSqlParser.Referencing_clauseContext ctx) {
        super.exitReferencing_clause(ctx);
    }

    @Override
    public void enterReferencing_element(PlSqlParser.Referencing_elementContext ctx) {
        throwNotImplemented(); // super.enterReferencing_element(ctx);
    }

    @Override
    public void exitReferencing_element(PlSqlParser.Referencing_elementContext ctx) {
        super.exitReferencing_element(ctx);
    }

    @Override
    public void enterDrop_type(PlSqlParser.Drop_typeContext ctx) {
        throwNotImplemented(); // super.enterDrop_type(ctx);
    }

    @Override
    public void exitDrop_type(PlSqlParser.Drop_typeContext ctx) {
        super.exitDrop_type(ctx);
    }

    @Override
    public void enterAlter_type(PlSqlParser.Alter_typeContext ctx) {
        throwNotImplemented(); // super.enterAlter_type(ctx);
    }

    @Override
    public void exitAlter_type(PlSqlParser.Alter_typeContext ctx) {
        super.exitAlter_type(ctx);
    }

    @Override
    public void enterCompile_type_clause(PlSqlParser.Compile_type_clauseContext ctx) {
        throwNotImplemented(); // super.enterCompile_type_clause(ctx);
    }

    @Override
    public void exitCompile_type_clause(PlSqlParser.Compile_type_clauseContext ctx) {
        super.exitCompile_type_clause(ctx);
    }

    @Override
    public void enterReplace_type_clause(PlSqlParser.Replace_type_clauseContext ctx) {
        throwNotImplemented(); // super.enterReplace_type_clause(ctx);
    }

    @Override
    public void exitReplace_type_clause(PlSqlParser.Replace_type_clauseContext ctx) {
        super.exitReplace_type_clause(ctx);
    }

    @Override
    public void enterAlter_method_spec(PlSqlParser.Alter_method_specContext ctx) {
        throwNotImplemented(); // super.enterAlter_method_spec(ctx);
    }

    @Override
    public void exitAlter_method_spec(PlSqlParser.Alter_method_specContext ctx) {
        super.exitAlter_method_spec(ctx);
    }

    @Override
    public void enterAlter_method_element(PlSqlParser.Alter_method_elementContext ctx) {
        throwNotImplemented(); // super.enterAlter_method_element(ctx);
    }

    @Override
    public void exitAlter_method_element(PlSqlParser.Alter_method_elementContext ctx) {
        super.exitAlter_method_element(ctx);
    }

    @Override
    public void enterAlter_attribute_definition(PlSqlParser.Alter_attribute_definitionContext ctx) {
        throwNotImplemented(); // super.enterAlter_attribute_definition(ctx);
    }

    @Override
    public void exitAlter_attribute_definition(PlSqlParser.Alter_attribute_definitionContext ctx) {
        super.exitAlter_attribute_definition(ctx);
    }

    @Override
    public void enterAttribute_definition(PlSqlParser.Attribute_definitionContext ctx) {
        throwNotImplemented(); // super.enterAttribute_definition(ctx);
    }

    @Override
    public void exitAttribute_definition(PlSqlParser.Attribute_definitionContext ctx) {
        super.exitAttribute_definition(ctx);
    }

    @Override
    public void enterAlter_collection_clauses(PlSqlParser.Alter_collection_clausesContext ctx) {
        throwNotImplemented(); // super.enterAlter_collection_clauses(ctx);
    }

    @Override
    public void exitAlter_collection_clauses(PlSqlParser.Alter_collection_clausesContext ctx) {
        super.exitAlter_collection_clauses(ctx);
    }

    @Override
    public void enterDependent_handling_clause(PlSqlParser.Dependent_handling_clauseContext ctx) {
        throwNotImplemented(); // super.enterDependent_handling_clause(ctx);
    }

    @Override
    public void exitDependent_handling_clause(PlSqlParser.Dependent_handling_clauseContext ctx) {
        super.exitDependent_handling_clause(ctx);
    }

    @Override
    public void enterDependent_exceptions_part(PlSqlParser.Dependent_exceptions_partContext ctx) {
        throwNotImplemented(); // super.enterDependent_exceptions_part(ctx);
    }

    @Override
    public void exitDependent_exceptions_part(PlSqlParser.Dependent_exceptions_partContext ctx) {
        super.exitDependent_exceptions_part(ctx);
    }

    @Override
    public void enterCreate_type(PlSqlParser.Create_typeContext ctx) {
        throwNotImplemented(); // super.enterCreate_type(ctx);
    }

    @Override
    public void exitCreate_type(PlSqlParser.Create_typeContext ctx) {
        super.exitCreate_type(ctx);
    }

    @Override
    public void enterType_definition(PlSqlParser.Type_definitionContext ctx) {
        throwNotImplemented(); // super.enterType_definition(ctx);
    }

    @Override
    public void exitType_definition(PlSqlParser.Type_definitionContext ctx) {
        super.exitType_definition(ctx);
    }

    @Override
    public void enterObject_type_def(PlSqlParser.Object_type_defContext ctx) {
        throwNotImplemented(); // super.enterObject_type_def(ctx);
    }

    @Override
    public void exitObject_type_def(PlSqlParser.Object_type_defContext ctx) {
        super.exitObject_type_def(ctx);
    }

    @Override
    public void enterObject_as_part(PlSqlParser.Object_as_partContext ctx) {
        throwNotImplemented(); // super.enterObject_as_part(ctx);
    }

    @Override
    public void exitObject_as_part(PlSqlParser.Object_as_partContext ctx) {
        super.exitObject_as_part(ctx);
    }

    @Override
    public void enterObject_under_part(PlSqlParser.Object_under_partContext ctx) {
        throwNotImplemented(); // super.enterObject_under_part(ctx);
    }

    @Override
    public void exitObject_under_part(PlSqlParser.Object_under_partContext ctx) {
        super.exitObject_under_part(ctx);
    }

    @Override
    public void enterNested_table_type_def(PlSqlParser.Nested_table_type_defContext ctx) {
        throwNotImplemented(); // super.enterNested_table_type_def(ctx);
    }

    @Override
    public void exitNested_table_type_def(PlSqlParser.Nested_table_type_defContext ctx) {
        super.exitNested_table_type_def(ctx);
    }

    @Override
    public void enterSqlj_object_type(PlSqlParser.Sqlj_object_typeContext ctx) {
        throwNotImplemented(); // super.enterSqlj_object_type(ctx);
    }

    @Override
    public void exitSqlj_object_type(PlSqlParser.Sqlj_object_typeContext ctx) {
        super.exitSqlj_object_type(ctx);
    }

    @Override
    public void enterType_body(PlSqlParser.Type_bodyContext ctx) {
        throwNotImplemented(); // super.enterType_body(ctx);
    }

    @Override
    public void exitType_body(PlSqlParser.Type_bodyContext ctx) {
        super.exitType_body(ctx);
    }

    @Override
    public void enterType_body_elements(PlSqlParser.Type_body_elementsContext ctx) {
        throwNotImplemented(); // super.enterType_body_elements(ctx);
    }

    @Override
    public void exitType_body_elements(PlSqlParser.Type_body_elementsContext ctx) {
        super.exitType_body_elements(ctx);
    }

    @Override
    public void enterMap_order_func_declaration(PlSqlParser.Map_order_func_declarationContext ctx) {
        throwNotImplemented(); // super.enterMap_order_func_declaration(ctx);
    }

    @Override
    public void exitMap_order_func_declaration(PlSqlParser.Map_order_func_declarationContext ctx) {
        super.exitMap_order_func_declaration(ctx);
    }

    @Override
    public void enterSubprog_decl_in_type(PlSqlParser.Subprog_decl_in_typeContext ctx) {
        throwNotImplemented(); // super.enterSubprog_decl_in_type(ctx);
    }

    @Override
    public void exitSubprog_decl_in_type(PlSqlParser.Subprog_decl_in_typeContext ctx) {
        super.exitSubprog_decl_in_type(ctx);
    }

    @Override
    public void enterProc_decl_in_type(PlSqlParser.Proc_decl_in_typeContext ctx) {
        throwNotImplemented(); // super.enterProc_decl_in_type(ctx);
    }

    @Override
    public void exitProc_decl_in_type(PlSqlParser.Proc_decl_in_typeContext ctx) {
        super.exitProc_decl_in_type(ctx);
    }

    @Override
    public void enterFunc_decl_in_type(PlSqlParser.Func_decl_in_typeContext ctx) {
        throwNotImplemented(); // super.enterFunc_decl_in_type(ctx);
    }

    @Override
    public void exitFunc_decl_in_type(PlSqlParser.Func_decl_in_typeContext ctx) {
        super.exitFunc_decl_in_type(ctx);
    }

    @Override
    public void enterConstructor_declaration(PlSqlParser.Constructor_declarationContext ctx) {
        throwNotImplemented(); // super.enterConstructor_declaration(ctx);
    }

    @Override
    public void exitConstructor_declaration(PlSqlParser.Constructor_declarationContext ctx) {
        super.exitConstructor_declaration(ctx);
    }

    @Override
    public void enterModifier_clause(PlSqlParser.Modifier_clauseContext ctx) {
        throwNotImplemented(); // super.enterModifier_clause(ctx);
    }

    @Override
    public void exitModifier_clause(PlSqlParser.Modifier_clauseContext ctx) {
        super.exitModifier_clause(ctx);
    }

    @Override
    public void enterObject_member_spec(PlSqlParser.Object_member_specContext ctx) {
        throwNotImplemented(); // super.enterObject_member_spec(ctx);
    }

    @Override
    public void exitObject_member_spec(PlSqlParser.Object_member_specContext ctx) {
        super.exitObject_member_spec(ctx);
    }

    @Override
    public void enterSqlj_object_type_attr(PlSqlParser.Sqlj_object_type_attrContext ctx) {
        throwNotImplemented(); // super.enterSqlj_object_type_attr(ctx);
    }

    @Override
    public void exitSqlj_object_type_attr(PlSqlParser.Sqlj_object_type_attrContext ctx) {
        super.exitSqlj_object_type_attr(ctx);
    }

    @Override
    public void enterElement_spec(PlSqlParser.Element_specContext ctx) {
        throwNotImplemented(); // super.enterElement_spec(ctx);
    }

    @Override
    public void exitElement_spec(PlSqlParser.Element_specContext ctx) {
        super.exitElement_spec(ctx);
    }

    @Override
    public void enterElement_spec_options(PlSqlParser.Element_spec_optionsContext ctx) {
        throwNotImplemented(); // super.enterElement_spec_options(ctx);
    }

    @Override
    public void exitElement_spec_options(PlSqlParser.Element_spec_optionsContext ctx) {
        super.exitElement_spec_options(ctx);
    }

    @Override
    public void enterSubprogram_spec(PlSqlParser.Subprogram_specContext ctx) {
        throwNotImplemented(); // super.enterSubprogram_spec(ctx);
    }

    @Override
    public void exitSubprogram_spec(PlSqlParser.Subprogram_specContext ctx) {
        super.exitSubprogram_spec(ctx);
    }

    @Override
    public void enterType_procedure_spec(PlSqlParser.Type_procedure_specContext ctx) {
        throwNotImplemented(); // super.enterType_procedure_spec(ctx);
    }

    @Override
    public void exitType_procedure_spec(PlSqlParser.Type_procedure_specContext ctx) {
        super.exitType_procedure_spec(ctx);
    }

    @Override
    public void enterType_function_spec(PlSqlParser.Type_function_specContext ctx) {
        throwNotImplemented(); // super.enterType_function_spec(ctx);
    }

    @Override
    public void exitType_function_spec(PlSqlParser.Type_function_specContext ctx) {
        super.exitType_function_spec(ctx);
    }

    @Override
    public void enterConstructor_spec(PlSqlParser.Constructor_specContext ctx) {
        throwNotImplemented(); // super.enterConstructor_spec(ctx);
    }

    @Override
    public void exitConstructor_spec(PlSqlParser.Constructor_specContext ctx) {
        super.exitConstructor_spec(ctx);
    }

    @Override
    public void enterMap_order_function_spec(PlSqlParser.Map_order_function_specContext ctx) {
        throwNotImplemented(); // super.enterMap_order_function_spec(ctx);
    }

    @Override
    public void exitMap_order_function_spec(PlSqlParser.Map_order_function_specContext ctx) {
        super.exitMap_order_function_spec(ctx);
    }

    @Override
    public void enterPragma_clause(PlSqlParser.Pragma_clauseContext ctx) {
        throwNotImplemented(); // super.enterPragma_clause(ctx);
    }

    @Override
    public void exitPragma_clause(PlSqlParser.Pragma_clauseContext ctx) {
        super.exitPragma_clause(ctx);
    }

    @Override
    public void enterPragma_elements(PlSqlParser.Pragma_elementsContext ctx) {
        throwNotImplemented(); // super.enterPragma_elements(ctx);
    }

    @Override
    public void exitPragma_elements(PlSqlParser.Pragma_elementsContext ctx) {
        super.exitPragma_elements(ctx);
    }

    @Override
    public void enterType_elements_parameter(PlSqlParser.Type_elements_parameterContext ctx) {
        throwNotImplemented(); // super.enterType_elements_parameter(ctx);
    }

    @Override
    public void exitType_elements_parameter(PlSqlParser.Type_elements_parameterContext ctx) {
        super.exitType_elements_parameter(ctx);
    }

    @Override
    public void enterDrop_sequence(PlSqlParser.Drop_sequenceContext ctx) {
        throwNotImplemented(); // super.enterDrop_sequence(ctx);
    }

    @Override
    public void exitDrop_sequence(PlSqlParser.Drop_sequenceContext ctx) {
        super.exitDrop_sequence(ctx);
    }

    @Override
    public void enterAlter_sequence(PlSqlParser.Alter_sequenceContext ctx) {
        throwNotImplemented(); // super.enterAlter_sequence(ctx);
    }

    @Override
    public void exitAlter_sequence(PlSqlParser.Alter_sequenceContext ctx) {
        super.exitAlter_sequence(ctx);
    }

    @Override
    public void enterCreate_sequence(PlSqlParser.Create_sequenceContext ctx) {
        throwNotImplemented(); // super.enterCreate_sequence(ctx);
    }

    @Override
    public void exitCreate_sequence(PlSqlParser.Create_sequenceContext ctx) {
        super.exitCreate_sequence(ctx);
    }

    @Override
    public void enterSequence_spec(PlSqlParser.Sequence_specContext ctx) {
        throwNotImplemented(); // super.enterSequence_spec(ctx);
    }

    @Override
    public void exitSequence_spec(PlSqlParser.Sequence_specContext ctx) {
        super.exitSequence_spec(ctx);
    }

    @Override
    public void enterSequence_start_clause(PlSqlParser.Sequence_start_clauseContext ctx) {
        throwNotImplemented(); // super.enterSequence_start_clause(ctx);
    }

    @Override
    public void exitSequence_start_clause(PlSqlParser.Sequence_start_clauseContext ctx) {
        super.exitSequence_start_clause(ctx);
    }

    @Override
    public void enterCreate_index(PlSqlParser.Create_indexContext ctx) {
        throwNotImplemented(); // super.enterCreate_index(ctx);
    }

    @Override
    public void exitCreate_index(PlSqlParser.Create_indexContext ctx) {
        super.exitCreate_index(ctx);
    }

    @Override
    public void enterCluster_index_clause(PlSqlParser.Cluster_index_clauseContext ctx) {
        throwNotImplemented(); // super.enterCluster_index_clause(ctx);
    }

    @Override
    public void exitCluster_index_clause(PlSqlParser.Cluster_index_clauseContext ctx) {
        super.exitCluster_index_clause(ctx);
    }

    @Override
    public void enterCluster_name(PlSqlParser.Cluster_nameContext ctx) {
        throwNotImplemented(); // super.enterCluster_name(ctx);
    }

    @Override
    public void exitCluster_name(PlSqlParser.Cluster_nameContext ctx) {
        super.exitCluster_name(ctx);
    }

    @Override
    public void enterTable_index_clause(PlSqlParser.Table_index_clauseContext ctx) {
        throwNotImplemented(); // super.enterTable_index_clause(ctx);
    }

    @Override
    public void exitTable_index_clause(PlSqlParser.Table_index_clauseContext ctx) {
        super.exitTable_index_clause(ctx);
    }

    @Override
    public void enterBitmap_join_index_clause(PlSqlParser.Bitmap_join_index_clauseContext ctx) {
        throwNotImplemented(); // super.enterBitmap_join_index_clause(ctx);
    }

    @Override
    public void exitBitmap_join_index_clause(PlSqlParser.Bitmap_join_index_clauseContext ctx) {
        super.exitBitmap_join_index_clause(ctx);
    }

    @Override
    public void enterIndex_expr(PlSqlParser.Index_exprContext ctx) {
        throwNotImplemented(); // super.enterIndex_expr(ctx);
    }

    @Override
    public void exitIndex_expr(PlSqlParser.Index_exprContext ctx) {
        super.exitIndex_expr(ctx);
    }

    @Override
    public void enterIndex_properties(PlSqlParser.Index_propertiesContext ctx) {
        throwNotImplemented(); // super.enterIndex_properties(ctx);
    }

    @Override
    public void exitIndex_properties(PlSqlParser.Index_propertiesContext ctx) {
        super.exitIndex_properties(ctx);
    }

    @Override
    public void enterDomain_index_clause(PlSqlParser.Domain_index_clauseContext ctx) {
        throwNotImplemented(); // super.enterDomain_index_clause(ctx);
    }

    @Override
    public void exitDomain_index_clause(PlSqlParser.Domain_index_clauseContext ctx) {
        super.exitDomain_index_clause(ctx);
    }

    @Override
    public void enterLocal_domain_index_clause(PlSqlParser.Local_domain_index_clauseContext ctx) {
        throwNotImplemented(); // super.enterLocal_domain_index_clause(ctx);
    }

    @Override
    public void exitLocal_domain_index_clause(PlSqlParser.Local_domain_index_clauseContext ctx) {
        super.exitLocal_domain_index_clause(ctx);
    }

    @Override
    public void enterXmlindex_clause(PlSqlParser.Xmlindex_clauseContext ctx) {
        throwNotImplemented(); // super.enterXmlindex_clause(ctx);
    }

    @Override
    public void exitXmlindex_clause(PlSqlParser.Xmlindex_clauseContext ctx) {
        super.exitXmlindex_clause(ctx);
    }

    @Override
    public void enterLocal_xmlindex_clause(PlSqlParser.Local_xmlindex_clauseContext ctx) {
        throwNotImplemented(); // super.enterLocal_xmlindex_clause(ctx);
    }

    @Override
    public void exitLocal_xmlindex_clause(PlSqlParser.Local_xmlindex_clauseContext ctx) {
        super.exitLocal_xmlindex_clause(ctx);
    }

    @Override
    public void enterGlobal_partitioned_index(PlSqlParser.Global_partitioned_indexContext ctx) {
        throwNotImplemented(); // super.enterGlobal_partitioned_index(ctx);
    }

    @Override
    public void exitGlobal_partitioned_index(PlSqlParser.Global_partitioned_indexContext ctx) {
        super.exitGlobal_partitioned_index(ctx);
    }

    @Override
    public void enterIndex_partitioning_clause(PlSqlParser.Index_partitioning_clauseContext ctx) {
        throwNotImplemented(); // super.enterIndex_partitioning_clause(ctx);
    }

    @Override
    public void exitIndex_partitioning_clause(PlSqlParser.Index_partitioning_clauseContext ctx) {
        super.exitIndex_partitioning_clause(ctx);
    }

    @Override
    public void enterLocal_partitioned_index(PlSqlParser.Local_partitioned_indexContext ctx) {
        throwNotImplemented(); // super.enterLocal_partitioned_index(ctx);
    }

    @Override
    public void exitLocal_partitioned_index(PlSqlParser.Local_partitioned_indexContext ctx) {
        super.exitLocal_partitioned_index(ctx);
    }

    @Override
    public void enterOn_range_partitioned_table(PlSqlParser.On_range_partitioned_tableContext ctx) {
        throwNotImplemented(); // super.enterOn_range_partitioned_table(ctx);
    }

    @Override
    public void exitOn_range_partitioned_table(PlSqlParser.On_range_partitioned_tableContext ctx) {
        super.exitOn_range_partitioned_table(ctx);
    }

    @Override
    public void enterOn_list_partitioned_table(PlSqlParser.On_list_partitioned_tableContext ctx) {
        throwNotImplemented(); // super.enterOn_list_partitioned_table(ctx);
    }

    @Override
    public void exitOn_list_partitioned_table(PlSqlParser.On_list_partitioned_tableContext ctx) {
        super.exitOn_list_partitioned_table(ctx);
    }

    @Override
    public void enterOn_hash_partitioned_table(PlSqlParser.On_hash_partitioned_tableContext ctx) {
        throwNotImplemented(); // super.enterOn_hash_partitioned_table(ctx);
    }

    @Override
    public void exitOn_hash_partitioned_table(PlSqlParser.On_hash_partitioned_tableContext ctx) {
        super.exitOn_hash_partitioned_table(ctx);
    }

    @Override
    public void enterOn_comp_partitioned_table(PlSqlParser.On_comp_partitioned_tableContext ctx) {
        throwNotImplemented(); // super.enterOn_comp_partitioned_table(ctx);
    }

    @Override
    public void exitOn_comp_partitioned_table(PlSqlParser.On_comp_partitioned_tableContext ctx) {
        super.exitOn_comp_partitioned_table(ctx);
    }

    @Override
    public void enterIndex_subpartition_clause(PlSqlParser.Index_subpartition_clauseContext ctx) {
        throwNotImplemented(); // super.enterIndex_subpartition_clause(ctx);
    }

    @Override
    public void exitIndex_subpartition_clause(PlSqlParser.Index_subpartition_clauseContext ctx) {
        super.exitIndex_subpartition_clause(ctx);
    }

    @Override
    public void enterOdci_parameters(PlSqlParser.Odci_parametersContext ctx) {
        throwNotImplemented(); // super.enterOdci_parameters(ctx);
    }

    @Override
    public void exitOdci_parameters(PlSqlParser.Odci_parametersContext ctx) {
        super.exitOdci_parameters(ctx);
    }

    @Override
    public void enterIndextype(PlSqlParser.IndextypeContext ctx) {
        throwNotImplemented(); // super.enterIndextype(ctx);
    }

    @Override
    public void exitIndextype(PlSqlParser.IndextypeContext ctx) {
        super.exitIndextype(ctx);
    }

    @Override
    public void enterAlter_index(PlSqlParser.Alter_indexContext ctx) {
        throwNotImplemented(); // super.enterAlter_index(ctx);
    }

    @Override
    public void exitAlter_index(PlSqlParser.Alter_indexContext ctx) {
        super.exitAlter_index(ctx);
    }

    @Override
    public void enterAlter_index_ops_set1(PlSqlParser.Alter_index_ops_set1Context ctx) {
        throwNotImplemented(); // super.enterAlter_index_ops_set1(ctx);
    }

    @Override
    public void exitAlter_index_ops_set1(PlSqlParser.Alter_index_ops_set1Context ctx) {
        super.exitAlter_index_ops_set1(ctx);
    }

    @Override
    public void enterAlter_index_ops_set2(PlSqlParser.Alter_index_ops_set2Context ctx) {
        throwNotImplemented(); // super.enterAlter_index_ops_set2(ctx);
    }

    @Override
    public void exitAlter_index_ops_set2(PlSqlParser.Alter_index_ops_set2Context ctx) {
        super.exitAlter_index_ops_set2(ctx);
    }

    @Override
    public void enterVisible_or_invisible(PlSqlParser.Visible_or_invisibleContext ctx) {
        throwNotImplemented(); // super.enterVisible_or_invisible(ctx);
    }

    @Override
    public void exitVisible_or_invisible(PlSqlParser.Visible_or_invisibleContext ctx) {
        super.exitVisible_or_invisible(ctx);
    }

    @Override
    public void enterMonitoring_nomonitoring(PlSqlParser.Monitoring_nomonitoringContext ctx) {
        throwNotImplemented(); // super.enterMonitoring_nomonitoring(ctx);
    }

    @Override
    public void exitMonitoring_nomonitoring(PlSqlParser.Monitoring_nomonitoringContext ctx) {
        super.exitMonitoring_nomonitoring(ctx);
    }

    @Override
    public void enterRebuild_clause(PlSqlParser.Rebuild_clauseContext ctx) {
        throwNotImplemented(); // super.enterRebuild_clause(ctx);
    }

    @Override
    public void exitRebuild_clause(PlSqlParser.Rebuild_clauseContext ctx) {
        super.exitRebuild_clause(ctx);
    }

    @Override
    public void enterAlter_index_partitioning(PlSqlParser.Alter_index_partitioningContext ctx) {
        throwNotImplemented(); // super.enterAlter_index_partitioning(ctx);
    }

    @Override
    public void exitAlter_index_partitioning(PlSqlParser.Alter_index_partitioningContext ctx) {
        super.exitAlter_index_partitioning(ctx);
    }

    @Override
    public void enterModify_index_default_attrs(PlSqlParser.Modify_index_default_attrsContext ctx) {
        throwNotImplemented(); // super.enterModify_index_default_attrs(ctx);
    }

    @Override
    public void exitModify_index_default_attrs(PlSqlParser.Modify_index_default_attrsContext ctx) {
        super.exitModify_index_default_attrs(ctx);
    }

    @Override
    public void enterAdd_hash_index_partition(PlSqlParser.Add_hash_index_partitionContext ctx) {
        throwNotImplemented(); // super.enterAdd_hash_index_partition(ctx);
    }

    @Override
    public void exitAdd_hash_index_partition(PlSqlParser.Add_hash_index_partitionContext ctx) {
        super.exitAdd_hash_index_partition(ctx);
    }

    @Override
    public void enterCoalesce_index_partition(PlSqlParser.Coalesce_index_partitionContext ctx) {
        throwNotImplemented(); // super.enterCoalesce_index_partition(ctx);
    }

    @Override
    public void exitCoalesce_index_partition(PlSqlParser.Coalesce_index_partitionContext ctx) {
        super.exitCoalesce_index_partition(ctx);
    }

    @Override
    public void enterModify_index_partition(PlSqlParser.Modify_index_partitionContext ctx) {
        throwNotImplemented(); // super.enterModify_index_partition(ctx);
    }

    @Override
    public void exitModify_index_partition(PlSqlParser.Modify_index_partitionContext ctx) {
        super.exitModify_index_partition(ctx);
    }

    @Override
    public void enterModify_index_partitions_ops(PlSqlParser.Modify_index_partitions_opsContext ctx) {
        throwNotImplemented(); // super.enterModify_index_partitions_ops(ctx);
    }

    @Override
    public void exitModify_index_partitions_ops(PlSqlParser.Modify_index_partitions_opsContext ctx) {
        super.exitModify_index_partitions_ops(ctx);
    }

    @Override
    public void enterRename_index_partition(PlSqlParser.Rename_index_partitionContext ctx) {
        throwNotImplemented(); // super.enterRename_index_partition(ctx);
    }

    @Override
    public void exitRename_index_partition(PlSqlParser.Rename_index_partitionContext ctx) {
        super.exitRename_index_partition(ctx);
    }

    @Override
    public void enterDrop_index_partition(PlSqlParser.Drop_index_partitionContext ctx) {
        throwNotImplemented(); // super.enterDrop_index_partition(ctx);
    }

    @Override
    public void exitDrop_index_partition(PlSqlParser.Drop_index_partitionContext ctx) {
        super.exitDrop_index_partition(ctx);
    }

    @Override
    public void enterSplit_index_partition(PlSqlParser.Split_index_partitionContext ctx) {
        throwNotImplemented(); // super.enterSplit_index_partition(ctx);
    }

    @Override
    public void exitSplit_index_partition(PlSqlParser.Split_index_partitionContext ctx) {
        super.exitSplit_index_partition(ctx);
    }

    @Override
    public void enterIndex_partition_description(PlSqlParser.Index_partition_descriptionContext ctx) {
        throwNotImplemented(); // super.enterIndex_partition_description(ctx);
    }

    @Override
    public void exitIndex_partition_description(PlSqlParser.Index_partition_descriptionContext ctx) {
        super.exitIndex_partition_description(ctx);
    }

    @Override
    public void enterModify_index_subpartition(PlSqlParser.Modify_index_subpartitionContext ctx) {
        throwNotImplemented(); // super.enterModify_index_subpartition(ctx);
    }

    @Override
    public void exitModify_index_subpartition(PlSqlParser.Modify_index_subpartitionContext ctx) {
        super.exitModify_index_subpartition(ctx);
    }

    @Override
    public void enterPartition_name_old(PlSqlParser.Partition_name_oldContext ctx) {
        throwNotImplemented(); // super.enterPartition_name_old(ctx);
    }

    @Override
    public void exitPartition_name_old(PlSqlParser.Partition_name_oldContext ctx) {
        super.exitPartition_name_old(ctx);
    }

    @Override
    public void enterNew_partition_name(PlSqlParser.New_partition_nameContext ctx) {
        throwNotImplemented(); // super.enterNew_partition_name(ctx);
    }

    @Override
    public void exitNew_partition_name(PlSqlParser.New_partition_nameContext ctx) {
        super.exitNew_partition_name(ctx);
    }

    @Override
    public void enterNew_index_name(PlSqlParser.New_index_nameContext ctx) {
        throwNotImplemented(); // super.enterNew_index_name(ctx);
    }

    @Override
    public void exitNew_index_name(PlSqlParser.New_index_nameContext ctx) {
        super.exitNew_index_name(ctx);
    }

    @Override
    public void enterCreate_user(PlSqlParser.Create_userContext ctx) {
        throwNotImplemented(); // super.enterCreate_user(ctx);
    }

    @Override
    public void exitCreate_user(PlSqlParser.Create_userContext ctx) {
        super.exitCreate_user(ctx);
    }

    @Override
    public void enterAlter_user(PlSqlParser.Alter_userContext ctx) {
        throwNotImplemented(); // super.enterAlter_user(ctx);
    }

    @Override
    public void exitAlter_user(PlSqlParser.Alter_userContext ctx) {
        super.exitAlter_user(ctx);
    }

    @Override
    public void enterAlter_identified_by(PlSqlParser.Alter_identified_byContext ctx) {
        throwNotImplemented(); // super.enterAlter_identified_by(ctx);
    }

    @Override
    public void exitAlter_identified_by(PlSqlParser.Alter_identified_byContext ctx) {
        super.exitAlter_identified_by(ctx);
    }

    @Override
    public void enterIdentified_by(PlSqlParser.Identified_byContext ctx) {
        throwNotImplemented(); // super.enterIdentified_by(ctx);
    }

    @Override
    public void exitIdentified_by(PlSqlParser.Identified_byContext ctx) {
        super.exitIdentified_by(ctx);
    }

    @Override
    public void enterIdentified_other_clause(PlSqlParser.Identified_other_clauseContext ctx) {
        throwNotImplemented(); // super.enterIdentified_other_clause(ctx);
    }

    @Override
    public void exitIdentified_other_clause(PlSqlParser.Identified_other_clauseContext ctx) {
        super.exitIdentified_other_clause(ctx);
    }

    @Override
    public void enterUser_tablespace_clause(PlSqlParser.User_tablespace_clauseContext ctx) {
        throwNotImplemented(); // super.enterUser_tablespace_clause(ctx);
    }

    @Override
    public void exitUser_tablespace_clause(PlSqlParser.User_tablespace_clauseContext ctx) {
        super.exitUser_tablespace_clause(ctx);
    }

    @Override
    public void enterQuota_clause(PlSqlParser.Quota_clauseContext ctx) {
        throwNotImplemented(); // super.enterQuota_clause(ctx);
    }

    @Override
    public void exitQuota_clause(PlSqlParser.Quota_clauseContext ctx) {
        super.exitQuota_clause(ctx);
    }

    @Override
    public void enterProfile_clause(PlSqlParser.Profile_clauseContext ctx) {
        throwNotImplemented(); // super.enterProfile_clause(ctx);
    }

    @Override
    public void exitProfile_clause(PlSqlParser.Profile_clauseContext ctx) {
        super.exitProfile_clause(ctx);
    }

    @Override
    public void enterRole_clause(PlSqlParser.Role_clauseContext ctx) {
        throwNotImplemented(); // super.enterRole_clause(ctx);
    }

    @Override
    public void exitRole_clause(PlSqlParser.Role_clauseContext ctx) {
        super.exitRole_clause(ctx);
    }

    @Override
    public void enterUser_default_role_clause(PlSqlParser.User_default_role_clauseContext ctx) {
        throwNotImplemented(); // super.enterUser_default_role_clause(ctx);
    }

    @Override
    public void exitUser_default_role_clause(PlSqlParser.User_default_role_clauseContext ctx) {
        super.exitUser_default_role_clause(ctx);
    }

    @Override
    public void enterPassword_expire_clause(PlSqlParser.Password_expire_clauseContext ctx) {
        throwNotImplemented(); // super.enterPassword_expire_clause(ctx);
    }

    @Override
    public void exitPassword_expire_clause(PlSqlParser.Password_expire_clauseContext ctx) {
        super.exitPassword_expire_clause(ctx);
    }

    @Override
    public void enterUser_lock_clause(PlSqlParser.User_lock_clauseContext ctx) {
        throwNotImplemented(); // super.enterUser_lock_clause(ctx);
    }

    @Override
    public void exitUser_lock_clause(PlSqlParser.User_lock_clauseContext ctx) {
        super.exitUser_lock_clause(ctx);
    }

    @Override
    public void enterUser_editions_clause(PlSqlParser.User_editions_clauseContext ctx) {
        throwNotImplemented(); // super.enterUser_editions_clause(ctx);
    }

    @Override
    public void exitUser_editions_clause(PlSqlParser.User_editions_clauseContext ctx) {
        super.exitUser_editions_clause(ctx);
    }

    @Override
    public void enterAlter_user_editions_clause(PlSqlParser.Alter_user_editions_clauseContext ctx) {
        throwNotImplemented(); // super.enterAlter_user_editions_clause(ctx);
    }

    @Override
    public void exitAlter_user_editions_clause(PlSqlParser.Alter_user_editions_clauseContext ctx) {
        super.exitAlter_user_editions_clause(ctx);
    }

    @Override
    public void enterProxy_clause(PlSqlParser.Proxy_clauseContext ctx) {
        throwNotImplemented(); // super.enterProxy_clause(ctx);
    }

    @Override
    public void exitProxy_clause(PlSqlParser.Proxy_clauseContext ctx) {
        super.exitProxy_clause(ctx);
    }

    @Override
    public void enterContainer_names(PlSqlParser.Container_namesContext ctx) {
        throwNotImplemented(); // super.enterContainer_names(ctx);
    }

    @Override
    public void exitContainer_names(PlSqlParser.Container_namesContext ctx) {
        super.exitContainer_names(ctx);
    }

    @Override
    public void enterSet_container_data(PlSqlParser.Set_container_dataContext ctx) {
        throwNotImplemented(); // super.enterSet_container_data(ctx);
    }

    @Override
    public void exitSet_container_data(PlSqlParser.Set_container_dataContext ctx) {
        super.exitSet_container_data(ctx);
    }

    @Override
    public void enterAdd_rem_container_data(PlSqlParser.Add_rem_container_dataContext ctx) {
        throwNotImplemented(); // super.enterAdd_rem_container_data(ctx);
    }

    @Override
    public void exitAdd_rem_container_data(PlSqlParser.Add_rem_container_dataContext ctx) {
        super.exitAdd_rem_container_data(ctx);
    }

    @Override
    public void enterContainer_data_clause(PlSqlParser.Container_data_clauseContext ctx) {
        throwNotImplemented(); // super.enterContainer_data_clause(ctx);
    }

    @Override
    public void exitContainer_data_clause(PlSqlParser.Container_data_clauseContext ctx) {
        super.exitContainer_data_clause(ctx);
    }

    @Override
    public void enterAnalyze(PlSqlParser.AnalyzeContext ctx) {
        throwNotImplemented(); // super.enterAnalyze(ctx);
    }

    @Override
    public void exitAnalyze(PlSqlParser.AnalyzeContext ctx) {
        super.exitAnalyze(ctx);
    }

    @Override
    public void enterPartition_extention_clause(PlSqlParser.Partition_extention_clauseContext ctx) {
        throwNotImplemented(); // super.enterPartition_extention_clause(ctx);
    }

    @Override
    public void exitPartition_extention_clause(PlSqlParser.Partition_extention_clauseContext ctx) {
        super.exitPartition_extention_clause(ctx);
    }

    @Override
    public void enterValidation_clauses(PlSqlParser.Validation_clausesContext ctx) {
        throwNotImplemented(); // super.enterValidation_clauses(ctx);
    }

    @Override
    public void exitValidation_clauses(PlSqlParser.Validation_clausesContext ctx) {
        super.exitValidation_clauses(ctx);
    }

    @Override
    public void enterOnline_or_offline(PlSqlParser.Online_or_offlineContext ctx) {
        throwNotImplemented(); // super.enterOnline_or_offline(ctx);
    }

    @Override
    public void exitOnline_or_offline(PlSqlParser.Online_or_offlineContext ctx) {
        super.exitOnline_or_offline(ctx);
    }

    @Override
    public void enterInto_clause1(PlSqlParser.Into_clause1Context ctx) {
        throwNotImplemented(); // super.enterInto_clause1(ctx);
    }

    @Override
    public void exitInto_clause1(PlSqlParser.Into_clause1Context ctx) {
        super.exitInto_clause1(ctx);
    }

    @Override
    public void enterPartition_key_value(PlSqlParser.Partition_key_valueContext ctx) {
        throwNotImplemented(); // super.enterPartition_key_value(ctx);
    }

    @Override
    public void exitPartition_key_value(PlSqlParser.Partition_key_valueContext ctx) {
        super.exitPartition_key_value(ctx);
    }

    @Override
    public void enterSubpartition_key_value(PlSqlParser.Subpartition_key_valueContext ctx) {
        throwNotImplemented(); // super.enterSubpartition_key_value(ctx);
    }

    @Override
    public void exitSubpartition_key_value(PlSqlParser.Subpartition_key_valueContext ctx) {
        super.exitSubpartition_key_value(ctx);
    }

    @Override
    public void enterAssociate_statistics(PlSqlParser.Associate_statisticsContext ctx) {
        throwNotImplemented(); // super.enterAssociate_statistics(ctx);
    }

    @Override
    public void exitAssociate_statistics(PlSqlParser.Associate_statisticsContext ctx) {
        super.exitAssociate_statistics(ctx);
    }

    @Override
    public void enterColumn_association(PlSqlParser.Column_associationContext ctx) {
        throwNotImplemented(); // super.enterColumn_association(ctx);
    }

    @Override
    public void exitColumn_association(PlSqlParser.Column_associationContext ctx) {
        super.exitColumn_association(ctx);
    }

    @Override
    public void enterFunction_association(PlSqlParser.Function_associationContext ctx) {
        throwNotImplemented(); // super.enterFunction_association(ctx);
    }

    @Override
    public void exitFunction_association(PlSqlParser.Function_associationContext ctx) {
        super.exitFunction_association(ctx);
    }

    @Override
    public void enterIndextype_name(PlSqlParser.Indextype_nameContext ctx) {
        throwNotImplemented(); // super.enterIndextype_name(ctx);
    }

    @Override
    public void exitIndextype_name(PlSqlParser.Indextype_nameContext ctx) {
        super.exitIndextype_name(ctx);
    }

    @Override
    public void enterUsing_statistics_type(PlSqlParser.Using_statistics_typeContext ctx) {
        throwNotImplemented(); // super.enterUsing_statistics_type(ctx);
    }

    @Override
    public void exitUsing_statistics_type(PlSqlParser.Using_statistics_typeContext ctx) {
        super.exitUsing_statistics_type(ctx);
    }

    @Override
    public void enterStatistics_type_name(PlSqlParser.Statistics_type_nameContext ctx) {
        throwNotImplemented(); // super.enterStatistics_type_name(ctx);
    }

    @Override
    public void exitStatistics_type_name(PlSqlParser.Statistics_type_nameContext ctx) {
        super.exitStatistics_type_name(ctx);
    }

    @Override
    public void enterDefault_cost_clause(PlSqlParser.Default_cost_clauseContext ctx) {
        throwNotImplemented(); // super.enterDefault_cost_clause(ctx);
    }

    @Override
    public void exitDefault_cost_clause(PlSqlParser.Default_cost_clauseContext ctx) {
        super.exitDefault_cost_clause(ctx);
    }

    @Override
    public void enterCpu_cost(PlSqlParser.Cpu_costContext ctx) {
        throwNotImplemented(); // super.enterCpu_cost(ctx);
    }

    @Override
    public void exitCpu_cost(PlSqlParser.Cpu_costContext ctx) {
        super.exitCpu_cost(ctx);
    }

    @Override
    public void enterIo_cost(PlSqlParser.Io_costContext ctx) {
        throwNotImplemented(); // super.enterIo_cost(ctx);
    }

    @Override
    public void exitIo_cost(PlSqlParser.Io_costContext ctx) {
        super.exitIo_cost(ctx);
    }

    @Override
    public void enterNetwork_cost(PlSqlParser.Network_costContext ctx) {
        throwNotImplemented(); // super.enterNetwork_cost(ctx);
    }

    @Override
    public void exitNetwork_cost(PlSqlParser.Network_costContext ctx) {
        super.exitNetwork_cost(ctx);
    }

    @Override
    public void enterDefault_selectivity_clause(PlSqlParser.Default_selectivity_clauseContext ctx) {
        throwNotImplemented(); // super.enterDefault_selectivity_clause(ctx);
    }

    @Override
    public void exitDefault_selectivity_clause(PlSqlParser.Default_selectivity_clauseContext ctx) {
        super.exitDefault_selectivity_clause(ctx);
    }

    @Override
    public void enterDefault_selectivity(PlSqlParser.Default_selectivityContext ctx) {
        throwNotImplemented(); // super.enterDefault_selectivity(ctx);
    }

    @Override
    public void exitDefault_selectivity(PlSqlParser.Default_selectivityContext ctx) {
        super.exitDefault_selectivity(ctx);
    }

    @Override
    public void enterStorage_table_clause(PlSqlParser.Storage_table_clauseContext ctx) {
        throwNotImplemented(); // super.enterStorage_table_clause(ctx);
    }

    @Override
    public void exitStorage_table_clause(PlSqlParser.Storage_table_clauseContext ctx) {
        super.exitStorage_table_clause(ctx);
    }

    @Override
    public void enterUnified_auditing(PlSqlParser.Unified_auditingContext ctx) {
        throwNotImplemented(); // super.enterUnified_auditing(ctx);
    }

    @Override
    public void exitUnified_auditing(PlSqlParser.Unified_auditingContext ctx) {
        super.exitUnified_auditing(ctx);
    }

    @Override
    public void enterPolicy_name(PlSqlParser.Policy_nameContext ctx) {
        throwNotImplemented(); // super.enterPolicy_name(ctx);
    }

    @Override
    public void exitPolicy_name(PlSqlParser.Policy_nameContext ctx) {
        super.exitPolicy_name(ctx);
    }

    @Override
    public void enterAudit_traditional(PlSqlParser.Audit_traditionalContext ctx) {
        throwNotImplemented(); // super.enterAudit_traditional(ctx);
    }

    @Override
    public void exitAudit_traditional(PlSqlParser.Audit_traditionalContext ctx) {
        super.exitAudit_traditional(ctx);
    }

    @Override
    public void enterAudit_direct_path(PlSqlParser.Audit_direct_pathContext ctx) {
        throwNotImplemented(); // super.enterAudit_direct_path(ctx);
    }

    @Override
    public void exitAudit_direct_path(PlSqlParser.Audit_direct_pathContext ctx) {
        super.exitAudit_direct_path(ctx);
    }

    @Override
    public void enterAudit_container_clause(PlSqlParser.Audit_container_clauseContext ctx) {
        throwNotImplemented(); // super.enterAudit_container_clause(ctx);
    }

    @Override
    public void exitAudit_container_clause(PlSqlParser.Audit_container_clauseContext ctx) {
        super.exitAudit_container_clause(ctx);
    }

    @Override
    public void enterAudit_operation_clause(PlSqlParser.Audit_operation_clauseContext ctx) {
        throwNotImplemented(); // super.enterAudit_operation_clause(ctx);
    }

    @Override
    public void exitAudit_operation_clause(PlSqlParser.Audit_operation_clauseContext ctx) {
        super.exitAudit_operation_clause(ctx);
    }

    @Override
    public void enterAuditing_by_clause(PlSqlParser.Auditing_by_clauseContext ctx) {
        throwNotImplemented(); // super.enterAuditing_by_clause(ctx);
    }

    @Override
    public void exitAuditing_by_clause(PlSqlParser.Auditing_by_clauseContext ctx) {
        super.exitAuditing_by_clause(ctx);
    }

    @Override
    public void enterAudit_user(PlSqlParser.Audit_userContext ctx) {
        throwNotImplemented(); // super.enterAudit_user(ctx);
    }

    @Override
    public void exitAudit_user(PlSqlParser.Audit_userContext ctx) {
        super.exitAudit_user(ctx);
    }

    @Override
    public void enterAudit_schema_object_clause(PlSqlParser.Audit_schema_object_clauseContext ctx) {
        throwNotImplemented(); // super.enterAudit_schema_object_clause(ctx);
    }

    @Override
    public void exitAudit_schema_object_clause(PlSqlParser.Audit_schema_object_clauseContext ctx) {
        super.exitAudit_schema_object_clause(ctx);
    }

    @Override
    public void enterSql_operation(PlSqlParser.Sql_operationContext ctx) {
        throwNotImplemented(); // super.enterSql_operation(ctx);
    }

    @Override
    public void exitSql_operation(PlSqlParser.Sql_operationContext ctx) {
        super.exitSql_operation(ctx);
    }

    @Override
    public void enterAuditing_on_clause(PlSqlParser.Auditing_on_clauseContext ctx) {
        throwNotImplemented(); // super.enterAuditing_on_clause(ctx);
    }

    @Override
    public void exitAuditing_on_clause(PlSqlParser.Auditing_on_clauseContext ctx) {
        super.exitAuditing_on_clause(ctx);
    }

    @Override
    public void enterModel_name(PlSqlParser.Model_nameContext ctx) {
        throwNotImplemented(); // super.enterModel_name(ctx);
    }

    @Override
    public void exitModel_name(PlSqlParser.Model_nameContext ctx) {
        super.exitModel_name(ctx);
    }

    @Override
    public void enterObject_name(PlSqlParser.Object_nameContext ctx) {
        throwNotImplemented(); // super.enterObject_name(ctx);
    }

    @Override
    public void exitObject_name(PlSqlParser.Object_nameContext ctx) {
        super.exitObject_name(ctx);
    }

    @Override
    public void enterProfile_name(PlSqlParser.Profile_nameContext ctx) {
        throwNotImplemented(); // super.enterProfile_name(ctx);
    }

    @Override
    public void exitProfile_name(PlSqlParser.Profile_nameContext ctx) {
        super.exitProfile_name(ctx);
    }

    @Override
    public void enterSql_statement_shortcut(PlSqlParser.Sql_statement_shortcutContext ctx) {
        throwNotImplemented(); // super.enterSql_statement_shortcut(ctx);
    }

    @Override
    public void exitSql_statement_shortcut(PlSqlParser.Sql_statement_shortcutContext ctx) {
        super.exitSql_statement_shortcut(ctx);
    }

    @Override
    public void enterDrop_index(PlSqlParser.Drop_indexContext ctx) {
        throwNotImplemented(); // super.enterDrop_index(ctx);
    }

    @Override
    public void exitDrop_index(PlSqlParser.Drop_indexContext ctx) {
        super.exitDrop_index(ctx);
    }

    @Override
    public void enterGrant_statement(PlSqlParser.Grant_statementContext ctx) {
        throwNotImplemented(); // super.enterGrant_statement(ctx);
    }

    @Override
    public void exitGrant_statement(PlSqlParser.Grant_statementContext ctx) {
        super.exitGrant_statement(ctx);
    }

    @Override
    public void enterContainer_clause(PlSqlParser.Container_clauseContext ctx) {
        throwNotImplemented(); // super.enterContainer_clause(ctx);
    }

    @Override
    public void exitContainer_clause(PlSqlParser.Container_clauseContext ctx) {
        super.exitContainer_clause(ctx);
    }

    @Override
    public void enterCreate_directory(PlSqlParser.Create_directoryContext ctx) {
        throwNotImplemented(); // super.enterCreate_directory(ctx);
    }

    @Override
    public void exitCreate_directory(PlSqlParser.Create_directoryContext ctx) {
        super.exitCreate_directory(ctx);
    }

    @Override
    public void enterDirectory_name(PlSqlParser.Directory_nameContext ctx) {
        throwNotImplemented(); // super.enterDirectory_name(ctx);
    }

    @Override
    public void exitDirectory_name(PlSqlParser.Directory_nameContext ctx) {
        super.exitDirectory_name(ctx);
    }

    @Override
    public void enterDirectory_path(PlSqlParser.Directory_pathContext ctx) {
        throwNotImplemented(); // super.enterDirectory_path(ctx);
    }

    @Override
    public void exitDirectory_path(PlSqlParser.Directory_pathContext ctx) {
        super.exitDirectory_path(ctx);
    }

    @Override
    public void enterAlter_library(PlSqlParser.Alter_libraryContext ctx) {
        throwNotImplemented(); // super.enterAlter_library(ctx);
    }

    @Override
    public void exitAlter_library(PlSqlParser.Alter_libraryContext ctx) {
        super.exitAlter_library(ctx);
    }

    @Override
    public void enterLibrary_editionable(PlSqlParser.Library_editionableContext ctx) {
        throwNotImplemented(); // super.enterLibrary_editionable(ctx);
    }

    @Override
    public void exitLibrary_editionable(PlSqlParser.Library_editionableContext ctx) {
        super.exitLibrary_editionable(ctx);
    }

    @Override
    public void enterLibrary_debug(PlSqlParser.Library_debugContext ctx) {
        throwNotImplemented(); // super.enterLibrary_debug(ctx);
    }

    @Override
    public void exitLibrary_debug(PlSqlParser.Library_debugContext ctx) {
        super.exitLibrary_debug(ctx);
    }

    @Override
    public void enterCompiler_parameters_clause(PlSqlParser.Compiler_parameters_clauseContext ctx) {
        throwNotImplemented(); // super.enterCompiler_parameters_clause(ctx);
    }

    @Override
    public void exitCompiler_parameters_clause(PlSqlParser.Compiler_parameters_clauseContext ctx) {
        super.exitCompiler_parameters_clause(ctx);
    }

    @Override
    public void enterParameter_value(PlSqlParser.Parameter_valueContext ctx) {
        throwNotImplemented(); // super.enterParameter_value(ctx);
    }

    @Override
    public void exitParameter_value(PlSqlParser.Parameter_valueContext ctx) {
        super.exitParameter_value(ctx);
    }

    @Override
    public void enterLibrary_name(PlSqlParser.Library_nameContext ctx) {
        throwNotImplemented(); // super.enterLibrary_name(ctx);
    }

    @Override
    public void exitLibrary_name(PlSqlParser.Library_nameContext ctx) {
        super.exitLibrary_name(ctx);
    }

    @Override
    public void enterAlter_view(PlSqlParser.Alter_viewContext ctx) {
        throwNotImplemented(); // super.enterAlter_view(ctx);
    }

    @Override
    public void exitAlter_view(PlSqlParser.Alter_viewContext ctx) {
        super.exitAlter_view(ctx);
    }

    @Override
    public void enterAlter_view_editionable(PlSqlParser.Alter_view_editionableContext ctx) {
        throwNotImplemented(); // super.enterAlter_view_editionable(ctx);
    }

    @Override
    public void exitAlter_view_editionable(PlSqlParser.Alter_view_editionableContext ctx) {
        super.exitAlter_view_editionable(ctx);
    }

    @Override
    public void enterCreate_view(PlSqlParser.Create_viewContext ctx) {
        throwNotImplemented(); // super.enterCreate_view(ctx);
    }

    @Override
    public void exitCreate_view(PlSqlParser.Create_viewContext ctx) {
        super.exitCreate_view(ctx);
    }

    @Override
    public void enterView_options(PlSqlParser.View_optionsContext ctx) {
        throwNotImplemented(); // super.enterView_options(ctx);
    }

    @Override
    public void exitView_options(PlSqlParser.View_optionsContext ctx) {
        super.exitView_options(ctx);
    }

    @Override
    public void enterView_alias_constraint(PlSqlParser.View_alias_constraintContext ctx) {
        throwNotImplemented(); // super.enterView_alias_constraint(ctx);
    }

    @Override
    public void exitView_alias_constraint(PlSqlParser.View_alias_constraintContext ctx) {
        super.exitView_alias_constraint(ctx);
    }

    @Override
    public void enterObject_view_clause(PlSqlParser.Object_view_clauseContext ctx) {
        throwNotImplemented(); // super.enterObject_view_clause(ctx);
    }

    @Override
    public void exitObject_view_clause(PlSqlParser.Object_view_clauseContext ctx) {
        super.exitObject_view_clause(ctx);
    }

    @Override
    public void enterInline_constraint(PlSqlParser.Inline_constraintContext ctx) {
        throwNotImplemented(); // super.enterInline_constraint(ctx);
    }

    @Override
    public void exitInline_constraint(PlSqlParser.Inline_constraintContext ctx) {
        super.exitInline_constraint(ctx);
    }

    @Override
    public void enterInline_ref_constraint(PlSqlParser.Inline_ref_constraintContext ctx) {
        throwNotImplemented(); // super.enterInline_ref_constraint(ctx);
    }

    @Override
    public void exitInline_ref_constraint(PlSqlParser.Inline_ref_constraintContext ctx) {
        super.exitInline_ref_constraint(ctx);
    }

    @Override
    public void enterOut_of_line_ref_constraint(PlSqlParser.Out_of_line_ref_constraintContext ctx) {
        throwNotImplemented(); // super.enterOut_of_line_ref_constraint(ctx);
    }

    @Override
    public void exitOut_of_line_ref_constraint(PlSqlParser.Out_of_line_ref_constraintContext ctx) {
        super.exitOut_of_line_ref_constraint(ctx);
    }

    @Override
    public void enterOut_of_line_constraint(PlSqlParser.Out_of_line_constraintContext ctx) {
        throwNotImplemented(); // super.enterOut_of_line_constraint(ctx);
    }

    @Override
    public void exitOut_of_line_constraint(PlSqlParser.Out_of_line_constraintContext ctx) {
        super.exitOut_of_line_constraint(ctx);
    }

    @Override
    public void enterConstraint_state(PlSqlParser.Constraint_stateContext ctx) {
        throwNotImplemented(); // super.enterConstraint_state(ctx);
    }

    @Override
    public void exitConstraint_state(PlSqlParser.Constraint_stateContext ctx) {
        super.exitConstraint_state(ctx);
    }

    @Override
    public void enterAlter_tablespace(PlSqlParser.Alter_tablespaceContext ctx) {
        throwNotImplemented(); // super.enterAlter_tablespace(ctx);
    }

    @Override
    public void exitAlter_tablespace(PlSqlParser.Alter_tablespaceContext ctx) {
        super.exitAlter_tablespace(ctx);
    }

    @Override
    public void enterDatafile_tempfile_clauses(PlSqlParser.Datafile_tempfile_clausesContext ctx) {
        throwNotImplemented(); // super.enterDatafile_tempfile_clauses(ctx);
    }

    @Override
    public void exitDatafile_tempfile_clauses(PlSqlParser.Datafile_tempfile_clausesContext ctx) {
        super.exitDatafile_tempfile_clauses(ctx);
    }

    @Override
    public void enterTablespace_logging_clauses(PlSqlParser.Tablespace_logging_clausesContext ctx) {
        throwNotImplemented(); // super.enterTablespace_logging_clauses(ctx);
    }

    @Override
    public void exitTablespace_logging_clauses(PlSqlParser.Tablespace_logging_clausesContext ctx) {
        super.exitTablespace_logging_clauses(ctx);
    }

    @Override
    public void enterTablespace_group_clause(PlSqlParser.Tablespace_group_clauseContext ctx) {
        throwNotImplemented(); // super.enterTablespace_group_clause(ctx);
    }

    @Override
    public void exitTablespace_group_clause(PlSqlParser.Tablespace_group_clauseContext ctx) {
        super.exitTablespace_group_clause(ctx);
    }

    @Override
    public void enterTablespace_group_name(PlSqlParser.Tablespace_group_nameContext ctx) {
        throwNotImplemented(); // super.enterTablespace_group_name(ctx);
    }

    @Override
    public void exitTablespace_group_name(PlSqlParser.Tablespace_group_nameContext ctx) {
        super.exitTablespace_group_name(ctx);
    }

    @Override
    public void enterTablespace_state_clauses(PlSqlParser.Tablespace_state_clausesContext ctx) {
        throwNotImplemented(); // super.enterTablespace_state_clauses(ctx);
    }

    @Override
    public void exitTablespace_state_clauses(PlSqlParser.Tablespace_state_clausesContext ctx) {
        super.exitTablespace_state_clauses(ctx);
    }

    @Override
    public void enterFlashback_mode_clause(PlSqlParser.Flashback_mode_clauseContext ctx) {
        throwNotImplemented(); // super.enterFlashback_mode_clause(ctx);
    }

    @Override
    public void exitFlashback_mode_clause(PlSqlParser.Flashback_mode_clauseContext ctx) {
        super.exitFlashback_mode_clause(ctx);
    }

    @Override
    public void enterNew_tablespace_name(PlSqlParser.New_tablespace_nameContext ctx) {
        throwNotImplemented(); // super.enterNew_tablespace_name(ctx);
    }

    @Override
    public void exitNew_tablespace_name(PlSqlParser.New_tablespace_nameContext ctx) {
        super.exitNew_tablespace_name(ctx);
    }

    @Override
    public void enterCreate_tablespace(PlSqlParser.Create_tablespaceContext ctx) {
        throwNotImplemented(); // super.enterCreate_tablespace(ctx);
    }

    @Override
    public void exitCreate_tablespace(PlSqlParser.Create_tablespaceContext ctx) {
        super.exitCreate_tablespace(ctx);
    }

    @Override
    public void enterPermanent_tablespace_clause(PlSqlParser.Permanent_tablespace_clauseContext ctx) {
        throwNotImplemented(); // super.enterPermanent_tablespace_clause(ctx);
    }

    @Override
    public void exitPermanent_tablespace_clause(PlSqlParser.Permanent_tablespace_clauseContext ctx) {
        super.exitPermanent_tablespace_clause(ctx);
    }

    @Override
    public void enterTablespace_encryption_spec(PlSqlParser.Tablespace_encryption_specContext ctx) {
        throwNotImplemented(); // super.enterTablespace_encryption_spec(ctx);
    }

    @Override
    public void exitTablespace_encryption_spec(PlSqlParser.Tablespace_encryption_specContext ctx) {
        super.exitTablespace_encryption_spec(ctx);
    }

    @Override
    public void enterLogging_clause(PlSqlParser.Logging_clauseContext ctx) {
        throwNotImplemented(); // super.enterLogging_clause(ctx);
    }

    @Override
    public void exitLogging_clause(PlSqlParser.Logging_clauseContext ctx) {
        super.exitLogging_clause(ctx);
    }

    @Override
    public void enterExtent_management_clause(PlSqlParser.Extent_management_clauseContext ctx) {
        throwNotImplemented(); // super.enterExtent_management_clause(ctx);
    }

    @Override
    public void exitExtent_management_clause(PlSqlParser.Extent_management_clauseContext ctx) {
        super.exitExtent_management_clause(ctx);
    }

    @Override
    public void enterSegment_management_clause(PlSqlParser.Segment_management_clauseContext ctx) {
        throwNotImplemented(); // super.enterSegment_management_clause(ctx);
    }

    @Override
    public void exitSegment_management_clause(PlSqlParser.Segment_management_clauseContext ctx) {
        super.exitSegment_management_clause(ctx);
    }

    @Override
    public void enterTemporary_tablespace_clause(PlSqlParser.Temporary_tablespace_clauseContext ctx) {
        throwNotImplemented(); // super.enterTemporary_tablespace_clause(ctx);
    }

    @Override
    public void exitTemporary_tablespace_clause(PlSqlParser.Temporary_tablespace_clauseContext ctx) {
        super.exitTemporary_tablespace_clause(ctx);
    }

    @Override
    public void enterUndo_tablespace_clause(PlSqlParser.Undo_tablespace_clauseContext ctx) {
        throwNotImplemented(); // super.enterUndo_tablespace_clause(ctx);
    }

    @Override
    public void exitUndo_tablespace_clause(PlSqlParser.Undo_tablespace_clauseContext ctx) {
        super.exitUndo_tablespace_clause(ctx);
    }

    @Override
    public void enterTablespace_retention_clause(PlSqlParser.Tablespace_retention_clauseContext ctx) {
        throwNotImplemented(); // super.enterTablespace_retention_clause(ctx);
    }

    @Override
    public void exitTablespace_retention_clause(PlSqlParser.Tablespace_retention_clauseContext ctx) {
        super.exitTablespace_retention_clause(ctx);
    }

    @Override
    public void enterDatafile_specification(PlSqlParser.Datafile_specificationContext ctx) {
        throwNotImplemented(); // super.enterDatafile_specification(ctx);
    }

    @Override
    public void exitDatafile_specification(PlSqlParser.Datafile_specificationContext ctx) {
        super.exitDatafile_specification(ctx);
    }

    @Override
    public void enterTempfile_specification(PlSqlParser.Tempfile_specificationContext ctx) {
        throwNotImplemented(); // super.enterTempfile_specification(ctx);
    }

    @Override
    public void exitTempfile_specification(PlSqlParser.Tempfile_specificationContext ctx) {
        super.exitTempfile_specification(ctx);
    }

    @Override
    public void enterDatafile_tempfile_spec(PlSqlParser.Datafile_tempfile_specContext ctx) {
        throwNotImplemented(); // super.enterDatafile_tempfile_spec(ctx);
    }

    @Override
    public void exitDatafile_tempfile_spec(PlSqlParser.Datafile_tempfile_specContext ctx) {
        super.exitDatafile_tempfile_spec(ctx);
    }

    @Override
    public void enterRedo_log_file_spec(PlSqlParser.Redo_log_file_specContext ctx) {
        throwNotImplemented(); // super.enterRedo_log_file_spec(ctx);
    }

    @Override
    public void exitRedo_log_file_spec(PlSqlParser.Redo_log_file_specContext ctx) {
        super.exitRedo_log_file_spec(ctx);
    }

    @Override
    public void enterAutoextend_clause(PlSqlParser.Autoextend_clauseContext ctx) {
        throwNotImplemented(); // super.enterAutoextend_clause(ctx);
    }

    @Override
    public void exitAutoextend_clause(PlSqlParser.Autoextend_clauseContext ctx) {
        super.exitAutoextend_clause(ctx);
    }

    @Override
    public void enterMaxsize_clause(PlSqlParser.Maxsize_clauseContext ctx) {
        throwNotImplemented(); // super.enterMaxsize_clause(ctx);
    }

    @Override
    public void exitMaxsize_clause(PlSqlParser.Maxsize_clauseContext ctx) {
        super.exitMaxsize_clause(ctx);
    }

    @Override
    public void enterBuild_clause(PlSqlParser.Build_clauseContext ctx) {
        throwNotImplemented(); // super.enterBuild_clause(ctx);
    }

    @Override
    public void exitBuild_clause(PlSqlParser.Build_clauseContext ctx) {
        super.exitBuild_clause(ctx);
    }

    @Override
    public void enterParallel_clause(PlSqlParser.Parallel_clauseContext ctx) {
        throwNotImplemented(); // super.enterParallel_clause(ctx);
    }

    @Override
    public void exitParallel_clause(PlSqlParser.Parallel_clauseContext ctx) {
        super.exitParallel_clause(ctx);
    }

    @Override
    public void enterAlter_materialized_view(PlSqlParser.Alter_materialized_viewContext ctx) {
        throwNotImplemented(); // super.enterAlter_materialized_view(ctx);
    }

    @Override
    public void exitAlter_materialized_view(PlSqlParser.Alter_materialized_viewContext ctx) {
        super.exitAlter_materialized_view(ctx);
    }

    @Override
    public void enterAlter_mv_option1(PlSqlParser.Alter_mv_option1Context ctx) {
        throwNotImplemented(); // super.enterAlter_mv_option1(ctx);
    }

    @Override
    public void exitAlter_mv_option1(PlSqlParser.Alter_mv_option1Context ctx) {
        super.exitAlter_mv_option1(ctx);
    }

    @Override
    public void enterAlter_mv_refresh(PlSqlParser.Alter_mv_refreshContext ctx) {
        throwNotImplemented(); // super.enterAlter_mv_refresh(ctx);
    }

    @Override
    public void exitAlter_mv_refresh(PlSqlParser.Alter_mv_refreshContext ctx) {
        super.exitAlter_mv_refresh(ctx);
    }

    @Override
    public void enterRollback_segment(PlSqlParser.Rollback_segmentContext ctx) {
        throwNotImplemented(); // super.enterRollback_segment(ctx);
    }

    @Override
    public void exitRollback_segment(PlSqlParser.Rollback_segmentContext ctx) {
        super.exitRollback_segment(ctx);
    }

    @Override
    public void enterModify_mv_column_clause(PlSqlParser.Modify_mv_column_clauseContext ctx) {
        throwNotImplemented(); // super.enterModify_mv_column_clause(ctx);
    }

    @Override
    public void exitModify_mv_column_clause(PlSqlParser.Modify_mv_column_clauseContext ctx) {
        super.exitModify_mv_column_clause(ctx);
    }

    @Override
    public void enterAlter_materialized_view_log(PlSqlParser.Alter_materialized_view_logContext ctx) {
        throwNotImplemented(); // super.enterAlter_materialized_view_log(ctx);
    }

    @Override
    public void exitAlter_materialized_view_log(PlSqlParser.Alter_materialized_view_logContext ctx) {
        super.exitAlter_materialized_view_log(ctx);
    }

    @Override
    public void enterAdd_mv_log_column_clause(PlSqlParser.Add_mv_log_column_clauseContext ctx) {
        throwNotImplemented(); // super.enterAdd_mv_log_column_clause(ctx);
    }

    @Override
    public void exitAdd_mv_log_column_clause(PlSqlParser.Add_mv_log_column_clauseContext ctx) {
        super.exitAdd_mv_log_column_clause(ctx);
    }

    @Override
    public void enterMove_mv_log_clause(PlSqlParser.Move_mv_log_clauseContext ctx) {
        throwNotImplemented(); // super.enterMove_mv_log_clause(ctx);
    }

    @Override
    public void exitMove_mv_log_clause(PlSqlParser.Move_mv_log_clauseContext ctx) {
        super.exitMove_mv_log_clause(ctx);
    }

    @Override
    public void enterMv_log_augmentation(PlSqlParser.Mv_log_augmentationContext ctx) {
        throwNotImplemented(); // super.enterMv_log_augmentation(ctx);
    }

    @Override
    public void exitMv_log_augmentation(PlSqlParser.Mv_log_augmentationContext ctx) {
        super.exitMv_log_augmentation(ctx);
    }

    @Override
    public void enterDatetime_expr(PlSqlParser.Datetime_exprContext ctx) {
        throwNotImplemented(); // super.enterDatetime_expr(ctx);
    }

    @Override
    public void exitDatetime_expr(PlSqlParser.Datetime_exprContext ctx) {
        super.exitDatetime_expr(ctx);
    }

    @Override
    public void enterInterval_expr(PlSqlParser.Interval_exprContext ctx) {
        throwNotImplemented(); // super.enterInterval_expr(ctx);
    }

    @Override
    public void exitInterval_expr(PlSqlParser.Interval_exprContext ctx) {
        super.exitInterval_expr(ctx);
    }

    @Override
    public void enterSynchronous_or_asynchronous(PlSqlParser.Synchronous_or_asynchronousContext ctx) {
        throwNotImplemented(); // super.enterSynchronous_or_asynchronous(ctx);
    }

    @Override
    public void exitSynchronous_or_asynchronous(PlSqlParser.Synchronous_or_asynchronousContext ctx) {
        super.exitSynchronous_or_asynchronous(ctx);
    }

    @Override
    public void enterIncluding_or_excluding(PlSqlParser.Including_or_excludingContext ctx) {
        throwNotImplemented(); // super.enterIncluding_or_excluding(ctx);
    }

    @Override
    public void exitIncluding_or_excluding(PlSqlParser.Including_or_excludingContext ctx) {
        super.exitIncluding_or_excluding(ctx);
    }

    @Override
    public void enterCreate_materialized_view_log(PlSqlParser.Create_materialized_view_logContext ctx) {
        throwNotImplemented(); // super.enterCreate_materialized_view_log(ctx);
    }

    @Override
    public void exitCreate_materialized_view_log(PlSqlParser.Create_materialized_view_logContext ctx) {
        super.exitCreate_materialized_view_log(ctx);
    }

    @Override
    public void enterNew_values_clause(PlSqlParser.New_values_clauseContext ctx) {
        throwNotImplemented(); // super.enterNew_values_clause(ctx);
    }

    @Override
    public void exitNew_values_clause(PlSqlParser.New_values_clauseContext ctx) {
        super.exitNew_values_clause(ctx);
    }

    @Override
    public void enterMv_log_purge_clause(PlSqlParser.Mv_log_purge_clauseContext ctx) {
        throwNotImplemented(); // super.enterMv_log_purge_clause(ctx);
    }

    @Override
    public void exitMv_log_purge_clause(PlSqlParser.Mv_log_purge_clauseContext ctx) {
        super.exitMv_log_purge_clause(ctx);
    }

    @Override
    public void enterCreate_materialized_view(PlSqlParser.Create_materialized_viewContext ctx) {
        throwNotImplemented(); // super.enterCreate_materialized_view(ctx);
    }

    @Override
    public void exitCreate_materialized_view(PlSqlParser.Create_materialized_viewContext ctx) {
        super.exitCreate_materialized_view(ctx);
    }

    @Override
    public void enterCreate_mv_refresh(PlSqlParser.Create_mv_refreshContext ctx) {
        throwNotImplemented(); // super.enterCreate_mv_refresh(ctx);
    }

    @Override
    public void exitCreate_mv_refresh(PlSqlParser.Create_mv_refreshContext ctx) {
        super.exitCreate_mv_refresh(ctx);
    }

    @Override
    public void enterCreate_context(PlSqlParser.Create_contextContext ctx) {
        throwNotImplemented(); // super.enterCreate_context(ctx);
    }

    @Override
    public void exitCreate_context(PlSqlParser.Create_contextContext ctx) {
        super.exitCreate_context(ctx);
    }

    @Override
    public void enterOracle_namespace(PlSqlParser.Oracle_namespaceContext ctx) {
        throwNotImplemented(); // super.enterOracle_namespace(ctx);
    }

    @Override
    public void exitOracle_namespace(PlSqlParser.Oracle_namespaceContext ctx) {
        super.exitOracle_namespace(ctx);
    }

    @Override
    public void enterCreate_cluster(PlSqlParser.Create_clusterContext ctx) {
        throwNotImplemented(); // super.enterCreate_cluster(ctx);
    }

    @Override
    public void exitCreate_cluster(PlSqlParser.Create_clusterContext ctx) {
        super.exitCreate_cluster(ctx);
    }

    @Override
    public void enterCreate_table(PlSqlParser.Create_tableContext ctx) {
        throwNotImplemented(); // super.enterCreate_table(ctx);
    }

    @Override
    public void exitCreate_table(PlSqlParser.Create_tableContext ctx) {
        super.exitCreate_table(ctx);
    }

    @Override
    public void enterXmltype_table(PlSqlParser.Xmltype_tableContext ctx) {
        throwNotImplemented(); // super.enterXmltype_table(ctx);
    }

    @Override
    public void exitXmltype_table(PlSqlParser.Xmltype_tableContext ctx) {
        super.exitXmltype_table(ctx);
    }

    @Override
    public void enterXmltype_virtual_columns(PlSqlParser.Xmltype_virtual_columnsContext ctx) {
        throwNotImplemented(); // super.enterXmltype_virtual_columns(ctx);
    }

    @Override
    public void exitXmltype_virtual_columns(PlSqlParser.Xmltype_virtual_columnsContext ctx) {
        super.exitXmltype_virtual_columns(ctx);
    }

    @Override
    public void enterXmltype_column_properties(PlSqlParser.Xmltype_column_propertiesContext ctx) {
        throwNotImplemented(); // super.enterXmltype_column_properties(ctx);
    }

    @Override
    public void exitXmltype_column_properties(PlSqlParser.Xmltype_column_propertiesContext ctx) {
        super.exitXmltype_column_properties(ctx);
    }

    @Override
    public void enterXmltype_storage(PlSqlParser.Xmltype_storageContext ctx) {
        throwNotImplemented(); // super.enterXmltype_storage(ctx);
    }

    @Override
    public void exitXmltype_storage(PlSqlParser.Xmltype_storageContext ctx) {
        super.exitXmltype_storage(ctx);
    }

    @Override
    public void enterXmlschema_spec(PlSqlParser.Xmlschema_specContext ctx) {
        throwNotImplemented(); // super.enterXmlschema_spec(ctx);
    }

    @Override
    public void exitXmlschema_spec(PlSqlParser.Xmlschema_specContext ctx) {
        super.exitXmlschema_spec(ctx);
    }

    @Override
    public void enterObject_table(PlSqlParser.Object_tableContext ctx) {
        throwNotImplemented(); // super.enterObject_table(ctx);
    }

    @Override
    public void exitObject_table(PlSqlParser.Object_tableContext ctx) {
        super.exitObject_table(ctx);
    }

    @Override
    public void enterOid_index_clause(PlSqlParser.Oid_index_clauseContext ctx) {
        throwNotImplemented(); // super.enterOid_index_clause(ctx);
    }

    @Override
    public void exitOid_index_clause(PlSqlParser.Oid_index_clauseContext ctx) {
        super.exitOid_index_clause(ctx);
    }

    @Override
    public void enterOid_clause(PlSqlParser.Oid_clauseContext ctx) {
        throwNotImplemented(); // super.enterOid_clause(ctx);
    }

    @Override
    public void exitOid_clause(PlSqlParser.Oid_clauseContext ctx) {
        super.exitOid_clause(ctx);
    }

    @Override
    public void enterObject_properties(PlSqlParser.Object_propertiesContext ctx) {
        throwNotImplemented(); // super.enterObject_properties(ctx);
    }

    @Override
    public void exitObject_properties(PlSqlParser.Object_propertiesContext ctx) {
        super.exitObject_properties(ctx);
    }

    @Override
    public void enterObject_table_substitution(PlSqlParser.Object_table_substitutionContext ctx) {
        throwNotImplemented(); // super.enterObject_table_substitution(ctx);
    }

    @Override
    public void exitObject_table_substitution(PlSqlParser.Object_table_substitutionContext ctx) {
        super.exitObject_table_substitution(ctx);
    }

    @Override
    public void enterRelational_table(PlSqlParser.Relational_tableContext ctx) {
        throwNotImplemented(); // super.enterRelational_table(ctx);
    }

    @Override
    public void exitRelational_table(PlSqlParser.Relational_tableContext ctx) {
        super.exitRelational_table(ctx);
    }

    @Override
    public void enterRelational_properties(PlSqlParser.Relational_propertiesContext ctx) {
        throwNotImplemented(); // super.enterRelational_properties(ctx);
    }

    @Override
    public void exitRelational_properties(PlSqlParser.Relational_propertiesContext ctx) {
        super.exitRelational_properties(ctx);
    }

    @Override
    public void enterTable_partitioning_clauses(PlSqlParser.Table_partitioning_clausesContext ctx) {
        throwNotImplemented(); // super.enterTable_partitioning_clauses(ctx);
    }

    @Override
    public void exitTable_partitioning_clauses(PlSqlParser.Table_partitioning_clausesContext ctx) {
        super.exitTable_partitioning_clauses(ctx);
    }

    @Override
    public void enterRange_partitions(PlSqlParser.Range_partitionsContext ctx) {
        throwNotImplemented(); // super.enterRange_partitions(ctx);
    }

    @Override
    public void exitRange_partitions(PlSqlParser.Range_partitionsContext ctx) {
        super.exitRange_partitions(ctx);
    }

    @Override
    public void enterList_partitions(PlSqlParser.List_partitionsContext ctx) {
        throwNotImplemented(); // super.enterList_partitions(ctx);
    }

    @Override
    public void exitList_partitions(PlSqlParser.List_partitionsContext ctx) {
        super.exitList_partitions(ctx);
    }

    @Override
    public void enterHash_partitions(PlSqlParser.Hash_partitionsContext ctx) {
        throwNotImplemented(); // super.enterHash_partitions(ctx);
    }

    @Override
    public void exitHash_partitions(PlSqlParser.Hash_partitionsContext ctx) {
        super.exitHash_partitions(ctx);
    }

    @Override
    public void enterIndividual_hash_partitions(PlSqlParser.Individual_hash_partitionsContext ctx) {
        throwNotImplemented(); // super.enterIndividual_hash_partitions(ctx);
    }

    @Override
    public void exitIndividual_hash_partitions(PlSqlParser.Individual_hash_partitionsContext ctx) {
        super.exitIndividual_hash_partitions(ctx);
    }

    @Override
    public void enterHash_partitions_by_quantity(PlSqlParser.Hash_partitions_by_quantityContext ctx) {
        throwNotImplemented(); // super.enterHash_partitions_by_quantity(ctx);
    }

    @Override
    public void exitHash_partitions_by_quantity(PlSqlParser.Hash_partitions_by_quantityContext ctx) {
        super.exitHash_partitions_by_quantity(ctx);
    }

    @Override
    public void enterHash_partition_quantity(PlSqlParser.Hash_partition_quantityContext ctx) {
        throwNotImplemented(); // super.enterHash_partition_quantity(ctx);
    }

    @Override
    public void exitHash_partition_quantity(PlSqlParser.Hash_partition_quantityContext ctx) {
        super.exitHash_partition_quantity(ctx);
    }

    @Override
    public void enterComposite_range_partitions(PlSqlParser.Composite_range_partitionsContext ctx) {
        throwNotImplemented(); // super.enterComposite_range_partitions(ctx);
    }

    @Override
    public void exitComposite_range_partitions(PlSqlParser.Composite_range_partitionsContext ctx) {
        super.exitComposite_range_partitions(ctx);
    }

    @Override
    public void enterComposite_list_partitions(PlSqlParser.Composite_list_partitionsContext ctx) {
        throwNotImplemented(); // super.enterComposite_list_partitions(ctx);
    }

    @Override
    public void exitComposite_list_partitions(PlSqlParser.Composite_list_partitionsContext ctx) {
        super.exitComposite_list_partitions(ctx);
    }

    @Override
    public void enterComposite_hash_partitions(PlSqlParser.Composite_hash_partitionsContext ctx) {
        throwNotImplemented(); // super.enterComposite_hash_partitions(ctx);
    }

    @Override
    public void exitComposite_hash_partitions(PlSqlParser.Composite_hash_partitionsContext ctx) {
        super.exitComposite_hash_partitions(ctx);
    }

    @Override
    public void enterReference_partitioning(PlSqlParser.Reference_partitioningContext ctx) {
        throwNotImplemented(); // super.enterReference_partitioning(ctx);
    }

    @Override
    public void exitReference_partitioning(PlSqlParser.Reference_partitioningContext ctx) {
        super.exitReference_partitioning(ctx);
    }

    @Override
    public void enterReference_partition_desc(PlSqlParser.Reference_partition_descContext ctx) {
        throwNotImplemented(); // super.enterReference_partition_desc(ctx);
    }

    @Override
    public void exitReference_partition_desc(PlSqlParser.Reference_partition_descContext ctx) {
        super.exitReference_partition_desc(ctx);
    }

    @Override
    public void enterSystem_partitioning(PlSqlParser.System_partitioningContext ctx) {
        throwNotImplemented(); // super.enterSystem_partitioning(ctx);
    }

    @Override
    public void exitSystem_partitioning(PlSqlParser.System_partitioningContext ctx) {
        super.exitSystem_partitioning(ctx);
    }

    @Override
    public void enterRange_partition_desc(PlSqlParser.Range_partition_descContext ctx) {
        throwNotImplemented(); // super.enterRange_partition_desc(ctx);
    }

    @Override
    public void exitRange_partition_desc(PlSqlParser.Range_partition_descContext ctx) {
        super.exitRange_partition_desc(ctx);
    }

    @Override
    public void enterList_partition_desc(PlSqlParser.List_partition_descContext ctx) {
        throwNotImplemented(); // super.enterList_partition_desc(ctx);
    }

    @Override
    public void exitList_partition_desc(PlSqlParser.List_partition_descContext ctx) {
        super.exitList_partition_desc(ctx);
    }

    @Override
    public void enterSubpartition_template(PlSqlParser.Subpartition_templateContext ctx) {
        throwNotImplemented(); // super.enterSubpartition_template(ctx);
    }

    @Override
    public void exitSubpartition_template(PlSqlParser.Subpartition_templateContext ctx) {
        super.exitSubpartition_template(ctx);
    }

    @Override
    public void enterHash_subpartition_quantity(PlSqlParser.Hash_subpartition_quantityContext ctx) {
        throwNotImplemented(); // super.enterHash_subpartition_quantity(ctx);
    }

    @Override
    public void exitHash_subpartition_quantity(PlSqlParser.Hash_subpartition_quantityContext ctx) {
        super.exitHash_subpartition_quantity(ctx);
    }

    @Override
    public void enterSubpartition_by_range(PlSqlParser.Subpartition_by_rangeContext ctx) {
        throwNotImplemented(); // super.enterSubpartition_by_range(ctx);
    }

    @Override
    public void exitSubpartition_by_range(PlSqlParser.Subpartition_by_rangeContext ctx) {
        super.exitSubpartition_by_range(ctx);
    }

    @Override
    public void enterSubpartition_by_list(PlSqlParser.Subpartition_by_listContext ctx) {
        throwNotImplemented(); // super.enterSubpartition_by_list(ctx);
    }

    @Override
    public void exitSubpartition_by_list(PlSqlParser.Subpartition_by_listContext ctx) {
        super.exitSubpartition_by_list(ctx);
    }

    @Override
    public void enterSubpartition_by_hash(PlSqlParser.Subpartition_by_hashContext ctx) {
        throwNotImplemented(); // super.enterSubpartition_by_hash(ctx);
    }

    @Override
    public void exitSubpartition_by_hash(PlSqlParser.Subpartition_by_hashContext ctx) {
        super.exitSubpartition_by_hash(ctx);
    }

    @Override
    public void enterSubpartition_name(PlSqlParser.Subpartition_nameContext ctx) {
        throwNotImplemented(); // super.enterSubpartition_name(ctx);
    }

    @Override
    public void exitSubpartition_name(PlSqlParser.Subpartition_nameContext ctx) {
        super.exitSubpartition_name(ctx);
    }

    @Override
    public void enterRange_subpartition_desc(PlSqlParser.Range_subpartition_descContext ctx) {
        throwNotImplemented(); // super.enterRange_subpartition_desc(ctx);
    }

    @Override
    public void exitRange_subpartition_desc(PlSqlParser.Range_subpartition_descContext ctx) {
        super.exitRange_subpartition_desc(ctx);
    }

    @Override
    public void enterList_subpartition_desc(PlSqlParser.List_subpartition_descContext ctx) {
        throwNotImplemented(); // super.enterList_subpartition_desc(ctx);
    }

    @Override
    public void exitList_subpartition_desc(PlSqlParser.List_subpartition_descContext ctx) {
        super.exitList_subpartition_desc(ctx);
    }

    @Override
    public void enterIndividual_hash_subparts(PlSqlParser.Individual_hash_subpartsContext ctx) {
        throwNotImplemented(); // super.enterIndividual_hash_subparts(ctx);
    }

    @Override
    public void exitIndividual_hash_subparts(PlSqlParser.Individual_hash_subpartsContext ctx) {
        super.exitIndividual_hash_subparts(ctx);
    }

    @Override
    public void enterHash_subparts_by_quantity(PlSqlParser.Hash_subparts_by_quantityContext ctx) {
        throwNotImplemented(); // super.enterHash_subparts_by_quantity(ctx);
    }

    @Override
    public void exitHash_subparts_by_quantity(PlSqlParser.Hash_subparts_by_quantityContext ctx) {
        super.exitHash_subparts_by_quantity(ctx);
    }

    @Override
    public void enterRange_values_clause(PlSqlParser.Range_values_clauseContext ctx) {
        throwNotImplemented(); // super.enterRange_values_clause(ctx);
    }

    @Override
    public void exitRange_values_clause(PlSqlParser.Range_values_clauseContext ctx) {
        super.exitRange_values_clause(ctx);
    }

    @Override
    public void enterList_values_clause(PlSqlParser.List_values_clauseContext ctx) {
        throwNotImplemented(); // super.enterList_values_clause(ctx);
    }

    @Override
    public void exitList_values_clause(PlSqlParser.List_values_clauseContext ctx) {
        super.exitList_values_clause(ctx);
    }

    @Override
    public void enterTable_partition_description(PlSqlParser.Table_partition_descriptionContext ctx) {
        throwNotImplemented(); // super.enterTable_partition_description(ctx);
    }

    @Override
    public void exitTable_partition_description(PlSqlParser.Table_partition_descriptionContext ctx) {
        super.exitTable_partition_description(ctx);
    }

    @Override
    public void enterPartitioning_storage_clause(PlSqlParser.Partitioning_storage_clauseContext ctx) {
        throwNotImplemented(); // super.enterPartitioning_storage_clause(ctx);
    }

    @Override
    public void exitPartitioning_storage_clause(PlSqlParser.Partitioning_storage_clauseContext ctx) {
        super.exitPartitioning_storage_clause(ctx);
    }

    @Override
    public void enterLob_partitioning_storage(PlSqlParser.Lob_partitioning_storageContext ctx) {
        throwNotImplemented(); // super.enterLob_partitioning_storage(ctx);
    }

    @Override
    public void exitLob_partitioning_storage(PlSqlParser.Lob_partitioning_storageContext ctx) {
        super.exitLob_partitioning_storage(ctx);
    }

    @Override
    public void enterDatatype_null_enable(PlSqlParser.Datatype_null_enableContext ctx) {
        throwNotImplemented(); // super.enterDatatype_null_enable(ctx);
    }

    @Override
    public void exitDatatype_null_enable(PlSqlParser.Datatype_null_enableContext ctx) {
        super.exitDatatype_null_enable(ctx);
    }

    @Override
    public void enterSize_clause(PlSqlParser.Size_clauseContext ctx) {
        throwNotImplemented(); // super.enterSize_clause(ctx);
    }

    @Override
    public void exitSize_clause(PlSqlParser.Size_clauseContext ctx) {
        super.exitSize_clause(ctx);
    }

    @Override
    public void enterTable_compression(PlSqlParser.Table_compressionContext ctx) {
        throwNotImplemented(); // super.enterTable_compression(ctx);
    }

    @Override
    public void exitTable_compression(PlSqlParser.Table_compressionContext ctx) {
        super.exitTable_compression(ctx);
    }

    @Override
    public void enterPhysical_attributes_clause(PlSqlParser.Physical_attributes_clauseContext ctx) {
        throwNotImplemented(); // super.enterPhysical_attributes_clause(ctx);
    }

    @Override
    public void exitPhysical_attributes_clause(PlSqlParser.Physical_attributes_clauseContext ctx) {
        super.exitPhysical_attributes_clause(ctx);
    }

    @Override
    public void enterStorage_clause(PlSqlParser.Storage_clauseContext ctx) {
        throwNotImplemented(); // super.enterStorage_clause(ctx);
    }

    @Override
    public void exitStorage_clause(PlSqlParser.Storage_clauseContext ctx) {
        super.exitStorage_clause(ctx);
    }

    @Override
    public void enterDeferred_segment_creation(PlSqlParser.Deferred_segment_creationContext ctx) {
        throwNotImplemented(); // super.enterDeferred_segment_creation(ctx);
    }

    @Override
    public void exitDeferred_segment_creation(PlSqlParser.Deferred_segment_creationContext ctx) {
        super.exitDeferred_segment_creation(ctx);
    }

    @Override
    public void enterSegment_attributes_clause(PlSqlParser.Segment_attributes_clauseContext ctx) {
        throwNotImplemented(); // super.enterSegment_attributes_clause(ctx);
    }

    @Override
    public void exitSegment_attributes_clause(PlSqlParser.Segment_attributes_clauseContext ctx) {
        super.exitSegment_attributes_clause(ctx);
    }

    @Override
    public void enterPhysical_properties(PlSqlParser.Physical_propertiesContext ctx) {
        throwNotImplemented(); // super.enterPhysical_properties(ctx);
    }

    @Override
    public void exitPhysical_properties(PlSqlParser.Physical_propertiesContext ctx) {
        super.exitPhysical_properties(ctx);
    }

    @Override
    public void enterRow_movement_clause(PlSqlParser.Row_movement_clauseContext ctx) {
        throwNotImplemented(); // super.enterRow_movement_clause(ctx);
    }

    @Override
    public void exitRow_movement_clause(PlSqlParser.Row_movement_clauseContext ctx) {
        super.exitRow_movement_clause(ctx);
    }

    @Override
    public void enterFlashback_archive_clause(PlSqlParser.Flashback_archive_clauseContext ctx) {
        throwNotImplemented(); // super.enterFlashback_archive_clause(ctx);
    }

    @Override
    public void exitFlashback_archive_clause(PlSqlParser.Flashback_archive_clauseContext ctx) {
        super.exitFlashback_archive_clause(ctx);
    }

    @Override
    public void enterLog_grp(PlSqlParser.Log_grpContext ctx) {
        throwNotImplemented(); // super.enterLog_grp(ctx);
    }

    @Override
    public void exitLog_grp(PlSqlParser.Log_grpContext ctx) {
        super.exitLog_grp(ctx);
    }

    @Override
    public void enterSupplemental_table_logging(PlSqlParser.Supplemental_table_loggingContext ctx) {
        throwNotImplemented(); // super.enterSupplemental_table_logging(ctx);
    }

    @Override
    public void exitSupplemental_table_logging(PlSqlParser.Supplemental_table_loggingContext ctx) {
        super.exitSupplemental_table_logging(ctx);
    }

    @Override
    public void enterSupplemental_log_grp_clause(PlSqlParser.Supplemental_log_grp_clauseContext ctx) {
        throwNotImplemented(); // super.enterSupplemental_log_grp_clause(ctx);
    }

    @Override
    public void exitSupplemental_log_grp_clause(PlSqlParser.Supplemental_log_grp_clauseContext ctx) {
        super.exitSupplemental_log_grp_clause(ctx);
    }

    @Override
    public void enterSupplemental_id_key_clause(PlSqlParser.Supplemental_id_key_clauseContext ctx) {
        throwNotImplemented(); // super.enterSupplemental_id_key_clause(ctx);
    }

    @Override
    public void exitSupplemental_id_key_clause(PlSqlParser.Supplemental_id_key_clauseContext ctx) {
        super.exitSupplemental_id_key_clause(ctx);
    }

    @Override
    public void enterAllocate_extent_clause(PlSqlParser.Allocate_extent_clauseContext ctx) {
        throwNotImplemented(); // super.enterAllocate_extent_clause(ctx);
    }

    @Override
    public void exitAllocate_extent_clause(PlSqlParser.Allocate_extent_clauseContext ctx) {
        super.exitAllocate_extent_clause(ctx);
    }

    @Override
    public void enterDeallocate_unused_clause(PlSqlParser.Deallocate_unused_clauseContext ctx) {
        throwNotImplemented(); // super.enterDeallocate_unused_clause(ctx);
    }

    @Override
    public void exitDeallocate_unused_clause(PlSqlParser.Deallocate_unused_clauseContext ctx) {
        super.exitDeallocate_unused_clause(ctx);
    }

    @Override
    public void enterShrink_clause(PlSqlParser.Shrink_clauseContext ctx) {
        throwNotImplemented(); // super.enterShrink_clause(ctx);
    }

    @Override
    public void exitShrink_clause(PlSqlParser.Shrink_clauseContext ctx) {
        super.exitShrink_clause(ctx);
    }

    @Override
    public void enterRecords_per_block_clause(PlSqlParser.Records_per_block_clauseContext ctx) {
        throwNotImplemented(); // super.enterRecords_per_block_clause(ctx);
    }

    @Override
    public void exitRecords_per_block_clause(PlSqlParser.Records_per_block_clauseContext ctx) {
        super.exitRecords_per_block_clause(ctx);
    }

    @Override
    public void enterUpgrade_table_clause(PlSqlParser.Upgrade_table_clauseContext ctx) {
        throwNotImplemented(); // super.enterUpgrade_table_clause(ctx);
    }

    @Override
    public void exitUpgrade_table_clause(PlSqlParser.Upgrade_table_clauseContext ctx) {
        super.exitUpgrade_table_clause(ctx);
    }

    @Override
    public void enterDrop_table(PlSqlParser.Drop_tableContext ctx) {
        throwNotImplemented(); // super.enterDrop_table(ctx);
    }

    @Override
    public void exitDrop_table(PlSqlParser.Drop_tableContext ctx) {
        super.exitDrop_table(ctx);
    }

    @Override
    public void enterComment_on_column(PlSqlParser.Comment_on_columnContext ctx) {
        throwNotImplemented(); // super.enterComment_on_column(ctx);
    }

    @Override
    public void exitComment_on_column(PlSqlParser.Comment_on_columnContext ctx) {
        super.exitComment_on_column(ctx);
    }

    @Override
    public void enterEnable_or_disable(PlSqlParser.Enable_or_disableContext ctx) {
        throwNotImplemented(); // super.enterEnable_or_disable(ctx);
    }

    @Override
    public void exitEnable_or_disable(PlSqlParser.Enable_or_disableContext ctx) {
        super.exitEnable_or_disable(ctx);
    }

    @Override
    public void enterAllow_or_disallow(PlSqlParser.Allow_or_disallowContext ctx) {
        throwNotImplemented(); // super.enterAllow_or_disallow(ctx);
    }

    @Override
    public void exitAllow_or_disallow(PlSqlParser.Allow_or_disallowContext ctx) {
        super.exitAllow_or_disallow(ctx);
    }

    @Override
    public void enterCreate_synonym(PlSqlParser.Create_synonymContext ctx) {
        throwNotImplemented(); // super.enterCreate_synonym(ctx);
    }

    @Override
    public void exitCreate_synonym(PlSqlParser.Create_synonymContext ctx) {
        super.exitCreate_synonym(ctx);
    }

    @Override
    public void enterComment_on_table(PlSqlParser.Comment_on_tableContext ctx) {
        throwNotImplemented(); // super.enterComment_on_table(ctx);
    }

    @Override
    public void exitComment_on_table(PlSqlParser.Comment_on_tableContext ctx) {
        super.exitComment_on_table(ctx);
    }

    @Override
    public void enterAlter_cluster(PlSqlParser.Alter_clusterContext ctx) {
        throwNotImplemented(); // super.enterAlter_cluster(ctx);
    }

    @Override
    public void exitAlter_cluster(PlSqlParser.Alter_clusterContext ctx) {
        super.exitAlter_cluster(ctx);
    }

    @Override
    public void enterCache_or_nocache(PlSqlParser.Cache_or_nocacheContext ctx) {
        throwNotImplemented(); // super.enterCache_or_nocache(ctx);
    }

    @Override
    public void exitCache_or_nocache(PlSqlParser.Cache_or_nocacheContext ctx) {
        super.exitCache_or_nocache(ctx);
    }

    @Override
    public void enterDatabase_name(PlSqlParser.Database_nameContext ctx) {
        throwNotImplemented(); // super.enterDatabase_name(ctx);
    }

    @Override
    public void exitDatabase_name(PlSqlParser.Database_nameContext ctx) {
        super.exitDatabase_name(ctx);
    }

    @Override
    public void enterAlter_database(PlSqlParser.Alter_databaseContext ctx) {
        throwNotImplemented(); // super.enterAlter_database(ctx);
    }

    @Override
    public void exitAlter_database(PlSqlParser.Alter_databaseContext ctx) {
        super.exitAlter_database(ctx);
    }

    @Override
    public void enterStartup_clauses(PlSqlParser.Startup_clausesContext ctx) {
        throwNotImplemented(); // super.enterStartup_clauses(ctx);
    }

    @Override
    public void exitStartup_clauses(PlSqlParser.Startup_clausesContext ctx) {
        super.exitStartup_clauses(ctx);
    }

    @Override
    public void enterResetlogs_or_noresetlogs(PlSqlParser.Resetlogs_or_noresetlogsContext ctx) {
        throwNotImplemented(); // super.enterResetlogs_or_noresetlogs(ctx);
    }

    @Override
    public void exitResetlogs_or_noresetlogs(PlSqlParser.Resetlogs_or_noresetlogsContext ctx) {
        super.exitResetlogs_or_noresetlogs(ctx);
    }

    @Override
    public void enterUpgrade_or_downgrade(PlSqlParser.Upgrade_or_downgradeContext ctx) {
        throwNotImplemented(); // super.enterUpgrade_or_downgrade(ctx);
    }

    @Override
    public void exitUpgrade_or_downgrade(PlSqlParser.Upgrade_or_downgradeContext ctx) {
        super.exitUpgrade_or_downgrade(ctx);
    }

    @Override
    public void enterRecovery_clauses(PlSqlParser.Recovery_clausesContext ctx) {
        throwNotImplemented(); // super.enterRecovery_clauses(ctx);
    }

    @Override
    public void exitRecovery_clauses(PlSqlParser.Recovery_clausesContext ctx) {
        super.exitRecovery_clauses(ctx);
    }

    @Override
    public void enterBegin_or_end(PlSqlParser.Begin_or_endContext ctx) {
        throwNotImplemented(); // super.enterBegin_or_end(ctx);
    }

    @Override
    public void exitBegin_or_end(PlSqlParser.Begin_or_endContext ctx) {
        super.exitBegin_or_end(ctx);
    }

    @Override
    public void enterGeneral_recovery(PlSqlParser.General_recoveryContext ctx) {
        throwNotImplemented(); // super.enterGeneral_recovery(ctx);
    }

    @Override
    public void exitGeneral_recovery(PlSqlParser.General_recoveryContext ctx) {
        super.exitGeneral_recovery(ctx);
    }

    @Override
    public void enterFull_database_recovery(PlSqlParser.Full_database_recoveryContext ctx) {
        throwNotImplemented(); // super.enterFull_database_recovery(ctx);
    }

    @Override
    public void exitFull_database_recovery(PlSqlParser.Full_database_recoveryContext ctx) {
        super.exitFull_database_recovery(ctx);
    }

    @Override
    public void enterPartial_database_recovery(PlSqlParser.Partial_database_recoveryContext ctx) {
        throwNotImplemented(); // super.enterPartial_database_recovery(ctx);
    }

    @Override
    public void exitPartial_database_recovery(PlSqlParser.Partial_database_recoveryContext ctx) {
        super.exitPartial_database_recovery(ctx);
    }

    @Override
    public void enterPartial_database_recovery_10g(PlSqlParser.Partial_database_recovery_10gContext ctx) {
        throwNotImplemented(); // super.enterPartial_database_recovery_10g(ctx);
    }

    @Override
    public void exitPartial_database_recovery_10g(PlSqlParser.Partial_database_recovery_10gContext ctx) {
        super.exitPartial_database_recovery_10g(ctx);
    }

    @Override
    public void enterManaged_standby_recovery(PlSqlParser.Managed_standby_recoveryContext ctx) {
        throwNotImplemented(); // super.enterManaged_standby_recovery(ctx);
    }

    @Override
    public void exitManaged_standby_recovery(PlSqlParser.Managed_standby_recoveryContext ctx) {
        super.exitManaged_standby_recovery(ctx);
    }

    @Override
    public void enterDb_name(PlSqlParser.Db_nameContext ctx) {
        throwNotImplemented(); // super.enterDb_name(ctx);
    }

    @Override
    public void exitDb_name(PlSqlParser.Db_nameContext ctx) {
        super.exitDb_name(ctx);
    }

    @Override
    public void enterDatabase_file_clauses(PlSqlParser.Database_file_clausesContext ctx) {
        throwNotImplemented(); // super.enterDatabase_file_clauses(ctx);
    }

    @Override
    public void exitDatabase_file_clauses(PlSqlParser.Database_file_clausesContext ctx) {
        super.exitDatabase_file_clauses(ctx);
    }

    @Override
    public void enterCreate_datafile_clause(PlSqlParser.Create_datafile_clauseContext ctx) {
        throwNotImplemented(); // super.enterCreate_datafile_clause(ctx);
    }

    @Override
    public void exitCreate_datafile_clause(PlSqlParser.Create_datafile_clauseContext ctx) {
        super.exitCreate_datafile_clause(ctx);
    }

    @Override
    public void enterAlter_datafile_clause(PlSqlParser.Alter_datafile_clauseContext ctx) {
        throwNotImplemented(); // super.enterAlter_datafile_clause(ctx);
    }

    @Override
    public void exitAlter_datafile_clause(PlSqlParser.Alter_datafile_clauseContext ctx) {
        super.exitAlter_datafile_clause(ctx);
    }

    @Override
    public void enterAlter_tempfile_clause(PlSqlParser.Alter_tempfile_clauseContext ctx) {
        throwNotImplemented(); // super.enterAlter_tempfile_clause(ctx);
    }

    @Override
    public void exitAlter_tempfile_clause(PlSqlParser.Alter_tempfile_clauseContext ctx) {
        super.exitAlter_tempfile_clause(ctx);
    }

    @Override
    public void enterLogfile_clauses(PlSqlParser.Logfile_clausesContext ctx) {
        throwNotImplemented(); // super.enterLogfile_clauses(ctx);
    }

    @Override
    public void exitLogfile_clauses(PlSqlParser.Logfile_clausesContext ctx) {
        super.exitLogfile_clauses(ctx);
    }

    @Override
    public void enterAdd_logfile_clauses(PlSqlParser.Add_logfile_clausesContext ctx) {
        throwNotImplemented(); // super.enterAdd_logfile_clauses(ctx);
    }

    @Override
    public void exitAdd_logfile_clauses(PlSqlParser.Add_logfile_clausesContext ctx) {
        super.exitAdd_logfile_clauses(ctx);
    }

    @Override
    public void enterLog_file_group(PlSqlParser.Log_file_groupContext ctx) {
        throwNotImplemented(); // super.enterLog_file_group(ctx);
    }

    @Override
    public void exitLog_file_group(PlSqlParser.Log_file_groupContext ctx) {
        super.exitLog_file_group(ctx);
    }

    @Override
    public void enterDrop_logfile_clauses(PlSqlParser.Drop_logfile_clausesContext ctx) {
        throwNotImplemented(); // super.enterDrop_logfile_clauses(ctx);
    }

    @Override
    public void exitDrop_logfile_clauses(PlSqlParser.Drop_logfile_clausesContext ctx) {
        super.exitDrop_logfile_clauses(ctx);
    }

    @Override
    public void enterSwitch_logfile_clause(PlSqlParser.Switch_logfile_clauseContext ctx) {
        throwNotImplemented(); // super.enterSwitch_logfile_clause(ctx);
    }

    @Override
    public void exitSwitch_logfile_clause(PlSqlParser.Switch_logfile_clauseContext ctx) {
        super.exitSwitch_logfile_clause(ctx);
    }

    @Override
    public void enterSupplemental_db_logging(PlSqlParser.Supplemental_db_loggingContext ctx) {
        throwNotImplemented(); // super.enterSupplemental_db_logging(ctx);
    }

    @Override
    public void exitSupplemental_db_logging(PlSqlParser.Supplemental_db_loggingContext ctx) {
        super.exitSupplemental_db_logging(ctx);
    }

    @Override
    public void enterAdd_or_drop(PlSqlParser.Add_or_dropContext ctx) {
        throwNotImplemented(); // super.enterAdd_or_drop(ctx);
    }

    @Override
    public void exitAdd_or_drop(PlSqlParser.Add_or_dropContext ctx) {
        super.exitAdd_or_drop(ctx);
    }

    @Override
    public void enterSupplemental_plsql_clause(PlSqlParser.Supplemental_plsql_clauseContext ctx) {
        throwNotImplemented(); // super.enterSupplemental_plsql_clause(ctx);
    }

    @Override
    public void exitSupplemental_plsql_clause(PlSqlParser.Supplemental_plsql_clauseContext ctx) {
        super.exitSupplemental_plsql_clause(ctx);
    }

    @Override
    public void enterLogfile_descriptor(PlSqlParser.Logfile_descriptorContext ctx) {
        throwNotImplemented(); // super.enterLogfile_descriptor(ctx);
    }

    @Override
    public void exitLogfile_descriptor(PlSqlParser.Logfile_descriptorContext ctx) {
        super.exitLogfile_descriptor(ctx);
    }

    @Override
    public void enterControlfile_clauses(PlSqlParser.Controlfile_clausesContext ctx) {
        throwNotImplemented(); // super.enterControlfile_clauses(ctx);
    }

    @Override
    public void exitControlfile_clauses(PlSqlParser.Controlfile_clausesContext ctx) {
        super.exitControlfile_clauses(ctx);
    }

    @Override
    public void enterTrace_file_clause(PlSqlParser.Trace_file_clauseContext ctx) {
        throwNotImplemented(); // super.enterTrace_file_clause(ctx);
    }

    @Override
    public void exitTrace_file_clause(PlSqlParser.Trace_file_clauseContext ctx) {
        super.exitTrace_file_clause(ctx);
    }

    @Override
    public void enterStandby_database_clauses(PlSqlParser.Standby_database_clausesContext ctx) {
        throwNotImplemented(); // super.enterStandby_database_clauses(ctx);
    }

    @Override
    public void exitStandby_database_clauses(PlSqlParser.Standby_database_clausesContext ctx) {
        super.exitStandby_database_clauses(ctx);
    }

    @Override
    public void enterActivate_standby_db_clause(PlSqlParser.Activate_standby_db_clauseContext ctx) {
        throwNotImplemented(); // super.enterActivate_standby_db_clause(ctx);
    }

    @Override
    public void exitActivate_standby_db_clause(PlSqlParser.Activate_standby_db_clauseContext ctx) {
        super.exitActivate_standby_db_clause(ctx);
    }

    @Override
    public void enterMaximize_standby_db_clause(PlSqlParser.Maximize_standby_db_clauseContext ctx) {
        throwNotImplemented(); // super.enterMaximize_standby_db_clause(ctx);
    }

    @Override
    public void exitMaximize_standby_db_clause(PlSqlParser.Maximize_standby_db_clauseContext ctx) {
        super.exitMaximize_standby_db_clause(ctx);
    }

    @Override
    public void enterRegister_logfile_clause(PlSqlParser.Register_logfile_clauseContext ctx) {
        throwNotImplemented(); // super.enterRegister_logfile_clause(ctx);
    }

    @Override
    public void exitRegister_logfile_clause(PlSqlParser.Register_logfile_clauseContext ctx) {
        super.exitRegister_logfile_clause(ctx);
    }

    @Override
    public void enterCommit_switchover_clause(PlSqlParser.Commit_switchover_clauseContext ctx) {
        throwNotImplemented(); // super.enterCommit_switchover_clause(ctx);
    }

    @Override
    public void exitCommit_switchover_clause(PlSqlParser.Commit_switchover_clauseContext ctx) {
        super.exitCommit_switchover_clause(ctx);
    }

    @Override
    public void enterStart_standby_clause(PlSqlParser.Start_standby_clauseContext ctx) {
        throwNotImplemented(); // super.enterStart_standby_clause(ctx);
    }

    @Override
    public void exitStart_standby_clause(PlSqlParser.Start_standby_clauseContext ctx) {
        super.exitStart_standby_clause(ctx);
    }

    @Override
    public void enterStop_standby_clause(PlSqlParser.Stop_standby_clauseContext ctx) {
        throwNotImplemented(); // super.enterStop_standby_clause(ctx);
    }

    @Override
    public void exitStop_standby_clause(PlSqlParser.Stop_standby_clauseContext ctx) {
        super.exitStop_standby_clause(ctx);
    }

    @Override
    public void enterConvert_database_clause(PlSqlParser.Convert_database_clauseContext ctx) {
        throwNotImplemented(); // super.enterConvert_database_clause(ctx);
    }

    @Override
    public void exitConvert_database_clause(PlSqlParser.Convert_database_clauseContext ctx) {
        super.exitConvert_database_clause(ctx);
    }

    @Override
    public void enterDefault_settings_clause(PlSqlParser.Default_settings_clauseContext ctx) {
        throwNotImplemented(); // super.enterDefault_settings_clause(ctx);
    }

    @Override
    public void exitDefault_settings_clause(PlSqlParser.Default_settings_clauseContext ctx) {
        super.exitDefault_settings_clause(ctx);
    }

    @Override
    public void enterSet_time_zone_clause(PlSqlParser.Set_time_zone_clauseContext ctx) {
        throwNotImplemented(); // super.enterSet_time_zone_clause(ctx);
    }

    @Override
    public void exitSet_time_zone_clause(PlSqlParser.Set_time_zone_clauseContext ctx) {
        super.exitSet_time_zone_clause(ctx);
    }

    @Override
    public void enterInstance_clauses(PlSqlParser.Instance_clausesContext ctx) {
        throwNotImplemented(); // super.enterInstance_clauses(ctx);
    }

    @Override
    public void exitInstance_clauses(PlSqlParser.Instance_clausesContext ctx) {
        super.exitInstance_clauses(ctx);
    }

    @Override
    public void enterSecurity_clause(PlSqlParser.Security_clauseContext ctx) {
        throwNotImplemented(); // super.enterSecurity_clause(ctx);
    }

    @Override
    public void exitSecurity_clause(PlSqlParser.Security_clauseContext ctx) {
        super.exitSecurity_clause(ctx);
    }

    @Override
    public void enterDomain(PlSqlParser.DomainContext ctx) {
        throwNotImplemented(); // super.enterDomain(ctx);
    }

    @Override
    public void exitDomain(PlSqlParser.DomainContext ctx) {
        super.exitDomain(ctx);
    }

    @Override
    public void enterDatabase(PlSqlParser.DatabaseContext ctx) {
        throwNotImplemented(); // super.enterDatabase(ctx);
    }

    @Override
    public void exitDatabase(PlSqlParser.DatabaseContext ctx) {
        super.exitDatabase(ctx);
    }

    @Override
    public void enterEdition_name(PlSqlParser.Edition_nameContext ctx) {
        throwNotImplemented(); // super.enterEdition_name(ctx);
    }

    @Override
    public void exitEdition_name(PlSqlParser.Edition_nameContext ctx) {
        super.exitEdition_name(ctx);
    }

    @Override
    public void enterFilenumber(PlSqlParser.FilenumberContext ctx) {
        throwNotImplemented(); // super.enterFilenumber(ctx);
    }

    @Override
    public void exitFilenumber(PlSqlParser.FilenumberContext ctx) {
        super.exitFilenumber(ctx);
    }

    @Override
    public void enterFilename(PlSqlParser.FilenameContext ctx) {
        throwNotImplemented(); // super.enterFilename(ctx);
    }

    @Override
    public void exitFilename(PlSqlParser.FilenameContext ctx) {
        super.exitFilename(ctx);
    }

    @Override
    public void enterAlter_table(PlSqlParser.Alter_tableContext ctx) {
        throwNotImplemented(); // super.enterAlter_table(ctx);
    }

    @Override
    public void exitAlter_table(PlSqlParser.Alter_tableContext ctx) {
        super.exitAlter_table(ctx);
    }

    @Override
    public void enterAlter_table_properties(PlSqlParser.Alter_table_propertiesContext ctx) {
        throwNotImplemented(); // super.enterAlter_table_properties(ctx);
    }

    @Override
    public void exitAlter_table_properties(PlSqlParser.Alter_table_propertiesContext ctx) {
        super.exitAlter_table_properties(ctx);
    }

    @Override
    public void enterAlter_table_properties_1(PlSqlParser.Alter_table_properties_1Context ctx) {
        throwNotImplemented(); // super.enterAlter_table_properties_1(ctx);
    }

    @Override
    public void exitAlter_table_properties_1(PlSqlParser.Alter_table_properties_1Context ctx) {
        super.exitAlter_table_properties_1(ctx);
    }

    @Override
    public void enterAlter_iot_clauses(PlSqlParser.Alter_iot_clausesContext ctx) {
        throwNotImplemented(); // super.enterAlter_iot_clauses(ctx);
    }

    @Override
    public void exitAlter_iot_clauses(PlSqlParser.Alter_iot_clausesContext ctx) {
        super.exitAlter_iot_clauses(ctx);
    }

    @Override
    public void enterAlter_mapping_table_clause(PlSqlParser.Alter_mapping_table_clauseContext ctx) {
        throwNotImplemented(); // super.enterAlter_mapping_table_clause(ctx);
    }

    @Override
    public void exitAlter_mapping_table_clause(PlSqlParser.Alter_mapping_table_clauseContext ctx) {
        super.exitAlter_mapping_table_clause(ctx);
    }

    @Override
    public void enterAlter_overflow_clause(PlSqlParser.Alter_overflow_clauseContext ctx) {
        throwNotImplemented(); // super.enterAlter_overflow_clause(ctx);
    }

    @Override
    public void exitAlter_overflow_clause(PlSqlParser.Alter_overflow_clauseContext ctx) {
        super.exitAlter_overflow_clause(ctx);
    }

    @Override
    public void enterAdd_overflow_clause(PlSqlParser.Add_overflow_clauseContext ctx) {
        throwNotImplemented(); // super.enterAdd_overflow_clause(ctx);
    }

    @Override
    public void exitAdd_overflow_clause(PlSqlParser.Add_overflow_clauseContext ctx) {
        super.exitAdd_overflow_clause(ctx);
    }

    @Override
    public void enterEnable_disable_clause(PlSqlParser.Enable_disable_clauseContext ctx) {
        throwNotImplemented(); // super.enterEnable_disable_clause(ctx);
    }

    @Override
    public void exitEnable_disable_clause(PlSqlParser.Enable_disable_clauseContext ctx) {
        super.exitEnable_disable_clause(ctx);
    }

    @Override
    public void enterUsing_index_clause(PlSqlParser.Using_index_clauseContext ctx) {
        throwNotImplemented(); // super.enterUsing_index_clause(ctx);
    }

    @Override
    public void exitUsing_index_clause(PlSqlParser.Using_index_clauseContext ctx) {
        super.exitUsing_index_clause(ctx);
    }

    @Override
    public void enterIndex_attributes(PlSqlParser.Index_attributesContext ctx) {
        throwNotImplemented(); // super.enterIndex_attributes(ctx);
    }

    @Override
    public void exitIndex_attributes(PlSqlParser.Index_attributesContext ctx) {
        super.exitIndex_attributes(ctx);
    }

    @Override
    public void enterSort_or_nosort(PlSqlParser.Sort_or_nosortContext ctx) {
        throwNotImplemented(); // super.enterSort_or_nosort(ctx);
    }

    @Override
    public void exitSort_or_nosort(PlSqlParser.Sort_or_nosortContext ctx) {
        super.exitSort_or_nosort(ctx);
    }

    @Override
    public void enterExceptions_clause(PlSqlParser.Exceptions_clauseContext ctx) {
        throwNotImplemented(); // super.enterExceptions_clause(ctx);
    }

    @Override
    public void exitExceptions_clause(PlSqlParser.Exceptions_clauseContext ctx) {
        super.exitExceptions_clause(ctx);
    }

    @Override
    public void enterMove_table_clause(PlSqlParser.Move_table_clauseContext ctx) {
        throwNotImplemented(); // super.enterMove_table_clause(ctx);
    }

    @Override
    public void exitMove_table_clause(PlSqlParser.Move_table_clauseContext ctx) {
        super.exitMove_table_clause(ctx);
    }

    @Override
    public void enterIndex_org_table_clause(PlSqlParser.Index_org_table_clauseContext ctx) {
        throwNotImplemented(); // super.enterIndex_org_table_clause(ctx);
    }

    @Override
    public void exitIndex_org_table_clause(PlSqlParser.Index_org_table_clauseContext ctx) {
        super.exitIndex_org_table_clause(ctx);
    }

    @Override
    public void enterMapping_table_clause(PlSqlParser.Mapping_table_clauseContext ctx) {
        throwNotImplemented(); // super.enterMapping_table_clause(ctx);
    }

    @Override
    public void exitMapping_table_clause(PlSqlParser.Mapping_table_clauseContext ctx) {
        super.exitMapping_table_clause(ctx);
    }

    @Override
    public void enterKey_compression(PlSqlParser.Key_compressionContext ctx) {
        throwNotImplemented(); // super.enterKey_compression(ctx);
    }

    @Override
    public void exitKey_compression(PlSqlParser.Key_compressionContext ctx) {
        super.exitKey_compression(ctx);
    }

    @Override
    public void enterIndex_org_overflow_clause(PlSqlParser.Index_org_overflow_clauseContext ctx) {
        throwNotImplemented(); // super.enterIndex_org_overflow_clause(ctx);
    }

    @Override
    public void exitIndex_org_overflow_clause(PlSqlParser.Index_org_overflow_clauseContext ctx) {
        super.exitIndex_org_overflow_clause(ctx);
    }

    @Override
    public void enterColumn_clauses(PlSqlParser.Column_clausesContext ctx) {
        throwNotImplemented(); // super.enterColumn_clauses(ctx);
    }

    @Override
    public void exitColumn_clauses(PlSqlParser.Column_clausesContext ctx) {
        super.exitColumn_clauses(ctx);
    }

    @Override
    public void enterModify_collection_retrieval(PlSqlParser.Modify_collection_retrievalContext ctx) {
        throwNotImplemented(); // super.enterModify_collection_retrieval(ctx);
    }

    @Override
    public void exitModify_collection_retrieval(PlSqlParser.Modify_collection_retrievalContext ctx) {
        super.exitModify_collection_retrieval(ctx);
    }

    @Override
    public void enterCollection_item(PlSqlParser.Collection_itemContext ctx) {
        throwNotImplemented(); // super.enterCollection_item(ctx);
    }

    @Override
    public void exitCollection_item(PlSqlParser.Collection_itemContext ctx) {
        super.exitCollection_item(ctx);
    }

    @Override
    public void enterRename_column_clause(PlSqlParser.Rename_column_clauseContext ctx) {
        throwNotImplemented(); // super.enterRename_column_clause(ctx);
    }

    @Override
    public void exitRename_column_clause(PlSqlParser.Rename_column_clauseContext ctx) {
        super.exitRename_column_clause(ctx);
    }

    @Override
    public void enterOld_column_name(PlSqlParser.Old_column_nameContext ctx) {
        throwNotImplemented(); // super.enterOld_column_name(ctx);
    }

    @Override
    public void exitOld_column_name(PlSqlParser.Old_column_nameContext ctx) {
        super.exitOld_column_name(ctx);
    }

    @Override
    public void enterNew_column_name(PlSqlParser.New_column_nameContext ctx) {
        throwNotImplemented(); // super.enterNew_column_name(ctx);
    }

    @Override
    public void exitNew_column_name(PlSqlParser.New_column_nameContext ctx) {
        super.exitNew_column_name(ctx);
    }

    @Override
    public void enterAdd_modify_drop_column_clauses(PlSqlParser.Add_modify_drop_column_clausesContext ctx) {
        throwNotImplemented(); // super.enterAdd_modify_drop_column_clauses(ctx);
    }

    @Override
    public void exitAdd_modify_drop_column_clauses(PlSqlParser.Add_modify_drop_column_clausesContext ctx) {
        super.exitAdd_modify_drop_column_clauses(ctx);
    }

    @Override
    public void enterDrop_column_clause(PlSqlParser.Drop_column_clauseContext ctx) {
        throwNotImplemented(); // super.enterDrop_column_clause(ctx);
    }

    @Override
    public void exitDrop_column_clause(PlSqlParser.Drop_column_clauseContext ctx) {
        super.exitDrop_column_clause(ctx);
    }

    @Override
    public void enterModify_column_clauses(PlSqlParser.Modify_column_clausesContext ctx) {
        throwNotImplemented(); // super.enterModify_column_clauses(ctx);
    }

    @Override
    public void exitModify_column_clauses(PlSqlParser.Modify_column_clausesContext ctx) {
        super.exitModify_column_clauses(ctx);
    }

    @Override
    public void enterModify_col_properties(PlSqlParser.Modify_col_propertiesContext ctx) {
        throwNotImplemented(); // super.enterModify_col_properties(ctx);
    }

    @Override
    public void exitModify_col_properties(PlSqlParser.Modify_col_propertiesContext ctx) {
        super.exitModify_col_properties(ctx);
    }

    @Override
    public void enterModify_col_substitutable(PlSqlParser.Modify_col_substitutableContext ctx) {
        throwNotImplemented(); // super.enterModify_col_substitutable(ctx);
    }

    @Override
    public void exitModify_col_substitutable(PlSqlParser.Modify_col_substitutableContext ctx) {
        super.exitModify_col_substitutable(ctx);
    }

    @Override
    public void enterAdd_column_clause(PlSqlParser.Add_column_clauseContext ctx) {
        throwNotImplemented(); // super.enterAdd_column_clause(ctx);
    }

    @Override
    public void exitAdd_column_clause(PlSqlParser.Add_column_clauseContext ctx) {
        super.exitAdd_column_clause(ctx);
    }

    @Override
    public void enterAlter_varray_col_properties(PlSqlParser.Alter_varray_col_propertiesContext ctx) {
        throwNotImplemented(); // super.enterAlter_varray_col_properties(ctx);
    }

    @Override
    public void exitAlter_varray_col_properties(PlSqlParser.Alter_varray_col_propertiesContext ctx) {
        super.exitAlter_varray_col_properties(ctx);
    }

    @Override
    public void enterVarray_col_properties(PlSqlParser.Varray_col_propertiesContext ctx) {
        throwNotImplemented(); // super.enterVarray_col_properties(ctx);
    }

    @Override
    public void exitVarray_col_properties(PlSqlParser.Varray_col_propertiesContext ctx) {
        super.exitVarray_col_properties(ctx);
    }

    @Override
    public void enterVarray_storage_clause(PlSqlParser.Varray_storage_clauseContext ctx) {
        throwNotImplemented(); // super.enterVarray_storage_clause(ctx);
    }

    @Override
    public void exitVarray_storage_clause(PlSqlParser.Varray_storage_clauseContext ctx) {
        super.exitVarray_storage_clause(ctx);
    }

    @Override
    public void enterLob_segname(PlSqlParser.Lob_segnameContext ctx) {
        throwNotImplemented(); // super.enterLob_segname(ctx);
    }

    @Override
    public void exitLob_segname(PlSqlParser.Lob_segnameContext ctx) {
        super.exitLob_segname(ctx);
    }

    @Override
    public void enterLob_item(PlSqlParser.Lob_itemContext ctx) {
        throwNotImplemented(); // super.enterLob_item(ctx);
    }

    @Override
    public void exitLob_item(PlSqlParser.Lob_itemContext ctx) {
        super.exitLob_item(ctx);
    }

    @Override
    public void enterLob_storage_parameters(PlSqlParser.Lob_storage_parametersContext ctx) {
        throwNotImplemented(); // super.enterLob_storage_parameters(ctx);
    }

    @Override
    public void exitLob_storage_parameters(PlSqlParser.Lob_storage_parametersContext ctx) {
        super.exitLob_storage_parameters(ctx);
    }

    @Override
    public void enterLob_storage_clause(PlSqlParser.Lob_storage_clauseContext ctx) {
        throwNotImplemented(); // super.enterLob_storage_clause(ctx);
    }

    @Override
    public void exitLob_storage_clause(PlSqlParser.Lob_storage_clauseContext ctx) {
        super.exitLob_storage_clause(ctx);
    }

    @Override
    public void enterModify_lob_storage_clause(PlSqlParser.Modify_lob_storage_clauseContext ctx) {
        throwNotImplemented(); // super.enterModify_lob_storage_clause(ctx);
    }

    @Override
    public void exitModify_lob_storage_clause(PlSqlParser.Modify_lob_storage_clauseContext ctx) {
        super.exitModify_lob_storage_clause(ctx);
    }

    @Override
    public void enterModify_lob_parameters(PlSqlParser.Modify_lob_parametersContext ctx) {
        throwNotImplemented(); // super.enterModify_lob_parameters(ctx);
    }

    @Override
    public void exitModify_lob_parameters(PlSqlParser.Modify_lob_parametersContext ctx) {
        super.exitModify_lob_parameters(ctx);
    }

    @Override
    public void enterLob_parameters(PlSqlParser.Lob_parametersContext ctx) {
        throwNotImplemented(); // super.enterLob_parameters(ctx);
    }

    @Override
    public void exitLob_parameters(PlSqlParser.Lob_parametersContext ctx) {
        super.exitLob_parameters(ctx);
    }

    @Override
    public void enterLob_deduplicate_clause(PlSqlParser.Lob_deduplicate_clauseContext ctx) {
        throwNotImplemented(); // super.enterLob_deduplicate_clause(ctx);
    }

    @Override
    public void exitLob_deduplicate_clause(PlSqlParser.Lob_deduplicate_clauseContext ctx) {
        super.exitLob_deduplicate_clause(ctx);
    }

    @Override
    public void enterLob_compression_clause(PlSqlParser.Lob_compression_clauseContext ctx) {
        throwNotImplemented(); // super.enterLob_compression_clause(ctx);
    }

    @Override
    public void exitLob_compression_clause(PlSqlParser.Lob_compression_clauseContext ctx) {
        super.exitLob_compression_clause(ctx);
    }

    @Override
    public void enterLob_retention_clause(PlSqlParser.Lob_retention_clauseContext ctx) {
        throwNotImplemented(); // super.enterLob_retention_clause(ctx);
    }

    @Override
    public void exitLob_retention_clause(PlSqlParser.Lob_retention_clauseContext ctx) {
        super.exitLob_retention_clause(ctx);
    }

    @Override
    public void enterEncryption_spec(PlSqlParser.Encryption_specContext ctx) {
        throwNotImplemented(); // super.enterEncryption_spec(ctx);
    }

    @Override
    public void exitEncryption_spec(PlSqlParser.Encryption_specContext ctx) {
        super.exitEncryption_spec(ctx);
    }

    @Override
    public void enterTablespace(PlSqlParser.TablespaceContext ctx) {
        throwNotImplemented(); // super.enterTablespace(ctx);
    }

    @Override
    public void exitTablespace(PlSqlParser.TablespaceContext ctx) {
        super.exitTablespace(ctx);
    }

    @Override
    public void enterVarray_item(PlSqlParser.Varray_itemContext ctx) {
        throwNotImplemented(); // super.enterVarray_item(ctx);
    }

    @Override
    public void exitVarray_item(PlSqlParser.Varray_itemContext ctx) {
        super.exitVarray_item(ctx);
    }

    @Override
    public void enterColumn_properties(PlSqlParser.Column_propertiesContext ctx) {
        throwNotImplemented(); // super.enterColumn_properties(ctx);
    }

    @Override
    public void exitColumn_properties(PlSqlParser.Column_propertiesContext ctx) {
        super.exitColumn_properties(ctx);
    }

    @Override
    public void enterPeriod_definition(PlSqlParser.Period_definitionContext ctx) {
        throwNotImplemented(); // super.enterPeriod_definition(ctx);
    }

    @Override
    public void exitPeriod_definition(PlSqlParser.Period_definitionContext ctx) {
        super.exitPeriod_definition(ctx);
    }

    @Override
    public void enterStart_time_column(PlSqlParser.Start_time_columnContext ctx) {
        throwNotImplemented(); // super.enterStart_time_column(ctx);
    }

    @Override
    public void exitStart_time_column(PlSqlParser.Start_time_columnContext ctx) {
        super.exitStart_time_column(ctx);
    }

    @Override
    public void enterEnd_time_column(PlSqlParser.End_time_columnContext ctx) {
        throwNotImplemented(); // super.enterEnd_time_column(ctx);
    }

    @Override
    public void exitEnd_time_column(PlSqlParser.End_time_columnContext ctx) {
        super.exitEnd_time_column(ctx);
    }

    @Override
    public void enterColumn_definition(PlSqlParser.Column_definitionContext ctx) {
        throwNotImplemented(); // super.enterColumn_definition(ctx);
    }

    @Override
    public void exitColumn_definition(PlSqlParser.Column_definitionContext ctx) {
        super.exitColumn_definition(ctx);
    }

    @Override
    public void enterVirtual_column_definition(PlSqlParser.Virtual_column_definitionContext ctx) {
        throwNotImplemented(); // super.enterVirtual_column_definition(ctx);
    }

    @Override
    public void exitVirtual_column_definition(PlSqlParser.Virtual_column_definitionContext ctx) {
        super.exitVirtual_column_definition(ctx);
    }

    @Override
    public void enterOut_of_line_part_storage(PlSqlParser.Out_of_line_part_storageContext ctx) {
        throwNotImplemented(); // super.enterOut_of_line_part_storage(ctx);
    }

    @Override
    public void exitOut_of_line_part_storage(PlSqlParser.Out_of_line_part_storageContext ctx) {
        super.exitOut_of_line_part_storage(ctx);
    }

    @Override
    public void enterNested_table_col_properties(PlSqlParser.Nested_table_col_propertiesContext ctx) {
        throwNotImplemented(); // super.enterNested_table_col_properties(ctx);
    }

    @Override
    public void exitNested_table_col_properties(PlSqlParser.Nested_table_col_propertiesContext ctx) {
        super.exitNested_table_col_properties(ctx);
    }

    @Override
    public void enterNested_item(PlSqlParser.Nested_itemContext ctx) {
        throwNotImplemented(); // super.enterNested_item(ctx);
    }

    @Override
    public void exitNested_item(PlSqlParser.Nested_itemContext ctx) {
        super.exitNested_item(ctx);
    }

    @Override
    public void enterSubstitutable_column_clause(PlSqlParser.Substitutable_column_clauseContext ctx) {
        throwNotImplemented(); // super.enterSubstitutable_column_clause(ctx);
    }

    @Override
    public void exitSubstitutable_column_clause(PlSqlParser.Substitutable_column_clauseContext ctx) {
        super.exitSubstitutable_column_clause(ctx);
    }

    @Override
    public void enterPartition_name(PlSqlParser.Partition_nameContext ctx) {
        throwNotImplemented(); // super.enterPartition_name(ctx);
    }

    @Override
    public void exitPartition_name(PlSqlParser.Partition_nameContext ctx) {
        super.exitPartition_name(ctx);
    }

    @Override
    public void enterSupplemental_logging_props(PlSqlParser.Supplemental_logging_propsContext ctx) {
        throwNotImplemented(); // super.enterSupplemental_logging_props(ctx);
    }

    @Override
    public void exitSupplemental_logging_props(PlSqlParser.Supplemental_logging_propsContext ctx) {
        super.exitSupplemental_logging_props(ctx);
    }

    @Override
    public void enterColumn_or_attribute(PlSqlParser.Column_or_attributeContext ctx) {
        throwNotImplemented(); // super.enterColumn_or_attribute(ctx);
    }

    @Override
    public void exitColumn_or_attribute(PlSqlParser.Column_or_attributeContext ctx) {
        super.exitColumn_or_attribute(ctx);
    }

    @Override
    public void enterObject_type_col_properties(PlSqlParser.Object_type_col_propertiesContext ctx) {
        throwNotImplemented(); // super.enterObject_type_col_properties(ctx);
    }

    @Override
    public void exitObject_type_col_properties(PlSqlParser.Object_type_col_propertiesContext ctx) {
        super.exitObject_type_col_properties(ctx);
    }

    @Override
    public void enterConstraint_clauses(PlSqlParser.Constraint_clausesContext ctx) {
        throwNotImplemented(); // super.enterConstraint_clauses(ctx);
    }

    @Override
    public void exitConstraint_clauses(PlSqlParser.Constraint_clausesContext ctx) {
        super.exitConstraint_clauses(ctx);
    }

    @Override
    public void enterOld_constraint_name(PlSqlParser.Old_constraint_nameContext ctx) {
        throwNotImplemented(); // super.enterOld_constraint_name(ctx);
    }

    @Override
    public void exitOld_constraint_name(PlSqlParser.Old_constraint_nameContext ctx) {
        super.exitOld_constraint_name(ctx);
    }

    @Override
    public void enterNew_constraint_name(PlSqlParser.New_constraint_nameContext ctx) {
        throwNotImplemented(); // super.enterNew_constraint_name(ctx);
    }

    @Override
    public void exitNew_constraint_name(PlSqlParser.New_constraint_nameContext ctx) {
        super.exitNew_constraint_name(ctx);
    }

    @Override
    public void enterDrop_constraint_clause(PlSqlParser.Drop_constraint_clauseContext ctx) {
        throwNotImplemented(); // super.enterDrop_constraint_clause(ctx);
    }

    @Override
    public void exitDrop_constraint_clause(PlSqlParser.Drop_constraint_clauseContext ctx) {
        super.exitDrop_constraint_clause(ctx);
    }

    @Override
    public void enterDrop_primary_key_or_unique_or_generic_clause(PlSqlParser.Drop_primary_key_or_unique_or_generic_clauseContext ctx) {
        throwNotImplemented(); // super.enterDrop_primary_key_or_unique_or_generic_clause(ctx);
    }

    @Override
    public void exitDrop_primary_key_or_unique_or_generic_clause(PlSqlParser.Drop_primary_key_or_unique_or_generic_clauseContext ctx) {
        super.exitDrop_primary_key_or_unique_or_generic_clause(ctx);
    }

    @Override
    public void enterAdd_constraint(PlSqlParser.Add_constraintContext ctx) {
        throwNotImplemented(); // super.enterAdd_constraint(ctx);
    }

    @Override
    public void exitAdd_constraint(PlSqlParser.Add_constraintContext ctx) {
        super.exitAdd_constraint(ctx);
    }

    @Override
    public void enterCheck_constraint(PlSqlParser.Check_constraintContext ctx) {
        throwNotImplemented(); // super.enterCheck_constraint(ctx);
    }

    @Override
    public void exitCheck_constraint(PlSqlParser.Check_constraintContext ctx) {
        super.exitCheck_constraint(ctx);
    }

    @Override
    public void enterDrop_constraint(PlSqlParser.Drop_constraintContext ctx) {
        throwNotImplemented(); // super.enterDrop_constraint(ctx);
    }

    @Override
    public void exitDrop_constraint(PlSqlParser.Drop_constraintContext ctx) {
        super.exitDrop_constraint(ctx);
    }

    @Override
    public void enterEnable_constraint(PlSqlParser.Enable_constraintContext ctx) {
        throwNotImplemented(); // super.enterEnable_constraint(ctx);
    }

    @Override
    public void exitEnable_constraint(PlSqlParser.Enable_constraintContext ctx) {
        super.exitEnable_constraint(ctx);
    }

    @Override
    public void enterDisable_constraint(PlSqlParser.Disable_constraintContext ctx) {
        throwNotImplemented(); // super.enterDisable_constraint(ctx);
    }

    @Override
    public void exitDisable_constraint(PlSqlParser.Disable_constraintContext ctx) {
        super.exitDisable_constraint(ctx);
    }

    @Override
    public void enterForeign_key_clause(PlSqlParser.Foreign_key_clauseContext ctx) {
        throwNotImplemented(); // super.enterForeign_key_clause(ctx);
    }

    @Override
    public void exitForeign_key_clause(PlSqlParser.Foreign_key_clauseContext ctx) {
        super.exitForeign_key_clause(ctx);
    }

    @Override
    public void enterReferences_clause(PlSqlParser.References_clauseContext ctx) {
        throwNotImplemented(); // super.enterReferences_clause(ctx);
    }

    @Override
    public void exitReferences_clause(PlSqlParser.References_clauseContext ctx) {
        super.exitReferences_clause(ctx);
    }

    @Override
    public void enterOn_delete_clause(PlSqlParser.On_delete_clauseContext ctx) {
        throwNotImplemented(); // super.enterOn_delete_clause(ctx);
    }

    @Override
    public void exitOn_delete_clause(PlSqlParser.On_delete_clauseContext ctx) {
        super.exitOn_delete_clause(ctx);
    }

    @Override
    public void enterUnique_key_clause(PlSqlParser.Unique_key_clauseContext ctx) {
        throwNotImplemented(); // super.enterUnique_key_clause(ctx);
    }

    @Override
    public void exitUnique_key_clause(PlSqlParser.Unique_key_clauseContext ctx) {
        super.exitUnique_key_clause(ctx);
    }

    @Override
    public void enterPrimary_key_clause(PlSqlParser.Primary_key_clauseContext ctx) {
        throwNotImplemented(); // super.enterPrimary_key_clause(ctx);
    }

    @Override
    public void exitPrimary_key_clause(PlSqlParser.Primary_key_clauseContext ctx) {
        super.exitPrimary_key_clause(ctx);
    }

    @Override
    public void enterAnonymous_block(PlSqlParser.Anonymous_blockContext ctx) {
        throwNotImplemented(); // super.enterAnonymous_block(ctx);
    }

    @Override
    public void exitAnonymous_block(PlSqlParser.Anonymous_blockContext ctx) {
        super.exitAnonymous_block(ctx);
    }

    @Override
    public void enterInvoker_rights_clause(PlSqlParser.Invoker_rights_clauseContext ctx) {
        throwNotImplemented(); // super.enterInvoker_rights_clause(ctx);
    }

    @Override
    public void exitInvoker_rights_clause(PlSqlParser.Invoker_rights_clauseContext ctx) {
        super.exitInvoker_rights_clause(ctx);
    }

    @Override
    public void enterCall_spec(PlSqlParser.Call_specContext ctx) {
        throwNotImplemented(); // super.enterCall_spec(ctx);
    }

    @Override
    public void exitCall_spec(PlSqlParser.Call_specContext ctx) {
        super.exitCall_spec(ctx);
    }

    @Override
    public void enterJava_spec(PlSqlParser.Java_specContext ctx) {
        throwNotImplemented(); // super.enterJava_spec(ctx);
    }

    @Override
    public void exitJava_spec(PlSqlParser.Java_specContext ctx) {
        super.exitJava_spec(ctx);
    }

    @Override
    public void enterC_spec(PlSqlParser.C_specContext ctx) {
        throwNotImplemented(); // super.enterC_spec(ctx);
    }

    @Override
    public void exitC_spec(PlSqlParser.C_specContext ctx) {
        super.exitC_spec(ctx);
    }

    @Override
    public void enterC_agent_in_clause(PlSqlParser.C_agent_in_clauseContext ctx) {
        throwNotImplemented(); // super.enterC_agent_in_clause(ctx);
    }

    @Override
    public void exitC_agent_in_clause(PlSqlParser.C_agent_in_clauseContext ctx) {
        super.exitC_agent_in_clause(ctx);
    }

    @Override
    public void enterC_parameters_clause(PlSqlParser.C_parameters_clauseContext ctx) {
        throwNotImplemented(); // super.enterC_parameters_clause(ctx);
    }

    @Override
    public void exitC_parameters_clause(PlSqlParser.C_parameters_clauseContext ctx) {
        super.exitC_parameters_clause(ctx);
    }

    @Override
    public void enterParameter(PlSqlParser.ParameterContext ctx) {
        super.enterParameter(ctx);

        MetadataNode currentNode = getNode();
        if (ServiceMethod.class.isAssignableFrom(currentNode.getClass())) {
            ((ServiceMethod) currentNode).getParameters().add(
                    push(ServiceMethodParameter.class, "parameters")
            );
        } else {
            throwException("Cannot add parameter for node class '?'", currentNode.getClass());
        }
    }

    @Override
    public void exitParameter(PlSqlParser.ParameterContext ctx) {
        super.exitParameter(ctx);
        pop();
    }

    @Override
    public void enterDefault_value_part(PlSqlParser.Default_value_partContext ctx) {
        throwNotImplemented(); // super.enterDefault_value_part(ctx);
    }

    @Override
    public void exitDefault_value_part(PlSqlParser.Default_value_partContext ctx) {
        super.exitDefault_value_part(ctx);
    }

    @Override
    public void enterSeq_of_declare_specs(PlSqlParser.Seq_of_declare_specsContext ctx) {
        throwNotImplemented(); // super.enterSeq_of_declare_specs(ctx);
    }

    @Override
    public void exitSeq_of_declare_specs(PlSqlParser.Seq_of_declare_specsContext ctx) {
        super.exitSeq_of_declare_specs(ctx);
    }

    @Override
    public void enterDeclare_spec(PlSqlParser.Declare_specContext ctx) {
        throwNotImplemented(); // super.enterDeclare_spec(ctx);
    }

    @Override
    public void exitDeclare_spec(PlSqlParser.Declare_specContext ctx) {
        super.exitDeclare_spec(ctx);
    }

    @Override
    public void enterVariable_declaration(PlSqlParser.Variable_declarationContext ctx) {
        throwNotImplemented(); // super.enterVariable_declaration(ctx);
    }

    @Override
    public void exitVariable_declaration(PlSqlParser.Variable_declarationContext ctx) {
        super.exitVariable_declaration(ctx);
    }

    @Override
    public void enterSubtype_declaration(PlSqlParser.Subtype_declarationContext ctx) {
        throwNotImplemented(); // super.enterSubtype_declaration(ctx);
    }

    @Override
    public void exitSubtype_declaration(PlSqlParser.Subtype_declarationContext ctx) {
        super.exitSubtype_declaration(ctx);
    }

    @Override
    public void enterCursor_declaration(PlSqlParser.Cursor_declarationContext ctx) {
        throwNotImplemented(); // super.enterCursor_declaration(ctx);
    }

    @Override
    public void exitCursor_declaration(PlSqlParser.Cursor_declarationContext ctx) {
        super.exitCursor_declaration(ctx);
    }

    @Override
    public void enterParameter_spec(PlSqlParser.Parameter_specContext ctx) {
        throwNotImplemented(); // super.enterParameter_spec(ctx);
    }

    @Override
    public void exitParameter_spec(PlSqlParser.Parameter_specContext ctx) {
        super.exitParameter_spec(ctx);
    }

    @Override
    public void enterException_declaration(PlSqlParser.Exception_declarationContext ctx) {
        throwNotImplemented(); // super.enterException_declaration(ctx);
    }

    @Override
    public void exitException_declaration(PlSqlParser.Exception_declarationContext ctx) {
        super.exitException_declaration(ctx);
    }

    @Override
    public void enterPragma_declaration(PlSqlParser.Pragma_declarationContext ctx) {
        throwNotImplemented(); // super.enterPragma_declaration(ctx);
    }

    @Override
    public void exitPragma_declaration(PlSqlParser.Pragma_declarationContext ctx) {
        super.exitPragma_declaration(ctx);
    }

    @Override
    public void enterRecord_type_def(PlSqlParser.Record_type_defContext ctx) {
        throwNotImplemented(); // super.enterRecord_type_def(ctx);
    }

    @Override
    public void exitRecord_type_def(PlSqlParser.Record_type_defContext ctx) {
        super.exitRecord_type_def(ctx);
    }

    @Override
    public void enterField_spec(PlSqlParser.Field_specContext ctx) {
        throwNotImplemented(); // super.enterField_spec(ctx);
    }

    @Override
    public void exitField_spec(PlSqlParser.Field_specContext ctx) {
        super.exitField_spec(ctx);
    }

    @Override
    public void enterRef_cursor_type_def(PlSqlParser.Ref_cursor_type_defContext ctx) {
        throwNotImplemented(); // super.enterRef_cursor_type_def(ctx);
    }

    @Override
    public void exitRef_cursor_type_def(PlSqlParser.Ref_cursor_type_defContext ctx) {
        super.exitRef_cursor_type_def(ctx);
    }

    @Override
    public void enterType_declaration(PlSqlParser.Type_declarationContext ctx) {
        throwNotImplemented(); // super.enterType_declaration(ctx);
    }

    @Override
    public void exitType_declaration(PlSqlParser.Type_declarationContext ctx) {
        super.exitType_declaration(ctx);
    }

    @Override
    public void enterTable_type_def(PlSqlParser.Table_type_defContext ctx) {
        throwNotImplemented(); // super.enterTable_type_def(ctx);
    }

    @Override
    public void exitTable_type_def(PlSqlParser.Table_type_defContext ctx) {
        super.exitTable_type_def(ctx);
    }

    @Override
    public void enterTable_indexed_by_part(PlSqlParser.Table_indexed_by_partContext ctx) {
        throwNotImplemented(); // super.enterTable_indexed_by_part(ctx);
    }

    @Override
    public void exitTable_indexed_by_part(PlSqlParser.Table_indexed_by_partContext ctx) {
        super.exitTable_indexed_by_part(ctx);
    }

    @Override
    public void enterVarray_type_def(PlSqlParser.Varray_type_defContext ctx) {
        throwNotImplemented(); // super.enterVarray_type_def(ctx);
    }

    @Override
    public void exitVarray_type_def(PlSqlParser.Varray_type_defContext ctx) {
        super.exitVarray_type_def(ctx);
    }

    @Override
    public void enterSeq_of_statements(PlSqlParser.Seq_of_statementsContext ctx) {
        throwNotImplemented(); // super.enterSeq_of_statements(ctx);
    }

    @Override
    public void exitSeq_of_statements(PlSqlParser.Seq_of_statementsContext ctx) {
        super.exitSeq_of_statements(ctx);
    }

    @Override
    public void enterLabel_declaration(PlSqlParser.Label_declarationContext ctx) {
        throwNotImplemented(); // super.enterLabel_declaration(ctx);
    }

    @Override
    public void exitLabel_declaration(PlSqlParser.Label_declarationContext ctx) {
        super.exitLabel_declaration(ctx);
    }

    @Override
    public void enterStatement(PlSqlParser.StatementContext ctx) {
        throwNotImplemented(); // super.enterStatement(ctx);
    }

    @Override
    public void exitStatement(PlSqlParser.StatementContext ctx) {
        super.exitStatement(ctx);
    }

    @Override
    public void enterSwallow_to_semi(PlSqlParser.Swallow_to_semiContext ctx) {
        throwNotImplemented(); // super.enterSwallow_to_semi(ctx);
    }

    @Override
    public void exitSwallow_to_semi(PlSqlParser.Swallow_to_semiContext ctx) {
        super.exitSwallow_to_semi(ctx);
    }

    @Override
    public void enterAssignment_statement(PlSqlParser.Assignment_statementContext ctx) {
        throwNotImplemented(); // super.enterAssignment_statement(ctx);
    }

    @Override
    public void exitAssignment_statement(PlSqlParser.Assignment_statementContext ctx) {
        super.exitAssignment_statement(ctx);
    }

    @Override
    public void enterContinue_statement(PlSqlParser.Continue_statementContext ctx) {
        throwNotImplemented(); // super.enterContinue_statement(ctx);
    }

    @Override
    public void exitContinue_statement(PlSqlParser.Continue_statementContext ctx) {
        super.exitContinue_statement(ctx);
    }

    @Override
    public void enterExit_statement(PlSqlParser.Exit_statementContext ctx) {
        throwNotImplemented(); // super.enterExit_statement(ctx);
    }

    @Override
    public void exitExit_statement(PlSqlParser.Exit_statementContext ctx) {
        super.exitExit_statement(ctx);
    }

    @Override
    public void enterGoto_statement(PlSqlParser.Goto_statementContext ctx) {
        throwNotImplemented(); // super.enterGoto_statement(ctx);
    }

    @Override
    public void exitGoto_statement(PlSqlParser.Goto_statementContext ctx) {
        super.exitGoto_statement(ctx);
    }

    @Override
    public void enterIf_statement(PlSqlParser.If_statementContext ctx) {
        throwNotImplemented(); // super.enterIf_statement(ctx);
    }

    @Override
    public void exitIf_statement(PlSqlParser.If_statementContext ctx) {
        super.exitIf_statement(ctx);
    }

    @Override
    public void enterElsif_part(PlSqlParser.Elsif_partContext ctx) {
        throwNotImplemented(); // super.enterElsif_part(ctx);
    }

    @Override
    public void exitElsif_part(PlSqlParser.Elsif_partContext ctx) {
        super.exitElsif_part(ctx);
    }

    @Override
    public void enterElse_part(PlSqlParser.Else_partContext ctx) {
        throwNotImplemented(); // super.enterElse_part(ctx);
    }

    @Override
    public void exitElse_part(PlSqlParser.Else_partContext ctx) {
        super.exitElse_part(ctx);
    }

    @Override
    public void enterLoop_statement(PlSqlParser.Loop_statementContext ctx) {
        throwNotImplemented(); // super.enterLoop_statement(ctx);
    }

    @Override
    public void exitLoop_statement(PlSqlParser.Loop_statementContext ctx) {
        super.exitLoop_statement(ctx);
    }

    @Override
    public void enterCursor_loop_param(PlSqlParser.Cursor_loop_paramContext ctx) {
        throwNotImplemented(); // super.enterCursor_loop_param(ctx);
    }

    @Override
    public void exitCursor_loop_param(PlSqlParser.Cursor_loop_paramContext ctx) {
        super.exitCursor_loop_param(ctx);
    }

    @Override
    public void enterForall_statement(PlSqlParser.Forall_statementContext ctx) {
        throwNotImplemented(); // super.enterForall_statement(ctx);
    }

    @Override
    public void exitForall_statement(PlSqlParser.Forall_statementContext ctx) {
        super.exitForall_statement(ctx);
    }

    @Override
    public void enterBounds_clause(PlSqlParser.Bounds_clauseContext ctx) {
        throwNotImplemented(); // super.enterBounds_clause(ctx);
    }

    @Override
    public void exitBounds_clause(PlSqlParser.Bounds_clauseContext ctx) {
        super.exitBounds_clause(ctx);
    }

    @Override
    public void enterBetween_bound(PlSqlParser.Between_boundContext ctx) {
        throwNotImplemented(); // super.enterBetween_bound(ctx);
    }

    @Override
    public void exitBetween_bound(PlSqlParser.Between_boundContext ctx) {
        super.exitBetween_bound(ctx);
    }

    @Override
    public void enterLower_bound(PlSqlParser.Lower_boundContext ctx) {
        throwNotImplemented(); // super.enterLower_bound(ctx);
    }

    @Override
    public void exitLower_bound(PlSqlParser.Lower_boundContext ctx) {
        super.exitLower_bound(ctx);
    }

    @Override
    public void enterUpper_bound(PlSqlParser.Upper_boundContext ctx) {
        throwNotImplemented(); // super.enterUpper_bound(ctx);
    }

    @Override
    public void exitUpper_bound(PlSqlParser.Upper_boundContext ctx) {
        super.exitUpper_bound(ctx);
    }

    @Override
    public void enterNull_statement(PlSqlParser.Null_statementContext ctx) {
        throwNotImplemented(); // super.enterNull_statement(ctx);
    }

    @Override
    public void exitNull_statement(PlSqlParser.Null_statementContext ctx) {
        super.exitNull_statement(ctx);
    }

    @Override
    public void enterRaise_statement(PlSqlParser.Raise_statementContext ctx) {
        throwNotImplemented(); // super.enterRaise_statement(ctx);
    }

    @Override
    public void exitRaise_statement(PlSqlParser.Raise_statementContext ctx) {
        super.exitRaise_statement(ctx);
    }

    @Override
    public void enterReturn_statement(PlSqlParser.Return_statementContext ctx) {
        throwNotImplemented(); // super.enterReturn_statement(ctx);
    }

    @Override
    public void exitReturn_statement(PlSqlParser.Return_statementContext ctx) {
        super.exitReturn_statement(ctx);
    }

    @Override
    public void enterFunction_call(PlSqlParser.Function_callContext ctx) {
        throwNotImplemented(); // super.enterFunction_call(ctx);
    }

    @Override
    public void exitFunction_call(PlSqlParser.Function_callContext ctx) {
        super.exitFunction_call(ctx);
    }

    @Override
    public void enterPipe_row_statement(PlSqlParser.Pipe_row_statementContext ctx) {
        throwNotImplemented(); // super.enterPipe_row_statement(ctx);
    }

    @Override
    public void exitPipe_row_statement(PlSqlParser.Pipe_row_statementContext ctx) {
        super.exitPipe_row_statement(ctx);
    }

    @Override
    public void enterBody(PlSqlParser.BodyContext ctx) {
        throwNotImplemented(); // super.enterBody(ctx);
    }

    @Override
    public void exitBody(PlSqlParser.BodyContext ctx) {
        super.exitBody(ctx);
    }

    @Override
    public void enterException_handler(PlSqlParser.Exception_handlerContext ctx) {
        throwNotImplemented(); // super.enterException_handler(ctx);
    }

    @Override
    public void exitException_handler(PlSqlParser.Exception_handlerContext ctx) {
        super.exitException_handler(ctx);
    }

    @Override
    public void enterTrigger_block(PlSqlParser.Trigger_blockContext ctx) {
        throwNotImplemented(); // super.enterTrigger_block(ctx);
    }

    @Override
    public void exitTrigger_block(PlSqlParser.Trigger_blockContext ctx) {
        super.exitTrigger_block(ctx);
    }

    @Override
    public void enterBlock(PlSqlParser.BlockContext ctx) {
        throwNotImplemented(); // super.enterBlock(ctx);
    }

    @Override
    public void exitBlock(PlSqlParser.BlockContext ctx) {
        super.exitBlock(ctx);
    }

    @Override
    public void enterSql_statement(PlSqlParser.Sql_statementContext ctx) {
        throwNotImplemented(); // super.enterSql_statement(ctx);
    }

    @Override
    public void exitSql_statement(PlSqlParser.Sql_statementContext ctx) {
        super.exitSql_statement(ctx);
    }

    @Override
    public void enterExecute_immediate(PlSqlParser.Execute_immediateContext ctx) {
        throwNotImplemented(); // super.enterExecute_immediate(ctx);
    }

    @Override
    public void exitExecute_immediate(PlSqlParser.Execute_immediateContext ctx) {
        super.exitExecute_immediate(ctx);
    }

    @Override
    public void enterDynamic_returning_clause(PlSqlParser.Dynamic_returning_clauseContext ctx) {
        throwNotImplemented(); // super.enterDynamic_returning_clause(ctx);
    }

    @Override
    public void exitDynamic_returning_clause(PlSqlParser.Dynamic_returning_clauseContext ctx) {
        super.exitDynamic_returning_clause(ctx);
    }

    @Override
    public void enterData_manipulation_language_statements(PlSqlParser.Data_manipulation_language_statementsContext ctx) {
        throwNotImplemented(); // super.enterData_manipulation_language_statements(ctx);
    }

    @Override
    public void exitData_manipulation_language_statements(PlSqlParser.Data_manipulation_language_statementsContext ctx) {
        super.exitData_manipulation_language_statements(ctx);
    }

    @Override
    public void enterCursor_manipulation_statements(PlSqlParser.Cursor_manipulation_statementsContext ctx) {
        throwNotImplemented(); // super.enterCursor_manipulation_statements(ctx);
    }

    @Override
    public void exitCursor_manipulation_statements(PlSqlParser.Cursor_manipulation_statementsContext ctx) {
        super.exitCursor_manipulation_statements(ctx);
    }

    @Override
    public void enterClose_statement(PlSqlParser.Close_statementContext ctx) {
        throwNotImplemented(); // super.enterClose_statement(ctx);
    }

    @Override
    public void exitClose_statement(PlSqlParser.Close_statementContext ctx) {
        super.exitClose_statement(ctx);
    }

    @Override
    public void enterOpen_statement(PlSqlParser.Open_statementContext ctx) {
        throwNotImplemented(); // super.enterOpen_statement(ctx);
    }

    @Override
    public void exitOpen_statement(PlSqlParser.Open_statementContext ctx) {
        super.exitOpen_statement(ctx);
    }

    @Override
    public void enterFetch_statement(PlSqlParser.Fetch_statementContext ctx) {
        throwNotImplemented(); // super.enterFetch_statement(ctx);
    }

    @Override
    public void exitFetch_statement(PlSqlParser.Fetch_statementContext ctx) {
        super.exitFetch_statement(ctx);
    }

    @Override
    public void enterOpen_for_statement(PlSqlParser.Open_for_statementContext ctx) {
        throwNotImplemented(); // super.enterOpen_for_statement(ctx);
    }

    @Override
    public void exitOpen_for_statement(PlSqlParser.Open_for_statementContext ctx) {
        super.exitOpen_for_statement(ctx);
    }

    @Override
    public void enterTransaction_control_statements(PlSqlParser.Transaction_control_statementsContext ctx) {
        throwNotImplemented(); // super.enterTransaction_control_statements(ctx);
    }

    @Override
    public void exitTransaction_control_statements(PlSqlParser.Transaction_control_statementsContext ctx) {
        super.exitTransaction_control_statements(ctx);
    }

    @Override
    public void enterSet_transaction_command(PlSqlParser.Set_transaction_commandContext ctx) {
        throwNotImplemented(); // super.enterSet_transaction_command(ctx);
    }

    @Override
    public void exitSet_transaction_command(PlSqlParser.Set_transaction_commandContext ctx) {
        super.exitSet_transaction_command(ctx);
    }

    @Override
    public void enterSet_constraint_command(PlSqlParser.Set_constraint_commandContext ctx) {
        throwNotImplemented(); // super.enterSet_constraint_command(ctx);
    }

    @Override
    public void exitSet_constraint_command(PlSqlParser.Set_constraint_commandContext ctx) {
        super.exitSet_constraint_command(ctx);
    }

    @Override
    public void enterCommit_statement(PlSqlParser.Commit_statementContext ctx) {
        throwNotImplemented(); // super.enterCommit_statement(ctx);
    }

    @Override
    public void exitCommit_statement(PlSqlParser.Commit_statementContext ctx) {
        super.exitCommit_statement(ctx);
    }

    @Override
    public void enterWrite_clause(PlSqlParser.Write_clauseContext ctx) {
        throwNotImplemented(); // super.enterWrite_clause(ctx);
    }

    @Override
    public void exitWrite_clause(PlSqlParser.Write_clauseContext ctx) {
        super.exitWrite_clause(ctx);
    }

    @Override
    public void enterRollback_statement(PlSqlParser.Rollback_statementContext ctx) {
        throwNotImplemented(); // super.enterRollback_statement(ctx);
    }

    @Override
    public void exitRollback_statement(PlSqlParser.Rollback_statementContext ctx) {
        super.exitRollback_statement(ctx);
    }

    @Override
    public void enterSavepoint_statement(PlSqlParser.Savepoint_statementContext ctx) {
        throwNotImplemented(); // super.enterSavepoint_statement(ctx);
    }

    @Override
    public void exitSavepoint_statement(PlSqlParser.Savepoint_statementContext ctx) {
        super.exitSavepoint_statement(ctx);
    }

    @Override
    public void enterExplain_statement(PlSqlParser.Explain_statementContext ctx) {
        throwNotImplemented(); // super.enterExplain_statement(ctx);
    }

    @Override
    public void exitExplain_statement(PlSqlParser.Explain_statementContext ctx) {
        super.exitExplain_statement(ctx);
    }

    @Override
    public void enterSelect_statement(PlSqlParser.Select_statementContext ctx) {
        throwNotImplemented(); // super.enterSelect_statement(ctx);
    }

    @Override
    public void exitSelect_statement(PlSqlParser.Select_statementContext ctx) {
        super.exitSelect_statement(ctx);
    }

    @Override
    public void enterSubquery_factoring_clause(PlSqlParser.Subquery_factoring_clauseContext ctx) {
        throwNotImplemented(); // super.enterSubquery_factoring_clause(ctx);
    }

    @Override
    public void exitSubquery_factoring_clause(PlSqlParser.Subquery_factoring_clauseContext ctx) {
        super.exitSubquery_factoring_clause(ctx);
    }

    @Override
    public void enterFactoring_element(PlSqlParser.Factoring_elementContext ctx) {
        throwNotImplemented(); // super.enterFactoring_element(ctx);
    }

    @Override
    public void exitFactoring_element(PlSqlParser.Factoring_elementContext ctx) {
        super.exitFactoring_element(ctx);
    }

    @Override
    public void enterSearch_clause(PlSqlParser.Search_clauseContext ctx) {
        throwNotImplemented(); // super.enterSearch_clause(ctx);
    }

    @Override
    public void exitSearch_clause(PlSqlParser.Search_clauseContext ctx) {
        super.exitSearch_clause(ctx);
    }

    @Override
    public void enterCycle_clause(PlSqlParser.Cycle_clauseContext ctx) {
        throwNotImplemented(); // super.enterCycle_clause(ctx);
    }

    @Override
    public void exitCycle_clause(PlSqlParser.Cycle_clauseContext ctx) {
        super.exitCycle_clause(ctx);
    }

    @Override
    public void enterSubquery(PlSqlParser.SubqueryContext ctx) {
        throwNotImplemented(); // super.enterSubquery(ctx);
    }

    @Override
    public void exitSubquery(PlSqlParser.SubqueryContext ctx) {
        super.exitSubquery(ctx);
    }

    @Override
    public void enterSubquery_basic_elements(PlSqlParser.Subquery_basic_elementsContext ctx) {
        throwNotImplemented(); // super.enterSubquery_basic_elements(ctx);
    }

    @Override
    public void exitSubquery_basic_elements(PlSqlParser.Subquery_basic_elementsContext ctx) {
        super.exitSubquery_basic_elements(ctx);
    }

    @Override
    public void enterSubquery_operation_part(PlSqlParser.Subquery_operation_partContext ctx) {
        throwNotImplemented(); // super.enterSubquery_operation_part(ctx);
    }

    @Override
    public void exitSubquery_operation_part(PlSqlParser.Subquery_operation_partContext ctx) {
        super.exitSubquery_operation_part(ctx);
    }

    @Override
    public void enterQuery_block(PlSqlParser.Query_blockContext ctx) {
        throwNotImplemented(); // super.enterQuery_block(ctx);
    }

    @Override
    public void exitQuery_block(PlSqlParser.Query_blockContext ctx) {
        super.exitQuery_block(ctx);
    }

    @Override
    public void enterSelected_element(PlSqlParser.Selected_elementContext ctx) {
        throwNotImplemented(); // super.enterSelected_element(ctx);
    }

    @Override
    public void exitSelected_element(PlSqlParser.Selected_elementContext ctx) {
        super.exitSelected_element(ctx);
    }

    @Override
    public void enterFrom_clause(PlSqlParser.From_clauseContext ctx) {
        throwNotImplemented(); // super.enterFrom_clause(ctx);
    }

    @Override
    public void exitFrom_clause(PlSqlParser.From_clauseContext ctx) {
        super.exitFrom_clause(ctx);
    }

    @Override
    public void enterSelect_list_elements(PlSqlParser.Select_list_elementsContext ctx) {
        throwNotImplemented(); // super.enterSelect_list_elements(ctx);
    }

    @Override
    public void exitSelect_list_elements(PlSqlParser.Select_list_elementsContext ctx) {
        super.exitSelect_list_elements(ctx);
    }

    @Override
    public void enterTable_ref_list(PlSqlParser.Table_ref_listContext ctx) {
        throwNotImplemented(); // super.enterTable_ref_list(ctx);
    }

    @Override
    public void exitTable_ref_list(PlSqlParser.Table_ref_listContext ctx) {
        super.exitTable_ref_list(ctx);
    }

    @Override
    public void enterTable_ref(PlSqlParser.Table_refContext ctx) {
        throwNotImplemented(); // super.enterTable_ref(ctx);
    }

    @Override
    public void exitTable_ref(PlSqlParser.Table_refContext ctx) {
        super.exitTable_ref(ctx);
    }

    @Override
    public void enterTable_ref_aux(PlSqlParser.Table_ref_auxContext ctx) {
        throwNotImplemented(); // super.enterTable_ref_aux(ctx);
    }

    @Override
    public void exitTable_ref_aux(PlSqlParser.Table_ref_auxContext ctx) {
        super.exitTable_ref_aux(ctx);
    }

    @Override
    public void enterTable_ref_aux_internal_one(PlSqlParser.Table_ref_aux_internal_oneContext ctx) {
        throwNotImplemented(); // super.enterTable_ref_aux_internal_one(ctx);
    }

    @Override
    public void exitTable_ref_aux_internal_one(PlSqlParser.Table_ref_aux_internal_oneContext ctx) {
        super.exitTable_ref_aux_internal_one(ctx);
    }

    @Override
    public void enterTable_ref_aux_internal_two(PlSqlParser.Table_ref_aux_internal_twoContext ctx) {
        throwNotImplemented(); // super.enterTable_ref_aux_internal_two(ctx);
    }

    @Override
    public void exitTable_ref_aux_internal_two(PlSqlParser.Table_ref_aux_internal_twoContext ctx) {
        super.exitTable_ref_aux_internal_two(ctx);
    }

    @Override
    public void enterTable_ref_aux_internal_three(PlSqlParser.Table_ref_aux_internal_threeContext ctx) {
        throwNotImplemented(); // super.enterTable_ref_aux_internal_three(ctx);
    }

    @Override
    public void exitTable_ref_aux_internal_three(PlSqlParser.Table_ref_aux_internal_threeContext ctx) {
        super.exitTable_ref_aux_internal_three(ctx);
    }

    @Override
    public void enterJoin_clause(PlSqlParser.Join_clauseContext ctx) {
        throwNotImplemented(); // super.enterJoin_clause(ctx);
    }

    @Override
    public void exitJoin_clause(PlSqlParser.Join_clauseContext ctx) {
        super.exitJoin_clause(ctx);
    }

    @Override
    public void enterJoin_on_part(PlSqlParser.Join_on_partContext ctx) {
        throwNotImplemented(); // super.enterJoin_on_part(ctx);
    }

    @Override
    public void exitJoin_on_part(PlSqlParser.Join_on_partContext ctx) {
        super.exitJoin_on_part(ctx);
    }

    @Override
    public void enterJoin_using_part(PlSqlParser.Join_using_partContext ctx) {
        throwNotImplemented(); // super.enterJoin_using_part(ctx);
    }

    @Override
    public void exitJoin_using_part(PlSqlParser.Join_using_partContext ctx) {
        super.exitJoin_using_part(ctx);
    }

    @Override
    public void enterOuter_join_type(PlSqlParser.Outer_join_typeContext ctx) {
        throwNotImplemented(); // super.enterOuter_join_type(ctx);
    }

    @Override
    public void exitOuter_join_type(PlSqlParser.Outer_join_typeContext ctx) {
        super.exitOuter_join_type(ctx);
    }

    @Override
    public void enterQuery_partition_clause(PlSqlParser.Query_partition_clauseContext ctx) {
        throwNotImplemented(); // super.enterQuery_partition_clause(ctx);
    }

    @Override
    public void exitQuery_partition_clause(PlSqlParser.Query_partition_clauseContext ctx) {
        super.exitQuery_partition_clause(ctx);
    }

    @Override
    public void enterFlashback_query_clause(PlSqlParser.Flashback_query_clauseContext ctx) {
        throwNotImplemented(); // super.enterFlashback_query_clause(ctx);
    }

    @Override
    public void exitFlashback_query_clause(PlSqlParser.Flashback_query_clauseContext ctx) {
        super.exitFlashback_query_clause(ctx);
    }

    @Override
    public void enterPivot_clause(PlSqlParser.Pivot_clauseContext ctx) {
        throwNotImplemented(); // super.enterPivot_clause(ctx);
    }

    @Override
    public void exitPivot_clause(PlSqlParser.Pivot_clauseContext ctx) {
        super.exitPivot_clause(ctx);
    }

    @Override
    public void enterPivot_element(PlSqlParser.Pivot_elementContext ctx) {
        throwNotImplemented(); // super.enterPivot_element(ctx);
    }

    @Override
    public void exitPivot_element(PlSqlParser.Pivot_elementContext ctx) {
        super.exitPivot_element(ctx);
    }

    @Override
    public void enterPivot_for_clause(PlSqlParser.Pivot_for_clauseContext ctx) {
        throwNotImplemented(); // super.enterPivot_for_clause(ctx);
    }

    @Override
    public void exitPivot_for_clause(PlSqlParser.Pivot_for_clauseContext ctx) {
        super.exitPivot_for_clause(ctx);
    }

    @Override
    public void enterPivot_in_clause(PlSqlParser.Pivot_in_clauseContext ctx) {
        throwNotImplemented(); // super.enterPivot_in_clause(ctx);
    }

    @Override
    public void exitPivot_in_clause(PlSqlParser.Pivot_in_clauseContext ctx) {
        super.exitPivot_in_clause(ctx);
    }

    @Override
    public void enterPivot_in_clause_element(PlSqlParser.Pivot_in_clause_elementContext ctx) {
        throwNotImplemented(); // super.enterPivot_in_clause_element(ctx);
    }

    @Override
    public void exitPivot_in_clause_element(PlSqlParser.Pivot_in_clause_elementContext ctx) {
        super.exitPivot_in_clause_element(ctx);
    }

    @Override
    public void enterPivot_in_clause_elements(PlSqlParser.Pivot_in_clause_elementsContext ctx) {
        throwNotImplemented(); // super.enterPivot_in_clause_elements(ctx);
    }

    @Override
    public void exitPivot_in_clause_elements(PlSqlParser.Pivot_in_clause_elementsContext ctx) {
        super.exitPivot_in_clause_elements(ctx);
    }

    @Override
    public void enterUnpivot_clause(PlSqlParser.Unpivot_clauseContext ctx) {
        throwNotImplemented(); // super.enterUnpivot_clause(ctx);
    }

    @Override
    public void exitUnpivot_clause(PlSqlParser.Unpivot_clauseContext ctx) {
        super.exitUnpivot_clause(ctx);
    }

    @Override
    public void enterUnpivot_in_clause(PlSqlParser.Unpivot_in_clauseContext ctx) {
        throwNotImplemented(); // super.enterUnpivot_in_clause(ctx);
    }

    @Override
    public void exitUnpivot_in_clause(PlSqlParser.Unpivot_in_clauseContext ctx) {
        super.exitUnpivot_in_clause(ctx);
    }

    @Override
    public void enterUnpivot_in_elements(PlSqlParser.Unpivot_in_elementsContext ctx) {
        throwNotImplemented(); // super.enterUnpivot_in_elements(ctx);
    }

    @Override
    public void exitUnpivot_in_elements(PlSqlParser.Unpivot_in_elementsContext ctx) {
        super.exitUnpivot_in_elements(ctx);
    }

    @Override
    public void enterHierarchical_query_clause(PlSqlParser.Hierarchical_query_clauseContext ctx) {
        throwNotImplemented(); // super.enterHierarchical_query_clause(ctx);
    }

    @Override
    public void exitHierarchical_query_clause(PlSqlParser.Hierarchical_query_clauseContext ctx) {
        super.exitHierarchical_query_clause(ctx);
    }

    @Override
    public void enterStart_part(PlSqlParser.Start_partContext ctx) {
        throwNotImplemented(); // super.enterStart_part(ctx);
    }

    @Override
    public void exitStart_part(PlSqlParser.Start_partContext ctx) {
        super.exitStart_part(ctx);
    }

    @Override
    public void enterGroup_by_clause(PlSqlParser.Group_by_clauseContext ctx) {
        throwNotImplemented(); // super.enterGroup_by_clause(ctx);
    }

    @Override
    public void exitGroup_by_clause(PlSqlParser.Group_by_clauseContext ctx) {
        super.exitGroup_by_clause(ctx);
    }

    @Override
    public void enterGroup_by_elements(PlSqlParser.Group_by_elementsContext ctx) {
        throwNotImplemented(); // super.enterGroup_by_elements(ctx);
    }

    @Override
    public void exitGroup_by_elements(PlSqlParser.Group_by_elementsContext ctx) {
        super.exitGroup_by_elements(ctx);
    }

    @Override
    public void enterRollup_cube_clause(PlSqlParser.Rollup_cube_clauseContext ctx) {
        throwNotImplemented(); // super.enterRollup_cube_clause(ctx);
    }

    @Override
    public void exitRollup_cube_clause(PlSqlParser.Rollup_cube_clauseContext ctx) {
        super.exitRollup_cube_clause(ctx);
    }

    @Override
    public void enterGrouping_sets_clause(PlSqlParser.Grouping_sets_clauseContext ctx) {
        throwNotImplemented(); // super.enterGrouping_sets_clause(ctx);
    }

    @Override
    public void exitGrouping_sets_clause(PlSqlParser.Grouping_sets_clauseContext ctx) {
        super.exitGrouping_sets_clause(ctx);
    }

    @Override
    public void enterGrouping_sets_elements(PlSqlParser.Grouping_sets_elementsContext ctx) {
        throwNotImplemented(); // super.enterGrouping_sets_elements(ctx);
    }

    @Override
    public void exitGrouping_sets_elements(PlSqlParser.Grouping_sets_elementsContext ctx) {
        super.exitGrouping_sets_elements(ctx);
    }

    @Override
    public void enterHaving_clause(PlSqlParser.Having_clauseContext ctx) {
        throwNotImplemented(); // super.enterHaving_clause(ctx);
    }

    @Override
    public void exitHaving_clause(PlSqlParser.Having_clauseContext ctx) {
        super.exitHaving_clause(ctx);
    }

    @Override
    public void enterModel_clause(PlSqlParser.Model_clauseContext ctx) {
        throwNotImplemented(); // super.enterModel_clause(ctx);
    }

    @Override
    public void exitModel_clause(PlSqlParser.Model_clauseContext ctx) {
        super.exitModel_clause(ctx);
    }

    @Override
    public void enterCell_reference_options(PlSqlParser.Cell_reference_optionsContext ctx) {
        throwNotImplemented(); // super.enterCell_reference_options(ctx);
    }

    @Override
    public void exitCell_reference_options(PlSqlParser.Cell_reference_optionsContext ctx) {
        super.exitCell_reference_options(ctx);
    }

    @Override
    public void enterReturn_rows_clause(PlSqlParser.Return_rows_clauseContext ctx) {
        throwNotImplemented(); // super.enterReturn_rows_clause(ctx);
    }

    @Override
    public void exitReturn_rows_clause(PlSqlParser.Return_rows_clauseContext ctx) {
        super.exitReturn_rows_clause(ctx);
    }

    @Override
    public void enterReference_model(PlSqlParser.Reference_modelContext ctx) {
        throwNotImplemented(); // super.enterReference_model(ctx);
    }

    @Override
    public void exitReference_model(PlSqlParser.Reference_modelContext ctx) {
        super.exitReference_model(ctx);
    }

    @Override
    public void enterMain_model(PlSqlParser.Main_modelContext ctx) {
        throwNotImplemented(); // super.enterMain_model(ctx);
    }

    @Override
    public void exitMain_model(PlSqlParser.Main_modelContext ctx) {
        super.exitMain_model(ctx);
    }

    @Override
    public void enterModel_column_clauses(PlSqlParser.Model_column_clausesContext ctx) {
        throwNotImplemented(); // super.enterModel_column_clauses(ctx);
    }

    @Override
    public void exitModel_column_clauses(PlSqlParser.Model_column_clausesContext ctx) {
        super.exitModel_column_clauses(ctx);
    }

    @Override
    public void enterModel_column_partition_part(PlSqlParser.Model_column_partition_partContext ctx) {
        throwNotImplemented(); // super.enterModel_column_partition_part(ctx);
    }

    @Override
    public void exitModel_column_partition_part(PlSqlParser.Model_column_partition_partContext ctx) {
        super.exitModel_column_partition_part(ctx);
    }

    @Override
    public void enterModel_column_list(PlSqlParser.Model_column_listContext ctx) {
        throwNotImplemented(); // super.enterModel_column_list(ctx);
    }

    @Override
    public void exitModel_column_list(PlSqlParser.Model_column_listContext ctx) {
        super.exitModel_column_list(ctx);
    }

    @Override
    public void enterModel_column(PlSqlParser.Model_columnContext ctx) {
        throwNotImplemented(); // super.enterModel_column(ctx);
    }

    @Override
    public void exitModel_column(PlSqlParser.Model_columnContext ctx) {
        super.exitModel_column(ctx);
    }

    @Override
    public void enterModel_rules_clause(PlSqlParser.Model_rules_clauseContext ctx) {
        throwNotImplemented(); // super.enterModel_rules_clause(ctx);
    }

    @Override
    public void exitModel_rules_clause(PlSqlParser.Model_rules_clauseContext ctx) {
        super.exitModel_rules_clause(ctx);
    }

    @Override
    public void enterModel_rules_part(PlSqlParser.Model_rules_partContext ctx) {
        throwNotImplemented(); // super.enterModel_rules_part(ctx);
    }

    @Override
    public void exitModel_rules_part(PlSqlParser.Model_rules_partContext ctx) {
        super.exitModel_rules_part(ctx);
    }

    @Override
    public void enterModel_rules_element(PlSqlParser.Model_rules_elementContext ctx) {
        throwNotImplemented(); // super.enterModel_rules_element(ctx);
    }

    @Override
    public void exitModel_rules_element(PlSqlParser.Model_rules_elementContext ctx) {
        super.exitModel_rules_element(ctx);
    }

    @Override
    public void enterCell_assignment(PlSqlParser.Cell_assignmentContext ctx) {
        throwNotImplemented(); // super.enterCell_assignment(ctx);
    }

    @Override
    public void exitCell_assignment(PlSqlParser.Cell_assignmentContext ctx) {
        super.exitCell_assignment(ctx);
    }

    @Override
    public void enterModel_iterate_clause(PlSqlParser.Model_iterate_clauseContext ctx) {
        throwNotImplemented(); // super.enterModel_iterate_clause(ctx);
    }

    @Override
    public void exitModel_iterate_clause(PlSqlParser.Model_iterate_clauseContext ctx) {
        super.exitModel_iterate_clause(ctx);
    }

    @Override
    public void enterUntil_part(PlSqlParser.Until_partContext ctx) {
        throwNotImplemented(); // super.enterUntil_part(ctx);
    }

    @Override
    public void exitUntil_part(PlSqlParser.Until_partContext ctx) {
        super.exitUntil_part(ctx);
    }

    @Override
    public void enterOrder_by_clause(PlSqlParser.Order_by_clauseContext ctx) {
        throwNotImplemented(); // super.enterOrder_by_clause(ctx);
    }

    @Override
    public void exitOrder_by_clause(PlSqlParser.Order_by_clauseContext ctx) {
        super.exitOrder_by_clause(ctx);
    }

    @Override
    public void enterOrder_by_elements(PlSqlParser.Order_by_elementsContext ctx) {
        throwNotImplemented(); // super.enterOrder_by_elements(ctx);
    }

    @Override
    public void exitOrder_by_elements(PlSqlParser.Order_by_elementsContext ctx) {
        super.exitOrder_by_elements(ctx);
    }

    @Override
    public void enterOffset_clause(PlSqlParser.Offset_clauseContext ctx) {
        throwNotImplemented(); // super.enterOffset_clause(ctx);
    }

    @Override
    public void exitOffset_clause(PlSqlParser.Offset_clauseContext ctx) {
        super.exitOffset_clause(ctx);
    }

    @Override
    public void enterFetch_clause(PlSqlParser.Fetch_clauseContext ctx) {
        throwNotImplemented(); // super.enterFetch_clause(ctx);
    }

    @Override
    public void exitFetch_clause(PlSqlParser.Fetch_clauseContext ctx) {
        super.exitFetch_clause(ctx);
    }

    @Override
    public void enterFor_update_clause(PlSqlParser.For_update_clauseContext ctx) {
        throwNotImplemented(); // super.enterFor_update_clause(ctx);
    }

    @Override
    public void exitFor_update_clause(PlSqlParser.For_update_clauseContext ctx) {
        super.exitFor_update_clause(ctx);
    }

    @Override
    public void enterFor_update_of_part(PlSqlParser.For_update_of_partContext ctx) {
        throwNotImplemented(); // super.enterFor_update_of_part(ctx);
    }

    @Override
    public void exitFor_update_of_part(PlSqlParser.For_update_of_partContext ctx) {
        super.exitFor_update_of_part(ctx);
    }

    @Override
    public void enterFor_update_options(PlSqlParser.For_update_optionsContext ctx) {
        throwNotImplemented(); // super.enterFor_update_options(ctx);
    }

    @Override
    public void exitFor_update_options(PlSqlParser.For_update_optionsContext ctx) {
        super.exitFor_update_options(ctx);
    }

    @Override
    public void enterUpdate_statement(PlSqlParser.Update_statementContext ctx) {
        throwNotImplemented(); // super.enterUpdate_statement(ctx);
    }

    @Override
    public void exitUpdate_statement(PlSqlParser.Update_statementContext ctx) {
        super.exitUpdate_statement(ctx);
    }

    @Override
    public void enterUpdate_set_clause(PlSqlParser.Update_set_clauseContext ctx) {
        throwNotImplemented(); // super.enterUpdate_set_clause(ctx);
    }

    @Override
    public void exitUpdate_set_clause(PlSqlParser.Update_set_clauseContext ctx) {
        super.exitUpdate_set_clause(ctx);
    }

    @Override
    public void enterColumn_based_update_set_clause(PlSqlParser.Column_based_update_set_clauseContext ctx) {
        throwNotImplemented(); // super.enterColumn_based_update_set_clause(ctx);
    }

    @Override
    public void exitColumn_based_update_set_clause(PlSqlParser.Column_based_update_set_clauseContext ctx) {
        super.exitColumn_based_update_set_clause(ctx);
    }

    @Override
    public void enterDelete_statement(PlSqlParser.Delete_statementContext ctx) {
        throwNotImplemented(); // super.enterDelete_statement(ctx);
    }

    @Override
    public void exitDelete_statement(PlSqlParser.Delete_statementContext ctx) {
        super.exitDelete_statement(ctx);
    }

    @Override
    public void enterInsert_statement(PlSqlParser.Insert_statementContext ctx) {
        throwNotImplemented(); // super.enterInsert_statement(ctx);
    }

    @Override
    public void exitInsert_statement(PlSqlParser.Insert_statementContext ctx) {
        super.exitInsert_statement(ctx);
    }

    @Override
    public void enterSingle_table_insert(PlSqlParser.Single_table_insertContext ctx) {
        throwNotImplemented(); // super.enterSingle_table_insert(ctx);
    }

    @Override
    public void exitSingle_table_insert(PlSqlParser.Single_table_insertContext ctx) {
        super.exitSingle_table_insert(ctx);
    }

    @Override
    public void enterMulti_table_insert(PlSqlParser.Multi_table_insertContext ctx) {
        throwNotImplemented(); // super.enterMulti_table_insert(ctx);
    }

    @Override
    public void exitMulti_table_insert(PlSqlParser.Multi_table_insertContext ctx) {
        super.exitMulti_table_insert(ctx);
    }

    @Override
    public void enterMulti_table_element(PlSqlParser.Multi_table_elementContext ctx) {
        throwNotImplemented(); // super.enterMulti_table_element(ctx);
    }

    @Override
    public void exitMulti_table_element(PlSqlParser.Multi_table_elementContext ctx) {
        super.exitMulti_table_element(ctx);
    }

    @Override
    public void enterConditional_insert_clause(PlSqlParser.Conditional_insert_clauseContext ctx) {
        throwNotImplemented(); // super.enterConditional_insert_clause(ctx);
    }

    @Override
    public void exitConditional_insert_clause(PlSqlParser.Conditional_insert_clauseContext ctx) {
        super.exitConditional_insert_clause(ctx);
    }

    @Override
    public void enterConditional_insert_when_part(PlSqlParser.Conditional_insert_when_partContext ctx) {
        throwNotImplemented(); // super.enterConditional_insert_when_part(ctx);
    }

    @Override
    public void exitConditional_insert_when_part(PlSqlParser.Conditional_insert_when_partContext ctx) {
        super.exitConditional_insert_when_part(ctx);
    }

    @Override
    public void enterConditional_insert_else_part(PlSqlParser.Conditional_insert_else_partContext ctx) {
        throwNotImplemented(); // super.enterConditional_insert_else_part(ctx);
    }

    @Override
    public void exitConditional_insert_else_part(PlSqlParser.Conditional_insert_else_partContext ctx) {
        super.exitConditional_insert_else_part(ctx);
    }

    @Override
    public void enterInsert_into_clause(PlSqlParser.Insert_into_clauseContext ctx) {
        throwNotImplemented(); // super.enterInsert_into_clause(ctx);
    }

    @Override
    public void exitInsert_into_clause(PlSqlParser.Insert_into_clauseContext ctx) {
        super.exitInsert_into_clause(ctx);
    }

    @Override
    public void enterValues_clause(PlSqlParser.Values_clauseContext ctx) {
        throwNotImplemented(); // super.enterValues_clause(ctx);
    }

    @Override
    public void exitValues_clause(PlSqlParser.Values_clauseContext ctx) {
        super.exitValues_clause(ctx);
    }

    @Override
    public void enterMerge_statement(PlSqlParser.Merge_statementContext ctx) {
        throwNotImplemented(); // super.enterMerge_statement(ctx);
    }

    @Override
    public void exitMerge_statement(PlSqlParser.Merge_statementContext ctx) {
        super.exitMerge_statement(ctx);
    }

    @Override
    public void enterMerge_update_clause(PlSqlParser.Merge_update_clauseContext ctx) {
        throwNotImplemented(); // super.enterMerge_update_clause(ctx);
    }

    @Override
    public void exitMerge_update_clause(PlSqlParser.Merge_update_clauseContext ctx) {
        super.exitMerge_update_clause(ctx);
    }

    @Override
    public void enterMerge_element(PlSqlParser.Merge_elementContext ctx) {
        throwNotImplemented(); // super.enterMerge_element(ctx);
    }

    @Override
    public void exitMerge_element(PlSqlParser.Merge_elementContext ctx) {
        super.exitMerge_element(ctx);
    }

    @Override
    public void enterMerge_update_delete_part(PlSqlParser.Merge_update_delete_partContext ctx) {
        throwNotImplemented(); // super.enterMerge_update_delete_part(ctx);
    }

    @Override
    public void exitMerge_update_delete_part(PlSqlParser.Merge_update_delete_partContext ctx) {
        super.exitMerge_update_delete_part(ctx);
    }

    @Override
    public void enterMerge_insert_clause(PlSqlParser.Merge_insert_clauseContext ctx) {
        throwNotImplemented(); // super.enterMerge_insert_clause(ctx);
    }

    @Override
    public void exitMerge_insert_clause(PlSqlParser.Merge_insert_clauseContext ctx) {
        super.exitMerge_insert_clause(ctx);
    }

    @Override
    public void enterSelected_tableview(PlSqlParser.Selected_tableviewContext ctx) {
        throwNotImplemented(); // super.enterSelected_tableview(ctx);
    }

    @Override
    public void exitSelected_tableview(PlSqlParser.Selected_tableviewContext ctx) {
        super.exitSelected_tableview(ctx);
    }

    @Override
    public void enterLock_table_statement(PlSqlParser.Lock_table_statementContext ctx) {
        throwNotImplemented(); // super.enterLock_table_statement(ctx);
    }

    @Override
    public void exitLock_table_statement(PlSqlParser.Lock_table_statementContext ctx) {
        super.exitLock_table_statement(ctx);
    }

    @Override
    public void enterWait_nowait_part(PlSqlParser.Wait_nowait_partContext ctx) {
        throwNotImplemented(); // super.enterWait_nowait_part(ctx);
    }

    @Override
    public void exitWait_nowait_part(PlSqlParser.Wait_nowait_partContext ctx) {
        super.exitWait_nowait_part(ctx);
    }

    @Override
    public void enterLock_table_element(PlSqlParser.Lock_table_elementContext ctx) {
        throwNotImplemented(); // super.enterLock_table_element(ctx);
    }

    @Override
    public void exitLock_table_element(PlSqlParser.Lock_table_elementContext ctx) {
        super.exitLock_table_element(ctx);
    }

    @Override
    public void enterLock_mode(PlSqlParser.Lock_modeContext ctx) {
        throwNotImplemented(); // super.enterLock_mode(ctx);
    }

    @Override
    public void exitLock_mode(PlSqlParser.Lock_modeContext ctx) {
        super.exitLock_mode(ctx);
    }

    @Override
    public void enterGeneral_table_ref(PlSqlParser.General_table_refContext ctx) {
        throwNotImplemented(); // super.enterGeneral_table_ref(ctx);
    }

    @Override
    public void exitGeneral_table_ref(PlSqlParser.General_table_refContext ctx) {
        super.exitGeneral_table_ref(ctx);
    }

    @Override
    public void enterStatic_returning_clause(PlSqlParser.Static_returning_clauseContext ctx) {
        throwNotImplemented(); // super.enterStatic_returning_clause(ctx);
    }

    @Override
    public void exitStatic_returning_clause(PlSqlParser.Static_returning_clauseContext ctx) {
        super.exitStatic_returning_clause(ctx);
    }

    @Override
    public void enterError_logging_clause(PlSqlParser.Error_logging_clauseContext ctx) {
        throwNotImplemented(); // super.enterError_logging_clause(ctx);
    }

    @Override
    public void exitError_logging_clause(PlSqlParser.Error_logging_clauseContext ctx) {
        super.exitError_logging_clause(ctx);
    }

    @Override
    public void enterError_logging_into_part(PlSqlParser.Error_logging_into_partContext ctx) {
        throwNotImplemented(); // super.enterError_logging_into_part(ctx);
    }

    @Override
    public void exitError_logging_into_part(PlSqlParser.Error_logging_into_partContext ctx) {
        super.exitError_logging_into_part(ctx);
    }

    @Override
    public void enterError_logging_reject_part(PlSqlParser.Error_logging_reject_partContext ctx) {
        throwNotImplemented(); // super.enterError_logging_reject_part(ctx);
    }

    @Override
    public void exitError_logging_reject_part(PlSqlParser.Error_logging_reject_partContext ctx) {
        super.exitError_logging_reject_part(ctx);
    }

    @Override
    public void enterDml_table_expression_clause(PlSqlParser.Dml_table_expression_clauseContext ctx) {
        throwNotImplemented(); // super.enterDml_table_expression_clause(ctx);
    }

    @Override
    public void exitDml_table_expression_clause(PlSqlParser.Dml_table_expression_clauseContext ctx) {
        super.exitDml_table_expression_clause(ctx);
    }

    @Override
    public void enterTable_collection_expression(PlSqlParser.Table_collection_expressionContext ctx) {
        throwNotImplemented(); // super.enterTable_collection_expression(ctx);
    }

    @Override
    public void exitTable_collection_expression(PlSqlParser.Table_collection_expressionContext ctx) {
        super.exitTable_collection_expression(ctx);
    }

    @Override
    public void enterSubquery_restriction_clause(PlSqlParser.Subquery_restriction_clauseContext ctx) {
        throwNotImplemented(); // super.enterSubquery_restriction_clause(ctx);
    }

    @Override
    public void exitSubquery_restriction_clause(PlSqlParser.Subquery_restriction_clauseContext ctx) {
        super.exitSubquery_restriction_clause(ctx);
    }

    @Override
    public void enterSample_clause(PlSqlParser.Sample_clauseContext ctx) {
        throwNotImplemented(); // super.enterSample_clause(ctx);
    }

    @Override
    public void exitSample_clause(PlSqlParser.Sample_clauseContext ctx) {
        super.exitSample_clause(ctx);
    }

    @Override
    public void enterSeed_part(PlSqlParser.Seed_partContext ctx) {
        throwNotImplemented(); // super.enterSeed_part(ctx);
    }

    @Override
    public void exitSeed_part(PlSqlParser.Seed_partContext ctx) {
        super.exitSeed_part(ctx);
    }

    @Override
    public void enterCondition(PlSqlParser.ConditionContext ctx) {
        throwNotImplemented(); // super.enterCondition(ctx);
    }

    @Override
    public void exitCondition(PlSqlParser.ConditionContext ctx) {
        super.exitCondition(ctx);
    }

    @Override
    public void enterExpressions(PlSqlParser.ExpressionsContext ctx) {
        throwNotImplemented(); // super.enterExpressions(ctx);
    }

    @Override
    public void exitExpressions(PlSqlParser.ExpressionsContext ctx) {
        super.exitExpressions(ctx);
    }

    @Override
    public void enterExpression(PlSqlParser.ExpressionContext ctx) {
        throwNotImplemented(); // super.enterExpression(ctx);
    }

    @Override
    public void exitExpression(PlSqlParser.ExpressionContext ctx) {
        super.exitExpression(ctx);
    }

    @Override
    public void enterCursor_expression(PlSqlParser.Cursor_expressionContext ctx) {
        throwNotImplemented(); // super.enterCursor_expression(ctx);
    }

    @Override
    public void exitCursor_expression(PlSqlParser.Cursor_expressionContext ctx) {
        super.exitCursor_expression(ctx);
    }

    @Override
    public void enterLogical_expression(PlSqlParser.Logical_expressionContext ctx) {
        throwNotImplemented(); // super.enterLogical_expression(ctx);
    }

    @Override
    public void exitLogical_expression(PlSqlParser.Logical_expressionContext ctx) {
        super.exitLogical_expression(ctx);
    }

    @Override
    public void enterMultiset_expression(PlSqlParser.Multiset_expressionContext ctx) {
        throwNotImplemented(); // super.enterMultiset_expression(ctx);
    }

    @Override
    public void exitMultiset_expression(PlSqlParser.Multiset_expressionContext ctx) {
        super.exitMultiset_expression(ctx);
    }

    @Override
    public void enterRelational_expression(PlSqlParser.Relational_expressionContext ctx) {
        throwNotImplemented(); // super.enterRelational_expression(ctx);
    }

    @Override
    public void exitRelational_expression(PlSqlParser.Relational_expressionContext ctx) {
        super.exitRelational_expression(ctx);
    }

    @Override
    public void enterCompound_expression(PlSqlParser.Compound_expressionContext ctx) {
        throwNotImplemented(); // super.enterCompound_expression(ctx);
    }

    @Override
    public void exitCompound_expression(PlSqlParser.Compound_expressionContext ctx) {
        super.exitCompound_expression(ctx);
    }

    @Override
    public void enterRelational_operator(PlSqlParser.Relational_operatorContext ctx) {
        throwNotImplemented(); // super.enterRelational_operator(ctx);
    }

    @Override
    public void exitRelational_operator(PlSqlParser.Relational_operatorContext ctx) {
        super.exitRelational_operator(ctx);
    }

    @Override
    public void enterIn_elements(PlSqlParser.In_elementsContext ctx) {
        throwNotImplemented(); // super.enterIn_elements(ctx);
    }

    @Override
    public void exitIn_elements(PlSqlParser.In_elementsContext ctx) {
        super.exitIn_elements(ctx);
    }

    @Override
    public void enterBetween_elements(PlSqlParser.Between_elementsContext ctx) {
        throwNotImplemented(); // super.enterBetween_elements(ctx);
    }

    @Override
    public void exitBetween_elements(PlSqlParser.Between_elementsContext ctx) {
        super.exitBetween_elements(ctx);
    }

    @Override
    public void enterConcatenation(PlSqlParser.ConcatenationContext ctx) {
        throwNotImplemented(); // super.enterConcatenation(ctx);
    }

    @Override
    public void exitConcatenation(PlSqlParser.ConcatenationContext ctx) {
        super.exitConcatenation(ctx);
    }

    @Override
    public void enterInterval_expression(PlSqlParser.Interval_expressionContext ctx) {
        throwNotImplemented(); // super.enterInterval_expression(ctx);
    }

    @Override
    public void exitInterval_expression(PlSqlParser.Interval_expressionContext ctx) {
        super.exitInterval_expression(ctx);
    }

    @Override
    public void enterModel_expression(PlSqlParser.Model_expressionContext ctx) {
        throwNotImplemented(); // super.enterModel_expression(ctx);
    }

    @Override
    public void exitModel_expression(PlSqlParser.Model_expressionContext ctx) {
        super.exitModel_expression(ctx);
    }

    @Override
    public void enterModel_expression_element(PlSqlParser.Model_expression_elementContext ctx) {
        throwNotImplemented(); // super.enterModel_expression_element(ctx);
    }

    @Override
    public void exitModel_expression_element(PlSqlParser.Model_expression_elementContext ctx) {
        super.exitModel_expression_element(ctx);
    }

    @Override
    public void enterSingle_column_for_loop(PlSqlParser.Single_column_for_loopContext ctx) {
        throwNotImplemented(); // super.enterSingle_column_for_loop(ctx);
    }

    @Override
    public void exitSingle_column_for_loop(PlSqlParser.Single_column_for_loopContext ctx) {
        super.exitSingle_column_for_loop(ctx);
    }

    @Override
    public void enterMulti_column_for_loop(PlSqlParser.Multi_column_for_loopContext ctx) {
        throwNotImplemented(); // super.enterMulti_column_for_loop(ctx);
    }

    @Override
    public void exitMulti_column_for_loop(PlSqlParser.Multi_column_for_loopContext ctx) {
        super.exitMulti_column_for_loop(ctx);
    }

    @Override
    public void enterUnary_expression(PlSqlParser.Unary_expressionContext ctx) {
        throwNotImplemented(); // super.enterUnary_expression(ctx);
    }

    @Override
    public void exitUnary_expression(PlSqlParser.Unary_expressionContext ctx) {
        super.exitUnary_expression(ctx);
    }

    @Override
    public void enterCase_statement(PlSqlParser.Case_statementContext ctx) {
        throwNotImplemented(); // super.enterCase_statement(ctx);
    }

    @Override
    public void exitCase_statement(PlSqlParser.Case_statementContext ctx) {
        super.exitCase_statement(ctx);
    }

    @Override
    public void enterSimple_case_statement(PlSqlParser.Simple_case_statementContext ctx) {
        throwNotImplemented(); // super.enterSimple_case_statement(ctx);
    }

    @Override
    public void exitSimple_case_statement(PlSqlParser.Simple_case_statementContext ctx) {
        super.exitSimple_case_statement(ctx);
    }

    @Override
    public void enterSimple_case_when_part(PlSqlParser.Simple_case_when_partContext ctx) {
        throwNotImplemented(); // super.enterSimple_case_when_part(ctx);
    }

    @Override
    public void exitSimple_case_when_part(PlSqlParser.Simple_case_when_partContext ctx) {
        super.exitSimple_case_when_part(ctx);
    }

    @Override
    public void enterSearched_case_statement(PlSqlParser.Searched_case_statementContext ctx) {
        throwNotImplemented(); // super.enterSearched_case_statement(ctx);
    }

    @Override
    public void exitSearched_case_statement(PlSqlParser.Searched_case_statementContext ctx) {
        super.exitSearched_case_statement(ctx);
    }

    @Override
    public void enterSearched_case_when_part(PlSqlParser.Searched_case_when_partContext ctx) {
        throwNotImplemented(); // super.enterSearched_case_when_part(ctx);
    }

    @Override
    public void exitSearched_case_when_part(PlSqlParser.Searched_case_when_partContext ctx) {
        super.exitSearched_case_when_part(ctx);
    }

    @Override
    public void enterCase_else_part(PlSqlParser.Case_else_partContext ctx) {
        throwNotImplemented(); // super.enterCase_else_part(ctx);
    }

    @Override
    public void exitCase_else_part(PlSqlParser.Case_else_partContext ctx) {
        super.exitCase_else_part(ctx);
    }

    @Override
    public void enterAtom(PlSqlParser.AtomContext ctx) {
        throwNotImplemented(); // super.enterAtom(ctx);
    }

    @Override
    public void exitAtom(PlSqlParser.AtomContext ctx) {
        super.exitAtom(ctx);
    }

    @Override
    public void enterQuantified_expression(PlSqlParser.Quantified_expressionContext ctx) {
        throwNotImplemented(); // super.enterQuantified_expression(ctx);
    }

    @Override
    public void exitQuantified_expression(PlSqlParser.Quantified_expressionContext ctx) {
        super.exitQuantified_expression(ctx);
    }

    @Override
    public void enterString_function(PlSqlParser.String_functionContext ctx) {
        throwNotImplemented(); // super.enterString_function(ctx);
    }

    @Override
    public void exitString_function(PlSqlParser.String_functionContext ctx) {
        super.exitString_function(ctx);
    }

    @Override
    public void enterStandard_function(PlSqlParser.Standard_functionContext ctx) {
        throwNotImplemented(); // super.enterStandard_function(ctx);
    }

    @Override
    public void exitStandard_function(PlSqlParser.Standard_functionContext ctx) {
        super.exitStandard_function(ctx);
    }

    @Override
    public void enterLiteral(PlSqlParser.LiteralContext ctx) {
        throwNotImplemented(); // super.enterLiteral(ctx);
    }

    @Override
    public void exitLiteral(PlSqlParser.LiteralContext ctx) {
        super.exitLiteral(ctx);
    }

    @Override
    public void enterNumeric_function_wrapper(PlSqlParser.Numeric_function_wrapperContext ctx) {
        throwNotImplemented(); // super.enterNumeric_function_wrapper(ctx);
    }

    @Override
    public void exitNumeric_function_wrapper(PlSqlParser.Numeric_function_wrapperContext ctx) {
        super.exitNumeric_function_wrapper(ctx);
    }

    @Override
    public void enterNumeric_function(PlSqlParser.Numeric_functionContext ctx) {
        throwNotImplemented(); // super.enterNumeric_function(ctx);
    }

    @Override
    public void exitNumeric_function(PlSqlParser.Numeric_functionContext ctx) {
        super.exitNumeric_function(ctx);
    }

    @Override
    public void enterOther_function(PlSqlParser.Other_functionContext ctx) {
        throwNotImplemented(); // super.enterOther_function(ctx);
    }

    @Override
    public void exitOther_function(PlSqlParser.Other_functionContext ctx) {
        super.exitOther_function(ctx);
    }

    @Override
    public void enterOver_clause_keyword(PlSqlParser.Over_clause_keywordContext ctx) {
        throwNotImplemented(); // super.enterOver_clause_keyword(ctx);
    }

    @Override
    public void exitOver_clause_keyword(PlSqlParser.Over_clause_keywordContext ctx) {
        super.exitOver_clause_keyword(ctx);
    }

    @Override
    public void enterWithin_or_over_clause_keyword(PlSqlParser.Within_or_over_clause_keywordContext ctx) {
        throwNotImplemented(); // super.enterWithin_or_over_clause_keyword(ctx);
    }

    @Override
    public void exitWithin_or_over_clause_keyword(PlSqlParser.Within_or_over_clause_keywordContext ctx) {
        super.exitWithin_or_over_clause_keyword(ctx);
    }

    @Override
    public void enterStandard_prediction_function_keyword(PlSqlParser.Standard_prediction_function_keywordContext ctx) {
        throwNotImplemented(); // super.enterStandard_prediction_function_keyword(ctx);
    }

    @Override
    public void exitStandard_prediction_function_keyword(PlSqlParser.Standard_prediction_function_keywordContext ctx) {
        super.exitStandard_prediction_function_keyword(ctx);
    }

    @Override
    public void enterOver_clause(PlSqlParser.Over_clauseContext ctx) {
        throwNotImplemented(); // super.enterOver_clause(ctx);
    }

    @Override
    public void exitOver_clause(PlSqlParser.Over_clauseContext ctx) {
        super.exitOver_clause(ctx);
    }

    @Override
    public void enterWindowing_clause(PlSqlParser.Windowing_clauseContext ctx) {
        throwNotImplemented(); // super.enterWindowing_clause(ctx);
    }

    @Override
    public void exitWindowing_clause(PlSqlParser.Windowing_clauseContext ctx) {
        super.exitWindowing_clause(ctx);
    }

    @Override
    public void enterWindowing_type(PlSqlParser.Windowing_typeContext ctx) {
        throwNotImplemented(); // super.enterWindowing_type(ctx);
    }

    @Override
    public void exitWindowing_type(PlSqlParser.Windowing_typeContext ctx) {
        super.exitWindowing_type(ctx);
    }

    @Override
    public void enterWindowing_elements(PlSqlParser.Windowing_elementsContext ctx) {
        throwNotImplemented(); // super.enterWindowing_elements(ctx);
    }

    @Override
    public void exitWindowing_elements(PlSqlParser.Windowing_elementsContext ctx) {
        super.exitWindowing_elements(ctx);
    }

    @Override
    public void enterUsing_clause(PlSqlParser.Using_clauseContext ctx) {
        throwNotImplemented(); // super.enterUsing_clause(ctx);
    }

    @Override
    public void exitUsing_clause(PlSqlParser.Using_clauseContext ctx) {
        super.exitUsing_clause(ctx);
    }

    @Override
    public void enterUsing_element(PlSqlParser.Using_elementContext ctx) {
        throwNotImplemented(); // super.enterUsing_element(ctx);
    }

    @Override
    public void exitUsing_element(PlSqlParser.Using_elementContext ctx) {
        super.exitUsing_element(ctx);
    }

    @Override
    public void enterCollect_order_by_part(PlSqlParser.Collect_order_by_partContext ctx) {
        throwNotImplemented(); // super.enterCollect_order_by_part(ctx);
    }

    @Override
    public void exitCollect_order_by_part(PlSqlParser.Collect_order_by_partContext ctx) {
        super.exitCollect_order_by_part(ctx);
    }

    @Override
    public void enterWithin_or_over_part(PlSqlParser.Within_or_over_partContext ctx) {
        throwNotImplemented(); // super.enterWithin_or_over_part(ctx);
    }

    @Override
    public void exitWithin_or_over_part(PlSqlParser.Within_or_over_partContext ctx) {
        super.exitWithin_or_over_part(ctx);
    }

    @Override
    public void enterCost_matrix_clause(PlSqlParser.Cost_matrix_clauseContext ctx) {
        throwNotImplemented(); // super.enterCost_matrix_clause(ctx);
    }

    @Override
    public void exitCost_matrix_clause(PlSqlParser.Cost_matrix_clauseContext ctx) {
        super.exitCost_matrix_clause(ctx);
    }

    @Override
    public void enterXml_passing_clause(PlSqlParser.Xml_passing_clauseContext ctx) {
        throwNotImplemented(); // super.enterXml_passing_clause(ctx);
    }

    @Override
    public void exitXml_passing_clause(PlSqlParser.Xml_passing_clauseContext ctx) {
        super.exitXml_passing_clause(ctx);
    }

    @Override
    public void enterXml_attributes_clause(PlSqlParser.Xml_attributes_clauseContext ctx) {
        throwNotImplemented(); // super.enterXml_attributes_clause(ctx);
    }

    @Override
    public void exitXml_attributes_clause(PlSqlParser.Xml_attributes_clauseContext ctx) {
        super.exitXml_attributes_clause(ctx);
    }

    @Override
    public void enterXml_namespaces_clause(PlSqlParser.Xml_namespaces_clauseContext ctx) {
        throwNotImplemented(); // super.enterXml_namespaces_clause(ctx);
    }

    @Override
    public void exitXml_namespaces_clause(PlSqlParser.Xml_namespaces_clauseContext ctx) {
        super.exitXml_namespaces_clause(ctx);
    }

    @Override
    public void enterXml_table_column(PlSqlParser.Xml_table_columnContext ctx) {
        throwNotImplemented(); // super.enterXml_table_column(ctx);
    }

    @Override
    public void exitXml_table_column(PlSqlParser.Xml_table_columnContext ctx) {
        super.exitXml_table_column(ctx);
    }

    @Override
    public void enterXml_general_default_part(PlSqlParser.Xml_general_default_partContext ctx) {
        throwNotImplemented(); // super.enterXml_general_default_part(ctx);
    }

    @Override
    public void exitXml_general_default_part(PlSqlParser.Xml_general_default_partContext ctx) {
        super.exitXml_general_default_part(ctx);
    }

    @Override
    public void enterXml_multiuse_expression_element(PlSqlParser.Xml_multiuse_expression_elementContext ctx) {
        throwNotImplemented(); // super.enterXml_multiuse_expression_element(ctx);
    }

    @Override
    public void exitXml_multiuse_expression_element(PlSqlParser.Xml_multiuse_expression_elementContext ctx) {
        super.exitXml_multiuse_expression_element(ctx);
    }

    @Override
    public void enterXmlroot_param_version_part(PlSqlParser.Xmlroot_param_version_partContext ctx) {
        throwNotImplemented(); // super.enterXmlroot_param_version_part(ctx);
    }

    @Override
    public void exitXmlroot_param_version_part(PlSqlParser.Xmlroot_param_version_partContext ctx) {
        super.exitXmlroot_param_version_part(ctx);
    }

    @Override
    public void enterXmlroot_param_standalone_part(PlSqlParser.Xmlroot_param_standalone_partContext ctx) {
        throwNotImplemented(); // super.enterXmlroot_param_standalone_part(ctx);
    }

    @Override
    public void exitXmlroot_param_standalone_part(PlSqlParser.Xmlroot_param_standalone_partContext ctx) {
        super.exitXmlroot_param_standalone_part(ctx);
    }

    @Override
    public void enterXmlserialize_param_enconding_part(PlSqlParser.Xmlserialize_param_enconding_partContext ctx) {
        throwNotImplemented(); // super.enterXmlserialize_param_enconding_part(ctx);
    }

    @Override
    public void exitXmlserialize_param_enconding_part(PlSqlParser.Xmlserialize_param_enconding_partContext ctx) {
        super.exitXmlserialize_param_enconding_part(ctx);
    }

    @Override
    public void enterXmlserialize_param_version_part(PlSqlParser.Xmlserialize_param_version_partContext ctx) {
        throwNotImplemented(); // super.enterXmlserialize_param_version_part(ctx);
    }

    @Override
    public void exitXmlserialize_param_version_part(PlSqlParser.Xmlserialize_param_version_partContext ctx) {
        super.exitXmlserialize_param_version_part(ctx);
    }

    @Override
    public void enterXmlserialize_param_ident_part(PlSqlParser.Xmlserialize_param_ident_partContext ctx) {
        throwNotImplemented(); // super.enterXmlserialize_param_ident_part(ctx);
    }

    @Override
    public void exitXmlserialize_param_ident_part(PlSqlParser.Xmlserialize_param_ident_partContext ctx) {
        super.exitXmlserialize_param_ident_part(ctx);
    }

    @Override
    public void enterSql_plus_command(PlSqlParser.Sql_plus_commandContext ctx) {
        throwNotImplemented(); // super.enterSql_plus_command(ctx);
    }

    @Override
    public void exitSql_plus_command(PlSqlParser.Sql_plus_commandContext ctx) {
        super.exitSql_plus_command(ctx);
    }

    @Override
    public void enterWhenever_command(PlSqlParser.Whenever_commandContext ctx) {
        throwNotImplemented(); // super.enterWhenever_command(ctx);
    }

    @Override
    public void exitWhenever_command(PlSqlParser.Whenever_commandContext ctx) {
        super.exitWhenever_command(ctx);
    }

    @Override
    public void enterSet_command(PlSqlParser.Set_commandContext ctx) {
        throwNotImplemented(); // super.enterSet_command(ctx);
    }

    @Override
    public void exitSet_command(PlSqlParser.Set_commandContext ctx) {
        super.exitSet_command(ctx);
    }

    @Override
    public void enterPartition_extension_clause(PlSqlParser.Partition_extension_clauseContext ctx) {
        throwNotImplemented(); // super.enterPartition_extension_clause(ctx);
    }

    @Override
    public void exitPartition_extension_clause(PlSqlParser.Partition_extension_clauseContext ctx) {
        super.exitPartition_extension_clause(ctx);
    }

    @Override
    public void enterColumn_alias(PlSqlParser.Column_aliasContext ctx) {
        throwNotImplemented(); // super.enterColumn_alias(ctx);
    }

    @Override
    public void exitColumn_alias(PlSqlParser.Column_aliasContext ctx) {
        super.exitColumn_alias(ctx);
    }

    @Override
    public void enterTable_alias(PlSqlParser.Table_aliasContext ctx) {
        throwNotImplemented(); // super.enterTable_alias(ctx);
    }

    @Override
    public void exitTable_alias(PlSqlParser.Table_aliasContext ctx) {
        super.exitTable_alias(ctx);
    }

    @Override
    public void enterWhere_clause(PlSqlParser.Where_clauseContext ctx) {
        throwNotImplemented(); // super.enterWhere_clause(ctx);
    }

    @Override
    public void exitWhere_clause(PlSqlParser.Where_clauseContext ctx) {
        super.exitWhere_clause(ctx);
    }

    @Override
    public void enterInto_clause(PlSqlParser.Into_clauseContext ctx) {
        throwNotImplemented(); // super.enterInto_clause(ctx);
    }

    @Override
    public void exitInto_clause(PlSqlParser.Into_clauseContext ctx) {
        super.exitInto_clause(ctx);
    }

    @Override
    public void enterXml_column_name(PlSqlParser.Xml_column_nameContext ctx) {
        throwNotImplemented(); // super.enterXml_column_name(ctx);
    }

    @Override
    public void exitXml_column_name(PlSqlParser.Xml_column_nameContext ctx) {
        super.exitXml_column_name(ctx);
    }

    @Override
    public void enterCost_class_name(PlSqlParser.Cost_class_nameContext ctx) {
        throwNotImplemented(); // super.enterCost_class_name(ctx);
    }

    @Override
    public void exitCost_class_name(PlSqlParser.Cost_class_nameContext ctx) {
        super.exitCost_class_name(ctx);
    }

    @Override
    public void enterAttribute_name(PlSqlParser.Attribute_nameContext ctx) {
        throwNotImplemented(); // super.enterAttribute_name(ctx);
    }

    @Override
    public void exitAttribute_name(PlSqlParser.Attribute_nameContext ctx) {
        super.exitAttribute_name(ctx);
    }

    @Override
    public void enterSavepoint_name(PlSqlParser.Savepoint_nameContext ctx) {
        throwNotImplemented(); // super.enterSavepoint_name(ctx);
    }

    @Override
    public void exitSavepoint_name(PlSqlParser.Savepoint_nameContext ctx) {
        super.exitSavepoint_name(ctx);
    }

    @Override
    public void enterRollback_segment_name(PlSqlParser.Rollback_segment_nameContext ctx) {
        throwNotImplemented(); // super.enterRollback_segment_name(ctx);
    }

    @Override
    public void exitRollback_segment_name(PlSqlParser.Rollback_segment_nameContext ctx) {
        super.exitRollback_segment_name(ctx);
    }

    @Override
    public void enterTable_var_name(PlSqlParser.Table_var_nameContext ctx) {
        throwNotImplemented(); // super.enterTable_var_name(ctx);
    }

    @Override
    public void exitTable_var_name(PlSqlParser.Table_var_nameContext ctx) {
        super.exitTable_var_name(ctx);
    }

    @Override
    public void enterSchema_name(PlSqlParser.Schema_nameContext ctx) {
        throwNotImplemented(); // super.enterSchema_name(ctx);
    }

    @Override
    public void exitSchema_name(PlSqlParser.Schema_nameContext ctx) {
        super.exitSchema_name(ctx);
    }

    @Override
    public void enterRoutine_name(PlSqlParser.Routine_nameContext ctx) {
        throwNotImplemented(); // super.enterRoutine_name(ctx);
    }

    @Override
    public void exitRoutine_name(PlSqlParser.Routine_nameContext ctx) {
        super.exitRoutine_name(ctx);
    }

    @Override
    public void enterPackage_name(PlSqlParser.Package_nameContext ctx) {
        super.enterPackage_name(ctx);
    }

    @Override
    public void exitPackage_name(PlSqlParser.Package_nameContext ctx) {
        super.exitPackage_name(ctx);
    }

    @Override
    public void enterImplementation_type_name(PlSqlParser.Implementation_type_nameContext ctx) {
        throwNotImplemented(); // super.enterImplementation_type_name(ctx);
    }

    @Override
    public void exitImplementation_type_name(PlSqlParser.Implementation_type_nameContext ctx) {
        super.exitImplementation_type_name(ctx);
    }

    @Override
    public void enterParameter_name(PlSqlParser.Parameter_nameContext ctx) {
        ((ServiceMethodParameter) getNode()).setName(ctx.identifier().getText());
    }

    @Override
    public void exitParameter_name(PlSqlParser.Parameter_nameContext ctx) {
        super.exitParameter_name(ctx);
    }

    @Override
    public void enterReference_model_name(PlSqlParser.Reference_model_nameContext ctx) {
        throwNotImplemented(); // super.enterReference_model_name(ctx);
    }

    @Override
    public void exitReference_model_name(PlSqlParser.Reference_model_nameContext ctx) {
        super.exitReference_model_name(ctx);
    }

    @Override
    public void enterMain_model_name(PlSqlParser.Main_model_nameContext ctx) {
        throwNotImplemented(); // super.enterMain_model_name(ctx);
    }

    @Override
    public void exitMain_model_name(PlSqlParser.Main_model_nameContext ctx) {
        super.exitMain_model_name(ctx);
    }

    @Override
    public void enterContainer_tableview_name(PlSqlParser.Container_tableview_nameContext ctx) {
        throwNotImplemented(); // super.enterContainer_tableview_name(ctx);
    }

    @Override
    public void exitContainer_tableview_name(PlSqlParser.Container_tableview_nameContext ctx) {
        super.exitContainer_tableview_name(ctx);
    }

    @Override
    public void enterAggregate_function_name(PlSqlParser.Aggregate_function_nameContext ctx) {
        throwNotImplemented(); // super.enterAggregate_function_name(ctx);
    }

    @Override
    public void exitAggregate_function_name(PlSqlParser.Aggregate_function_nameContext ctx) {
        super.exitAggregate_function_name(ctx);
    }

    @Override
    public void enterQuery_name(PlSqlParser.Query_nameContext ctx) {
        throwNotImplemented(); // super.enterQuery_name(ctx);
    }

    @Override
    public void exitQuery_name(PlSqlParser.Query_nameContext ctx) {
        super.exitQuery_name(ctx);
    }

    @Override
    public void enterGrantee_name(PlSqlParser.Grantee_nameContext ctx) {
        throwNotImplemented(); // super.enterGrantee_name(ctx);
    }

    @Override
    public void exitGrantee_name(PlSqlParser.Grantee_nameContext ctx) {
        super.exitGrantee_name(ctx);
    }

    @Override
    public void enterRole_name(PlSqlParser.Role_nameContext ctx) {
        throwNotImplemented(); // super.enterRole_name(ctx);
    }

    @Override
    public void exitRole_name(PlSqlParser.Role_nameContext ctx) {
        super.exitRole_name(ctx);
    }

    @Override
    public void enterConstraint_name(PlSqlParser.Constraint_nameContext ctx) {
        throwNotImplemented(); // super.enterConstraint_name(ctx);
    }

    @Override
    public void exitConstraint_name(PlSqlParser.Constraint_nameContext ctx) {
        super.exitConstraint_name(ctx);
    }

    @Override
    public void enterLabel_name(PlSqlParser.Label_nameContext ctx) {
        throwNotImplemented(); // super.enterLabel_name(ctx);
    }

    @Override
    public void exitLabel_name(PlSqlParser.Label_nameContext ctx) {
        super.exitLabel_name(ctx);
    }

    @Override
    public void enterType_name(PlSqlParser.Type_nameContext ctx) {
        throwNotImplemented(); // super.enterType_name(ctx);
    }

    @Override
    public void exitType_name(PlSqlParser.Type_nameContext ctx) {
        super.exitType_name(ctx);
    }

    @Override
    public void enterSequence_name(PlSqlParser.Sequence_nameContext ctx) {
        throwNotImplemented(); // super.enterSequence_name(ctx);
    }

    @Override
    public void exitSequence_name(PlSqlParser.Sequence_nameContext ctx) {
        super.exitSequence_name(ctx);
    }

    @Override
    public void enterException_name(PlSqlParser.Exception_nameContext ctx) {
        throwNotImplemented(); // super.enterException_name(ctx);
    }

    @Override
    public void exitException_name(PlSqlParser.Exception_nameContext ctx) {
        super.exitException_name(ctx);
    }

    @Override
    public void enterFunction_name(PlSqlParser.Function_nameContext ctx) {
        throwNotImplemented(); // super.enterFunction_name(ctx);
    }

    @Override
    public void exitFunction_name(PlSqlParser.Function_nameContext ctx) {
        super.exitFunction_name(ctx);
    }

    @Override
    public void enterProcedure_name(PlSqlParser.Procedure_nameContext ctx) {
        throwNotImplemented(); // super.enterProcedure_name(ctx);
    }

    @Override
    public void exitProcedure_name(PlSqlParser.Procedure_nameContext ctx) {
        super.exitProcedure_name(ctx);
    }

    @Override
    public void enterTrigger_name(PlSqlParser.Trigger_nameContext ctx) {
        throwNotImplemented(); // super.enterTrigger_name(ctx);
    }

    @Override
    public void exitTrigger_name(PlSqlParser.Trigger_nameContext ctx) {
        super.exitTrigger_name(ctx);
    }

    @Override
    public void enterVariable_name(PlSqlParser.Variable_nameContext ctx) {
        throwNotImplemented(); // super.enterVariable_name(ctx);
    }

    @Override
    public void exitVariable_name(PlSqlParser.Variable_nameContext ctx) {
        super.exitVariable_name(ctx);
    }

    @Override
    public void enterIndex_name(PlSqlParser.Index_nameContext ctx) {
        throwNotImplemented(); // super.enterIndex_name(ctx);
    }

    @Override
    public void exitIndex_name(PlSqlParser.Index_nameContext ctx) {
        super.exitIndex_name(ctx);
    }

    @Override
    public void enterCursor_name(PlSqlParser.Cursor_nameContext ctx) {
        throwNotImplemented(); // super.enterCursor_name(ctx);
    }

    @Override
    public void exitCursor_name(PlSqlParser.Cursor_nameContext ctx) {
        super.exitCursor_name(ctx);
    }

    @Override
    public void enterRecord_name(PlSqlParser.Record_nameContext ctx) {
        throwNotImplemented(); // super.enterRecord_name(ctx);
    }

    @Override
    public void exitRecord_name(PlSqlParser.Record_nameContext ctx) {
        super.exitRecord_name(ctx);
    }

    @Override
    public void enterCollection_name(PlSqlParser.Collection_nameContext ctx) {
        throwNotImplemented(); // super.enterCollection_name(ctx);
    }

    @Override
    public void exitCollection_name(PlSqlParser.Collection_nameContext ctx) {
        super.exitCollection_name(ctx);
    }

    @Override
    public void enterLink_name(PlSqlParser.Link_nameContext ctx) {
        throwNotImplemented(); // super.enterLink_name(ctx);
    }

    @Override
    public void exitLink_name(PlSqlParser.Link_nameContext ctx) {
        super.exitLink_name(ctx);
    }

    @Override
    public void enterColumn_name(PlSqlParser.Column_nameContext ctx) {
        throwNotImplemented(); // super.enterColumn_name(ctx);
    }

    @Override
    public void exitColumn_name(PlSqlParser.Column_nameContext ctx) {
        super.exitColumn_name(ctx);
    }

    @Override
    public void enterTableview_name(PlSqlParser.Tableview_nameContext ctx) {
        throwNotImplemented(); // super.enterTableview_name(ctx);
    }

    @Override
    public void exitTableview_name(PlSqlParser.Tableview_nameContext ctx) {
        super.exitTableview_name(ctx);
    }

    @Override
    public void enterChar_set_name(PlSqlParser.Char_set_nameContext ctx) {
        throwNotImplemented(); // super.enterChar_set_name(ctx);
    }

    @Override
    public void exitChar_set_name(PlSqlParser.Char_set_nameContext ctx) {
        super.exitChar_set_name(ctx);
    }

    @Override
    public void enterSynonym_name(PlSqlParser.Synonym_nameContext ctx) {
        throwNotImplemented(); // super.enterSynonym_name(ctx);
    }

    @Override
    public void exitSynonym_name(PlSqlParser.Synonym_nameContext ctx) {
        super.exitSynonym_name(ctx);
    }

    @Override
    public void enterSchema_object_name(PlSqlParser.Schema_object_nameContext ctx) {
        throwNotImplemented(); // super.enterSchema_object_name(ctx);
    }

    @Override
    public void exitSchema_object_name(PlSqlParser.Schema_object_nameContext ctx) {
        super.exitSchema_object_name(ctx);
    }

    @Override
    public void enterDir_object_name(PlSqlParser.Dir_object_nameContext ctx) {
        throwNotImplemented(); // super.enterDir_object_name(ctx);
    }

    @Override
    public void exitDir_object_name(PlSqlParser.Dir_object_nameContext ctx) {
        super.exitDir_object_name(ctx);
    }

    @Override
    public void enterUser_object_name(PlSqlParser.User_object_nameContext ctx) {
        throwNotImplemented(); // super.enterUser_object_name(ctx);
    }

    @Override
    public void exitUser_object_name(PlSqlParser.User_object_nameContext ctx) {
        super.exitUser_object_name(ctx);
    }

    @Override
    public void enterGrant_object_name(PlSqlParser.Grant_object_nameContext ctx) {
        throwNotImplemented(); // super.enterGrant_object_name(ctx);
    }

    @Override
    public void exitGrant_object_name(PlSqlParser.Grant_object_nameContext ctx) {
        super.exitGrant_object_name(ctx);
    }

    @Override
    public void enterColumn_list(PlSqlParser.Column_listContext ctx) {
        throwNotImplemented(); // super.enterColumn_list(ctx);
    }

    @Override
    public void exitColumn_list(PlSqlParser.Column_listContext ctx) {
        super.exitColumn_list(ctx);
    }

    @Override
    public void enterParen_column_list(PlSqlParser.Paren_column_listContext ctx) {
        throwNotImplemented(); // super.enterParen_column_list(ctx);
    }

    @Override
    public void exitParen_column_list(PlSqlParser.Paren_column_listContext ctx) {
        super.exitParen_column_list(ctx);
    }

    @Override
    public void enterKeep_clause(PlSqlParser.Keep_clauseContext ctx) {
        throwNotImplemented(); // super.enterKeep_clause(ctx);
    }

    @Override
    public void exitKeep_clause(PlSqlParser.Keep_clauseContext ctx) {
        super.exitKeep_clause(ctx);
    }

    @Override
    public void enterFunction_argument(PlSqlParser.Function_argumentContext ctx) {
        throwNotImplemented(); // super.enterFunction_argument(ctx);
    }

    @Override
    public void exitFunction_argument(PlSqlParser.Function_argumentContext ctx) {
        super.exitFunction_argument(ctx);
    }

    @Override
    public void enterFunction_argument_analytic(PlSqlParser.Function_argument_analyticContext ctx) {
        throwNotImplemented(); // super.enterFunction_argument_analytic(ctx);
    }

    @Override
    public void exitFunction_argument_analytic(PlSqlParser.Function_argument_analyticContext ctx) {
        super.exitFunction_argument_analytic(ctx);
    }

    @Override
    public void enterFunction_argument_modeling(PlSqlParser.Function_argument_modelingContext ctx) {
        throwNotImplemented(); // super.enterFunction_argument_modeling(ctx);
    }

    @Override
    public void exitFunction_argument_modeling(PlSqlParser.Function_argument_modelingContext ctx) {
        super.exitFunction_argument_modeling(ctx);
    }

    @Override
    public void enterRespect_or_ignore_nulls(PlSqlParser.Respect_or_ignore_nullsContext ctx) {
        throwNotImplemented(); // super.enterRespect_or_ignore_nulls(ctx);
    }

    @Override
    public void exitRespect_or_ignore_nulls(PlSqlParser.Respect_or_ignore_nullsContext ctx) {
        super.exitRespect_or_ignore_nulls(ctx);
    }

    @Override
    public void enterArgument(PlSqlParser.ArgumentContext ctx) {
        throwNotImplemented(); // super.enterArgument(ctx);
    }

    @Override
    public void exitArgument(PlSqlParser.ArgumentContext ctx) {
        super.exitArgument(ctx);
    }

    @Override
    public void enterType_spec(PlSqlParser.Type_specContext ctx) {
        super.enterType_spec(ctx);
    }

    @Override
    public void exitType_spec(PlSqlParser.Type_specContext ctx) {
        super.exitType_spec(ctx);
    }

    @Override
    public void enterDatatype(PlSqlParser.DatatypeContext ctx) {
        super.enterDatatype(ctx);
    }

    @Override
    public void exitDatatype(PlSqlParser.DatatypeContext ctx) {
        super.exitDatatype(ctx);
    }

    @Override
    public void enterPrecision_part(PlSqlParser.Precision_partContext ctx) {
        throwNotImplemented(); // super.enterPrecision_part(ctx);
    }

    @Override
    public void exitPrecision_part(PlSqlParser.Precision_partContext ctx) {
        super.exitPrecision_part(ctx);
    }

    @Override
    public void enterNative_datatype_element(PlSqlParser.Native_datatype_elementContext ctx) {
        super.enterNative_datatype_element(ctx);

        Class valueTypeClass = nativeDataTypeNameToServiceMethodValueTypeClass(ctx.getText());
        if ( ServiceMethodParameter.class.isAssignableFrom(getNode().getClass())) {
            ((ServiceMethodParameter) getNode()).setValueType(
                    push((Class<AbstractServiceMethodValueType>) valueTypeClass, "valueType")
            );
        } else {
            throwException("Cannot set value type for node class ?", getNode().getClass());
        }

    }

    @Override
    public void exitNative_datatype_element(PlSqlParser.Native_datatype_elementContext ctx) {
        super.exitNative_datatype_element(ctx);
        pop();
    }

    @Override
    public void enterBind_variable(PlSqlParser.Bind_variableContext ctx) {
        throwNotImplemented(); // super.enterBind_variable(ctx);
    }

    @Override
    public void exitBind_variable(PlSqlParser.Bind_variableContext ctx) {
        super.exitBind_variable(ctx);
    }

    @Override
    public void enterGeneral_element(PlSqlParser.General_elementContext ctx) {
        throwNotImplemented(); // super.enterGeneral_element(ctx);
    }

    @Override
    public void exitGeneral_element(PlSqlParser.General_elementContext ctx) {
        super.exitGeneral_element(ctx);
    }

    @Override
    public void enterGeneral_element_part(PlSqlParser.General_element_partContext ctx) {
        throwNotImplemented(); // super.enterGeneral_element_part(ctx);
    }

    @Override
    public void exitGeneral_element_part(PlSqlParser.General_element_partContext ctx) {
        super.exitGeneral_element_part(ctx);
    }

    @Override
    public void enterTable_element(PlSqlParser.Table_elementContext ctx) {
        throwNotImplemented(); // super.enterTable_element(ctx);
    }

    @Override
    public void exitTable_element(PlSqlParser.Table_elementContext ctx) {
        super.exitTable_element(ctx);
    }

    @Override
    public void enterObject_privilege(PlSqlParser.Object_privilegeContext ctx) {
        throwNotImplemented(); // super.enterObject_privilege(ctx);
    }

    @Override
    public void exitObject_privilege(PlSqlParser.Object_privilegeContext ctx) {
        super.exitObject_privilege(ctx);
    }

    @Override
    public void enterSystem_privilege(PlSqlParser.System_privilegeContext ctx) {
        throwNotImplemented(); // super.enterSystem_privilege(ctx);
    }

    @Override
    public void exitSystem_privilege(PlSqlParser.System_privilegeContext ctx) {
        super.exitSystem_privilege(ctx);
    }

    @Override
    public void enterConstant(PlSqlParser.ConstantContext ctx) {
        throwNotImplemented(); // super.enterConstant(ctx);
    }

    @Override
    public void exitConstant(PlSqlParser.ConstantContext ctx) {
        super.exitConstant(ctx);
    }

    @Override
    public void enterNumeric(PlSqlParser.NumericContext ctx) {
        throwNotImplemented(); // super.enterNumeric(ctx);
    }

    @Override
    public void exitNumeric(PlSqlParser.NumericContext ctx) {
        super.exitNumeric(ctx);
    }

    @Override
    public void enterNumeric_negative(PlSqlParser.Numeric_negativeContext ctx) {
        throwNotImplemented(); // super.enterNumeric_negative(ctx);
    }

    @Override
    public void exitNumeric_negative(PlSqlParser.Numeric_negativeContext ctx) {
        super.exitNumeric_negative(ctx);
    }

    @Override
    public void enterQuoted_string(PlSqlParser.Quoted_stringContext ctx) {
        throwNotImplemented(); // super.enterQuoted_string(ctx);
    }

    @Override
    public void exitQuoted_string(PlSqlParser.Quoted_stringContext ctx) {
        super.exitQuoted_string(ctx);
    }

    @Override
    public void enterIdentifier(PlSqlParser.IdentifierContext ctx) {

        if ( ServiceNode.class.isAssignableFrom(getNode().getClass())
                 || ServiceMethod.class.isAssignableFrom(getNode().getClass())
                 || ServiceMethodParameter.class.isAssignableFrom(getNode().getClass())
                ) {
            getNode().setName(ctx.getText());
        } else {
            throwException("Cannot set identified for node class ?", getNode().getClass());
        }

//        throwNotImplemented(); // super.enterIdentifier(ctx);
    }

    @Override
    public void exitIdentifier(PlSqlParser.IdentifierContext ctx) {
        super.exitIdentifier(ctx);
    }

    @Override
    public void enterId_expression(PlSqlParser.Id_expressionContext ctx) {
//        throwNotImplemented(); // super.enterId_expression(ctx);
    }

    @Override
    public void exitId_expression(PlSqlParser.Id_expressionContext ctx) {
        super.exitId_expression(ctx);
    }

    @Override
    public void enterOuter_join_sign(PlSqlParser.Outer_join_signContext ctx) {
        throwNotImplemented(); // super.enterOuter_join_sign(ctx);
    }

    @Override
    public void exitOuter_join_sign(PlSqlParser.Outer_join_signContext ctx) {
        super.exitOuter_join_sign(ctx);
    }

    @Override
    public void enterRegular_id(PlSqlParser.Regular_idContext ctx) {
//        throwNotImplemented(); // super.enterRegular_id(ctx);
    }

    @Override
    public void exitRegular_id(PlSqlParser.Regular_idContext ctx) {
        super.exitRegular_id(ctx);
    }

    @Override
    public void enterNon_reserved_keywords_in_12c(PlSqlParser.Non_reserved_keywords_in_12cContext ctx) {
        throwNotImplemented(); // super.enterNon_reserved_keywords_in_12c(ctx);
    }

    @Override
    public void exitNon_reserved_keywords_in_12c(PlSqlParser.Non_reserved_keywords_in_12cContext ctx) {
        super.exitNon_reserved_keywords_in_12c(ctx);
    }

    @Override
    public void enterNon_reserved_keywords_pre12c(PlSqlParser.Non_reserved_keywords_pre12cContext ctx) {
//        throwNotImplemented(); // super.enterNon_reserved_keywords_pre12c(ctx);
    }

    @Override
    public void exitNon_reserved_keywords_pre12c(PlSqlParser.Non_reserved_keywords_pre12cContext ctx) {
        super.exitNon_reserved_keywords_pre12c(ctx);
    }

    @Override
    public void enterString_function_name(PlSqlParser.String_function_nameContext ctx) {
        throwNotImplemented(); // super.enterString_function_name(ctx);
    }

    @Override
    public void exitString_function_name(PlSqlParser.String_function_nameContext ctx) {
        super.exitString_function_name(ctx);
    }

    @Override
    public void enterNumeric_function_name(PlSqlParser.Numeric_function_nameContext ctx) {
        throwNotImplemented(); // super.enterNumeric_function_name(ctx);
    }

    @Override
    public void exitNumeric_function_name(PlSqlParser.Numeric_function_nameContext ctx) {
        super.exitNumeric_function_name(ctx);
    }

    @Override
    public void enterEveryRule(ParserRuleContext ctx) {
//        throwNotImplemented(); // super.enterEveryRule(ctx);
        super.enterEveryRule(ctx);
    }

    @Override
    public void exitEveryRule(ParserRuleContext ctx) {
        super.exitEveryRule(ctx);
    }

    @Override
    public void visitTerminal(TerminalNode node) {
        super.visitTerminal(node);
    }

    @Override
    public void visitErrorNode(ErrorNode node) {
        super.visitErrorNode(node);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }
}
