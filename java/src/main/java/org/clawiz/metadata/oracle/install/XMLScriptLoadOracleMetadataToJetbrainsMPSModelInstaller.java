/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.oracle.install;

import org.clawiz.core.common.system.installer.script.xml.AbstractXMLScriptElementInstaller;
import org.clawiz.core.common.utils.StringUtils;

public class XMLScriptLoadOracleMetadataToJetbrainsMPSModelInstaller extends AbstractXMLScriptElementInstaller {

    public static final String ATTRIBUTE_NAME_URL        = "url";
    public static final String ATTRIBUTE_NAME_USER       = "user";
    public static final String ATTRIBUTE_NAME_PASSWORD   = "password";
    public static final String ATTRIBUTE_NAME_MODEL_PATH = "model-path";

    @Override
    public String getAttribute(String name) {
        String value = super.getAttribute(name);
        if ( !StringUtils.isEmpty(value)) {
            return value;
        }
        throw coreException("Attribute ? not defined for 'load-oracle-metadata-to-jetbrains-mps-model' tag in install script ?", name, getScriptFileName());
    }


    @Override
    public void process() {

        LoadOracleMetadataToJetbrainsMPSModelInstaller installer = addInstaller(LoadOracleMetadataToJetbrainsMPSModelInstaller.class);
        installer.setUrl(getAttribute(ATTRIBUTE_NAME_URL));
        installer.setUser(getAttribute(ATTRIBUTE_NAME_USER));
        installer.setPassword(getAttribute(ATTRIBUTE_NAME_PASSWORD));
        installer.setModelPath(relativeToFullFileName(getAttribute(ATTRIBUTE_NAME_MODEL_PATH)));

    }
}
