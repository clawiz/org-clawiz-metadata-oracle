/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.oracle.loader;

import org.clawiz.core.common.metadata.data.database.table.TableList;
import org.clawiz.core.common.metadata.loader.MetadataLoaderList;
import org.clawiz.core.common.metadata.loader.jdbc.AbstractJdbcMetadataLoaderContext;

import java.util.HashMap;

public class OracleMetadataLoaderContext extends AbstractJdbcMetadataLoaderContext {

    HashMap<String, HashMap<String, String>> sources = new HashMap<>();

    public HashMap<String, HashMap<String, String>> getSources() {
        return sources;
    }

    public HashMap<String, String> getSourcesByType(OracleObjectType type) {
        HashMap<String, String> result = sources.get(type.toString());
        if ( result == null ) {
            result = new HashMap<>();
            sources.put(type.toString(), result);
        }
        return result;
    }

}
