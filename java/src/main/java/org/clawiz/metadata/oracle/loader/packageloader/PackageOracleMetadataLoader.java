/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.oracle.loader.packageloader;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.atn.ATN;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.oracle.loader.AbstractOracleMetadataParserErrorListener;
import org.clawiz.metadata.oracle.loader.CaseChangingCharStream;
import org.clawiz.metadata.oracle.loader.antlr.PlSqlBaseParser;
import org.clawiz.metadata.oracle.loader.antlr.PlSqlLexer;
import org.clawiz.metadata.oracle.loader.AbstractOracleMetadataLoader;
import org.clawiz.metadata.oracle.loader.antlr.PlSqlParser;

public class PackageOracleMetadataLoader extends AbstractOracleMetadataLoader {

    String name;
    String spec;
    String body;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }


    protected void parseSources() {

        ParseTree         tree  = null;
        String spec = getSpec();
        if (StringUtils.isLike(spec, "PACKAGE*")) {
            spec = "CREATE " + spec;
        }

        try {
            CaseChangingCharStream stream = new CaseChangingCharStream(CharStreams.fromString(spec), true);
            PlSqlLexer lexer         = new PlSqlLexer(stream);

            lexer.removeErrorListeners();
            lexer.addErrorListener(new AbstractOracleMetadataParserErrorListener());

            CommonTokenStream tokens = new CommonTokenStream(lexer);
            PlSqlParser       parser = new PlSqlParser(tokens);

            parser.removeErrorListeners();
            parser.addErrorListener(new AbstractOracleMetadataParserErrorListener());

            tree = parser.create_package();

        } catch (Exception e) {
            throwException("Exception on parse plsql : ? : \n<PLSQL>\n?\n</PLSQL>", e.getMessage(), spec, e);
        }

        logDebug("Parsed");

        ParseTreeWalker walker = new ParseTreeWalker();

        PackageOracleMetadataParserListener listener = new PackageOracleMetadataParserListener();
        listener.setLoader(this);

        walker.walk(listener, tree);

    }

    @Override
    public void process() {
        super.process();

        logDebug("Loading package " + getName());

        parseSources();

        logDebug("   package " + getName() + " loaded");

    }
}
