/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.metadata.oracle.install;

import org.clawiz.core.common.system.installer.AbstractInstaller;
import org.clawiz.metadata.jetbrains.mps.generator.solution.MpsSolutionGenerator;
import org.clawiz.metadata.jetbrains.mps.generator.solution.MpsSolutionGeneratorContext;
import org.clawiz.metadata.oracle.loader.OracleMetadataLoader;
import org.clawiz.metadata.oracle.loader.OracleMetadataLoaderContext;

public class LoadOracleMetadataToJetbrainsMPSModelInstaller extends AbstractInstaller {

    String url;
    String user;
    String password;
    String modelPath;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getModelPath() {
        return modelPath;
    }

    public void setModelPath(String modelPath) {
        this.modelPath = modelPath;
    }

    @Override
    public void process() {

        OracleMetadataLoaderContext context = new OracleMetadataLoaderContext();
        context.setSourceUrl(getUrl());
        context.setSourceUser(getUser());
        context.setSourcePassword(getPassword());

        context.setPackageName(getPackageName());

        OracleMetadataLoader loader = getService(OracleMetadataLoader.class,true);
        loader.setContext(context);

        loader.run();

        MpsSolutionGeneratorContext generatorContext = new MpsSolutionGeneratorContext();

        generatorContext.setSourceRootPackageName(getPackageName());
        generatorContext.setDestinationPath(getModelPath());

        generatorContext.addSourceNodes(loader.getContext().getNodes());

        MpsSolutionGenerator generator = getService(MpsSolutionGenerator.class);
        generator.setContext(generatorContext);

        generator.run();

    }
}
